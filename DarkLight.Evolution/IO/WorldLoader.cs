﻿using System.IO;
using System.Linq;
using DarkLight.Model;
using DarkLight.Model.GameObjects;
using Flai;

namespace DarkLight.IO
{
    public static class WorldLoader
    {
        public static World Load(BinaryReader reader)
        {
            RoomManager roomManager = WorldLoader.LoadRoomManager(reader);
            Player player = new Player(roomManager[0], new Vector2i(roomManager[0].Width, roomManager[0].Height) * Tile.Size / 2f); // todo: okay figure out how to do this properly..

            World world = new World(player, roomManager);
            WorldLoader.ResolvePortalDestinations(world);

            return world;
        }

        private static RoomManager LoadRoomManager(BinaryReader reader)
        {
            int roomCount = reader.ReadInt32();
            Room[] rooms = new Room[roomCount];
            for (int i = 0; i < roomCount; i++)
            {
                rooms[i] = RoomLoader.Load(reader);
            }

            return new RoomManager(rooms);
        }

        private static void ResolvePortalDestinations(World world)
        {
            foreach (Portal portal in world.RoomManager.SelectMany(room => room.GameObjects.GetOfType<Portal>()))
            {
                portal.ResolveDestination(world.RoomManager);
            }
        }
    }
}
