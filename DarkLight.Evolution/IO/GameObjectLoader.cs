﻿using System;
using System.IO;
using System.Reflection;
using DarkLight.Model.GameObjects;
using Flai.IO;

namespace DarkLight.IO
{
    // TODO: This is slow...
    public static class GameObjectLoader
    {
        // todo: at least cache stuff
        public static GameObject Load(BinaryReader reader)
        {
            string typeName = reader.ReadString();

            Type type = AssemblyHelper.FindType(Assembly.GetCallingAssembly(), typeName);
            if (type == null || !typeof(GameObject).IsAssignableFrom(type))
            {
                throw new ArgumentException("Invalid type");
            }

            // BinaryReader.ReadGeneric<T> "is null" check. 
            if (!reader.ReadBoolean())
            {
                throw new ArgumentException("GameObject is null");
            }

            MethodInfo methodInfo = type.GetMethod("FromStream");
            return methodInfo.Invoke(null, new object[] { reader }) as GameObject;
        }
    }
}
