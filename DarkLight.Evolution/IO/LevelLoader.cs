﻿using System.IO;
using DarkLight.Model;

namespace DarkLight.IO
{
    public static class LevelLoader
    {
        // TODO: I was thinking.. Maybe having like "WorldInfo"/"RoomInfo"/"<GameObject>Info" (these could be shared between Editor and Game!!)
        // todo: cont.. and then I could do all stuff (linking portals etc) somehow cleaner... or would it be cleaner... hmph..
        // TODO: "Path" or "LevelName"?
        public static Level Load(string path)
        {
            using (Stream stream = File.OpenRead(path))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    return LevelLoader.Load(reader);
                }
            }
        }

        private static Level Load(BinaryReader reader)
        {
            // TODO: Properties or some other stuff here...?
            World world = WorldLoader.Load(reader);
            return new Level(world);
        }
    }
}
