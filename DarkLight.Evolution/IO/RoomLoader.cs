﻿using System.IO;
using DarkLight.Model;
using DarkLight.Model.GameObjects;
using Flai;
using Flai.General;

namespace DarkLight.IO
{
    public static class RoomLoader
    {
        public static Room Load(BinaryReader reader)
        {
            UID uid = UID.FromString(reader.ReadString());
            RoomTileMap tileMap = RoomLoader.LoadTileMap(reader);
            GameObjectManager gameObjectManager = RoomLoader.LoadGameObjects(reader);

            return new Room(uid, tileMap, gameObjectManager);
        }

        private static RoomTileMap LoadTileMap(BinaryReader reader)
        {
            int width = reader.ReadInt32();
            int height = reader.ReadInt32();

            int[] darkTileInts = reader.ReadInt32Array();
            int[] lightTileInts = reader.ReadInt32Array();

            TileType[] darkTiles = new TileType[darkTileInts.Length];
            for (int i = 0; i < darkTileInts.Length; i++)
            {
                darkTiles[i] = (TileType)darkTileInts[i];
            }

            TileType[] lightTiles = new TileType[lightTileInts.Length];
            for (int i = 0; i < lightTileInts.Length; i++)
            {
                lightTiles[i] = (TileType)lightTileInts[i];
            }

            return new RoomTileMap(new TileMap<TileType>(darkTiles, width, height, TileType.Solid), new TileMap<TileType>(lightTiles, width, height, TileType.Solid));
        }

        private static GameObjectManager LoadGameObjects(BinaryReader reader)
        {
            int gameObjectCount = reader.ReadInt32();
            GameObject[] gameObjects = new GameObject[gameObjectCount];
            for (int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i] = GameObjectLoader.Load(reader);
            }

            return new GameObjectManager(gameObjects);
        }
    }
}
