﻿using System.IO;
using System.Linq;
using Flai.DataStructures;

namespace DarkLight.IO
{
    public static class LevelFileHelper
    {
        public const string LevelFileExtension = ".darklight";

        public static bool FindLevelPathFromArguments(ReadOnlyArray<string> args, out string path)
        {
            path = args.FirstOrDefault(arg => arg.EndsWith(LevelFileHelper.LevelFileExtension) && File.Exists(arg));
            return path != null;
        }
    }
}
