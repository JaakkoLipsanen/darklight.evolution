﻿
using System;

namespace DarkLight
{
    // Just a wrapper for string. Possibly later change it to use something more efficient?
    // No idea if using this is a good choice.. Could be cleaner than using string id's
    public struct UID : IEquatable<UID>
    {
        private readonly string _id; // TODO: Needed?
        private readonly int _hash;

        private UID(string id)
        {
            _id = id;  // todo: generate better 64-bit hash or something and don't even save the string?
            _hash = _id.GetHashCode();
        }

        public override int GetHashCode()
        {
            return _hash;
        }

        public bool Equals(UID other)
        {
            return _hash == other._hash && _id == other._id;
        }

        public override bool Equals(object obj)
        {
            return obj is UID && this.Equals((UID)obj);
        }

        public static implicit operator UID(string s)
        {
            return UID.FromString(s);
        }

        public static UID FromString(string id)
        {
            return new UID(id);
        }
    }
}
