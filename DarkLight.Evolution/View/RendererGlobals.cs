﻿
namespace DarkLight.View
{
    // Hmm... Instead of one big "DarkLightGlobals", maybe we could have multiple 
    // classes, like ( "DarkLight(View/Model/Controller/<maybe nothing?>)Globals" )
    public static class RendererGlobals
    {
        // Should there be "Dimension" in the name..?
        // Like ForegroundDimensionAlpha.. idk
        public const float ForegroundAlpha = 1f;
        public const float BackgroundAlpha = 0.25f;
    }
}
