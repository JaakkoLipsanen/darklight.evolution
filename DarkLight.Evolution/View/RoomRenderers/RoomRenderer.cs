﻿using DarkLight.Model;
using Flai.Graphics;

namespace DarkLight.View.RoomRenderers
{
    // todo: post process stuff here?
    public abstract class RoomRenderer : FlaiRenderer
    {
        protected readonly Player _player;
        protected readonly Room _room;
        protected readonly RoomCamera _camera; // todo: "RoomCamera" class?

        public RoomCamera Camera
        {
            get { return _camera; }
        }

        protected RoomRenderer(Player player, Room room)
        {
            _player = player;
            _room = room;
            _camera = new RoomCamera(room);
        }
    }
}