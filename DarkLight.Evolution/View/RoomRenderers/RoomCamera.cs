﻿using DarkLight.Model;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DarkLight.View.RoomRenderers
{
    public class RoomCamera : ICamera2D
    {
        private readonly Room _room;
        private readonly Camera2D _camera;

        public Vector2 Position
        {
            get { return _camera.Position; }
        }

        public float Rotation
        {
            get { return _camera.Rotation; } // 0
        }

        public float Zoom
        {
            get { return _camera.Zoom; }
        }

        public RoomCamera(Room room)
        {
            _room = room;
            _camera = new Camera2D(new Vector2i(_room.Width, _room.Height) * Tile.Size / 2f) { Zoom = RoomCamera.CalculateZoom(room) };
        }

        public Matrix GetTransform(GraphicsDevice graphicsDevice)
        {
            return _camera.GetTransform(graphicsDevice);
        }

        public Matrix GetTransform(Size viewportSize)
        {
            return _camera.GetTransform(viewportSize);
        }

        public RectangleF GetArea(GraphicsDevice graphicsDevice)
        {
            return _camera.GetArea(graphicsDevice);
        }

        public RectangleF GetArea(Size screenSize)
        {
            return _camera.GetArea(screenSize);
        }

        public Vector2 ScreenToWorld(GraphicsDevice graphicsDevice, Vector2 v)
        {
            return _camera.ScreenToWorld(graphicsDevice, v);
        }

        public Vector2 ScreenToWorld(Size screenSize, Vector2 v)
        {
            return _camera.ScreenToWorld(screenSize, v);
        }

        private static float CalculateZoom(Room room)
        {
            float horizontalZoom = FlaiGame.Current.ScreenSize.Width / (float)(room.Width * Tile.Size);
            float verticalZoom = FlaiGame.Current.ScreenSize.Height / (float)(room.Height * Tile.Size);
            return FlaiMath.Min(horizontalZoom, verticalZoom);
        }
    }
}
