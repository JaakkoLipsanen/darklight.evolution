﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkLight.Model;
using Flai;
using Flai.Graphics;

namespace DarkLight.View.RoomRenderers
{
    public class RoomRendererManager : IEnumerable<RoomRenderer>
    {
        private readonly Dictionary<Room, RoomRenderer> _roomRenderers;
        private readonly World _world;

        public RoomRendererManager(World world) // blahh.. requires World since RoomManager doesn't have any access to Player.. hmph
        {
            _world = world;
            _roomRenderers = world.RoomManager.ToDictionary(room => room, room => RoomRendererFactory.CreateRenderer(world.Player, room));
        }

        public void LoadContent()
        {
            foreach (RoomRenderer renderer in _roomRenderers.Values)
            {
                renderer.LoadContent();
            }
        }

        public void Unload()
        {
            foreach (RoomRenderer renderer in _roomRenderers.Values)
            {
                renderer.Unload();
            }
        }

        public void Update(UpdateContext updateContext)
        {
            _roomRenderers[_world.CurrentRoom].Update(updateContext);
        }

        public void Draw(GraphicsContext graphicsContext)
        {
            _roomRenderers[_world.CurrentRoom].Draw(graphicsContext);
        }

        #region Implementation of IEnumerable<T>

        public IEnumerator<RoomRenderer> GetEnumerator()
        {
            return _roomRenderers.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}
