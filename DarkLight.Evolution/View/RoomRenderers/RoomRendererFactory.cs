﻿using DarkLight.Model;
using DarkLight.Model.GameObjects;
using Flai;
using Flai.General;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace DarkLight.View.RoomRenderers
{
    // todo: singleton or totally non-static? maybe not needed though..
    public static class RoomRendererFactory
    {
        public static RoomRenderer CreateRenderer(Player player, Room room)
        {
            switch (room.GraphicsStyle)
            {
                // ....
                default:
                    return new DefaultRoomRenderer(player, room);
            }
        }

        #region DefaultRoomRenderer

        private class DefaultRoomRenderer : RoomRenderer
        {
            public DefaultRoomRenderer(Player player, Room room)
                : base(player, room)
            {
            }

            protected override void DrawInner(GraphicsContext graphicsContext)
            {
                graphicsContext.SpriteBatch.Begin(_camera.GetTransform(graphicsContext.GraphicsDevice));
                graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, new RectangleF(0, 0, _room.Width * Tile.Size, _room.Width * Tile.Size), Color.Purple * 0.5f);

                // todo: post process stuff
                this.DrawTiles(graphicsContext, _player.GameDimension.Opposite());
                this.DrawGameObjects(graphicsContext, _player.GameDimension.Opposite());

                this.DrawPlayer(graphicsContext);

                this.DrawTiles(graphicsContext, _player.GameDimension);
                this.DrawGameObjects(graphicsContext, _player.GameDimension);

                graphicsContext.SpriteBatch.End(); ;
            }

            private void DrawPlayer(GraphicsContext graphicsContext)
            {
                graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, _player.Area, Color.Red);
            }

            private void DrawTiles(GraphicsContext graphicsContext, GameDimension gameDimension)
            {
                ReadOnlyTileMap<TileType> tileMap = _room.TileMap.GetTileMapForDimension(gameDimension);
                for (int y = 0; y < _room.Height; y++)
                {
                    for (int x = 0; x <= _room.Width; x++) // assumes that the camera is static always 
                    {
                        this.DrawTile(graphicsContext, gameDimension, x, y, tileMap[x, y]);
                    }
                }
            }

            private void DrawTile(GraphicsContext graphicsContext, GameDimension gameDimension, int x, int y, TileType tileType)
            {
                float alpha = (gameDimension == _player.GameDimension) ?
                    RendererGlobals.ForegroundAlpha :
                    RendererGlobals.BackgroundAlpha;

                if (tileType == TileType.Solid)
                {
                    graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, Tile.GetTileBounds(x, y), gameDimension.ToColor() * alpha);
                }
                else if (tileType == TileType.Lava)
                {
                    graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, Tile.GetTileBounds(x, y), Color.Red * alpha);
                }
            }

            private void DrawGameObjects(GraphicsContext graphicsContext, GameDimension gameDimension)
            {
                float alpha = (gameDimension == _player.GameDimension) ?
                    RendererGlobals.ForegroundAlpha :
                    RendererGlobals.BackgroundAlpha;

                foreach (GameObject gameObject in _room.GameObjects.GetGameObjects(gameDimension))
                {
                    graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, gameObject.VisualArea, Color.Purple * alpha);
                }
            }
        }

        #endregion
    }
}
