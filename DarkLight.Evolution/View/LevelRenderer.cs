﻿using DarkLight.Model;
using DarkLight.View.RoomRenderers;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace DarkLight.View
{
    public class LevelRenderer : FlaiRenderer
    {
        private readonly Level _level;
        private readonly RoomRendererManager _roomRendererManager;
        // todo: should Room stuff be put one level deeper (WorldRenderer)?

        public LevelRenderer(Level level)
        {
            _level = level;
            _roomRendererManager = new RoomRendererManager(level.World);
        }

        protected override void LoadContentInner()
        {
            _roomRendererManager.LoadContent();
        }

        protected override void UnloadInner()
        {
            _roomRendererManager.Unload();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _roomRendererManager.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            graphicsContext.GraphicsDevice.Clear(Color.Black);
            _roomRendererManager.Draw(graphicsContext);
        }
    }
}