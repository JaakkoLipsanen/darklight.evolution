﻿using DarkLight.Model;
using Flai;
using Flai.Input;
using Microsoft.Xna.Framework.Input;

namespace DarkLight.Controller
{
    public class PlayerController : FlaiController
    {
        private readonly Player _player;
        public PlayerController(Player player)
        {
            _player = player;
        }

        public override void Control(UpdateContext updateContext)
        {
            this.HandleChangeDimension(updateContext);
            this.HandleMovement(updateContext);
            this.HandleJumping(updateContext);
        }

        private void HandleMovement(UpdateContext updateContext)
        {
            float horizontalMovement = 0f;
            if (updateContext.InputState.IsKeyPressed(Keys.A) || updateContext.InputState.IsKeyPressed(Keys.Left))
            {
                horizontalMovement -= Player.HorizontalSpeed;
            }

            if (updateContext.InputState.IsKeyPressed(Keys.D) || updateContext.InputState.IsKeyPressed(Keys.Right))
            {
                horizontalMovement += Player.HorizontalSpeed;
            }

            _player.TargetHorizontalVelocity = horizontalMovement;
        }

        private void HandleJumping(UpdateContext updateContext)
        {
            // TODO: Pre/Late jumping?
            if (!_player.CanJump && _player.IsJumping)
            {
                if (!updateContext.InputState.IsKeyPressed(Keys.Space))
                {
                    _player.Fall(updateContext);
                }
            }
            else
            {
                if (updateContext.InputState.IsNewKeyPress(Keys.Space))
                {
                    _player.Jump();
                }
            }
        }

        private void HandleChangeDimension(UpdateContext updateContext)
        {
            if (updateContext.InputState.IsNewKeyPress(Keys.LeftShift))
            {
                // TODO: If succeeded, then yay, otherwise make a little late-changing/bias..?
                _player.TryChangeDimension();
            }
        }
    }
}