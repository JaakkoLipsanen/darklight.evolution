﻿
using Flai;
using Flai.General;

namespace DarkLight.Model
{
    // todo: better name..
    public enum RoomGraphicalStyle
    {
        // plaah
        // plooh
    }

    // Should Room be inheritable? There could be different attributes to each room or something?
    // And could the Player be different in each room? For example shape or something.
    // Maybe make "RoomProxy" etc?
    // TODO: It'd be a lot cleaner to completely separate Portals and GameObjects I thin... PortalManager etc?
    public class Room
    {
        private readonly UID _uid;
        private readonly RoomTileMap _tileMap;
        private readonly GameObjectManager _gameObjectManager;
        private readonly RoomGraphicalStyle _graphicalStyle; // todo

        public UID UID
        {
            get { return _uid; }
        }

        public RoomTileMap TileMap
        {
            get { return _tileMap; }
        }

        public GameObjectManager GameObjects
        {
            get { return _gameObjectManager; }
        }

        public int Width
        {
            get { return _tileMap.Width; }
        }

        public int Height
        {
            get { return _tileMap.Height; }
        }

        public RoomGraphicalStyle GraphicsStyle
        {
            get { return _graphicalStyle; }
        }

        public Room(UID uid, RoomTileMap tileMap, GameObjectManager gameObjectManager)
        {
            _uid = uid;
            _tileMap = tileMap;
            _gameObjectManager = gameObjectManager;
        }

        public bool IsOccupied(RectangleF area, GameDimension gameDimension)
        {
            /* Tiles */
            int leftTile = Tile.WorldToTileCoordinate(area.Left);
            int topTile = Tile.WorldToTileCoordinate(area.Top);
            int rightTile = Tile.WorldToTileCoordinate(FlaiMath.Ceiling(area.Right / Tile.Size) * Tile.Size) - 1;
            int bottomTile = Tile.WorldToTileCoordinate(FlaiMath.Ceiling(area.Bottom / Tile.Size) * Tile.Size) - 1;

            ReadOnlyTileMap<TileType> tileMap = _tileMap.GetTileMapForDimension(gameDimension);
            for (int y = topTile; y <= bottomTile; y++)
            {
                for (int x = leftTile; x <= rightTile; x++)
                {
                    if (Tile.IsSolid(tileMap[x, y]))
                    {
                        return true;
                    }
                }
            }

            return false;

            // TODO: GameObjects?
            // _gameObjectManager.IsOccupied(area, gameDimension)
        }
    }
}
