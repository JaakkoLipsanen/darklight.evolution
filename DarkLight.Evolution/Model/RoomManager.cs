﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DarkLight.Model
{
    // hmm... atm GameObjectManager and RoomManager are basically 1:1 copies..
    public class RoomManager : IEnumerable<Room>
    {
        private readonly List<Room> _rooms;
        private readonly Dictionary<UID, Room> _roomsById;

        public int Count
        {
            get { return _rooms.Count; }
        }

        public RoomManager(IEnumerable<Room> gameObjects)
        {
            _rooms = new List<Room>();
            _roomsById = new Dictionary<UID, Room>();

            foreach (Room gameObject in gameObjects)
            {
                this.Add(gameObject);
            }
        }

        public void Add(Room room)
        {
            if (_roomsById.ContainsKey(room.UID))
            {
                throw new ArgumentException("room");
            }

            _rooms.Add(room);
            _roomsById.Add(room.UID, room);
        }

        public Room GetRoomById(UID id)
        {
            return _roomsById[id];
        }

        public Room this[int index]
        {
            get { return _rooms[index]; }
        }

        #region Implementation of IEnumerable<T>

        public IEnumerator<Room> GetEnumerator()
        {
            return _rooms.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _rooms.GetEnumerator();
        }

        #endregion
    }
}
