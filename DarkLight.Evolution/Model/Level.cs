﻿using Flai;

namespace DarkLight.Model
{
    public class Level
    {
        private readonly World _world;

        public World World
        {
            get { return _world; }
        }

        public Player Player
        {
            get { return _world.Player; }
        }

        public Level(World world)
        {
            _world = world;
        }

        public void Update(UpdateContext updateContext)
        {
            _world.Update(updateContext);
        }
    }
}
