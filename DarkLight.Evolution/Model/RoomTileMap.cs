﻿using System;
using Flai.General;

namespace DarkLight.Model
{
    // Should this contain game objects etc too ("RoomMap" vs "RoomTileMap")
    public class RoomTileMap
    {
        private readonly ReadOnlyTileMap<TileType> _darkTileMap;
        private readonly ReadOnlyTileMap<TileType> _lightTileMap;

        public ITileMap<TileType> DarkTiles
        {
            get { return _darkTileMap; }
        }

        public ITileMap<TileType> LightTiles
        {
            get { return _lightTileMap; }
        }

        public int Width
        {
            // Dark and Light tile maps have same width
            get { return _darkTileMap.Width; }
        }

        public int Height
        {
            // Dark and Light tile maps have same height
            get { return _lightTileMap.Height; }
        }

        public RoomTileMap(ITileMap<TileType> darkTileMap, ITileMap<TileType> lightTileMap)
        {
            _darkTileMap = new ReadOnlyTileMap<TileType>(darkTileMap);
            _lightTileMap = new ReadOnlyTileMap<TileType>(lightTileMap);
        }

        public ReadOnlyTileMap<TileType> GetTileMapForDimension(GameDimension gameDimension)
        {
            switch (gameDimension)
            {
                case GameDimension.Dark:
                    return _darkTileMap;

                case GameDimension.Light:
                    return _lightTileMap;

                default:
                    throw new ArgumentException("Invalid GameDimension");
            }
        }
    }
}
