﻿using System.IO;
using Flai;
using Microsoft.Xna.Framework;

namespace DarkLight.Model.GameObjects
{
    public class MovingPlatform : GameObject
    {
        private readonly Size _size;
        public override RectangleF CollisionArea
        {
            get
            {
                Vector2 size = _size.ToVector2i() * Tile.Size;
                Vector2 topLeftPosition = base.Position - size / 2f;
                return new RectangleF(topLeftPosition.X, topLeftPosition.Y, size.X, size.Y);
            }
        }

        public MovingPlatform(UID uid, GameDimension gameDimension, Vector2 position, Size size) :
            base(uid, gameDimension, position)
        {
            _size = size;
        }

        public static MovingPlatform FromStream(BinaryReader reader)
        {
            UID uid;
            GameDimension gameDimension;
            Vector2 position;
            GameObject.FromStream(reader, out uid, out gameDimension, out position);

            int width = reader.ReadInt32();
            int height = reader.ReadInt32();
            bool isEnabled = reader.ReadBoolean();
            bool isCircular = reader.ReadBoolean();
            bool isRepeating = reader.ReadBoolean();
            float speed = reader.ReadSingle();

            Vector2i[] loopPoints = reader.ReadArray<Vector2i>();
            return new MovingPlatform(uid, gameDimension, position, new Size(width, height));
        }
    }
}
