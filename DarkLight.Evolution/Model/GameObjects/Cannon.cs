﻿using System.IO;
using Flai;
using Microsoft.Xna.Framework;

namespace DarkLight.Model.GameObjects
{
    public class Cannon : GameObject
    {
        private const float DefaultBulletSpeed = Tile.Size * 8;
        private const float DefaultTimeBetweenShots = 1.5f;

        public static readonly Vector2 BodySize = new Vector2(1, 2) * Tile.Size;
        public static readonly Vector2 PipeSize = new Vector2(1, 1) * Tile.Size;

        private Direction2D _direction;
        private float _bulletSpeed = Cannon.DefaultBulletSpeed;
        private float _timeBetweenShots = Cannon.DefaultTimeBetweenShots;
        private float _timeSinceLastShot = 0f;

        private Vector2 BulletSpawnPosition
        {
            get { return _position + _direction.ToUnitVector() * 0.5f * Tile.Size; }
        }

        public Direction2D Direction
        {
            get { return _direction; }
        }

        public float BulletSpeed
        {
            get { return _bulletSpeed; }
            set { _bulletSpeed = value; }
        }

        public float TimeBetweenShots
        {
            get { return _timeBetweenShots; }
            set { _timeBetweenShots = value; }
        }

        public override RectangleF CollisionArea
        {
            get
            {
                Vector2 size;
                if (_direction == Direction2D.Left || _direction == Direction2D.Right)
                {
                    size = new Vector2(Cannon.PipeSize.X + Cannon.BodySize.X,
                        FlaiMath.Max(Cannon.PipeSize.Y, Cannon.BodySize.Y));
                }
                else
                {
                    size = new Vector2(FlaiMath.Max(Cannon.PipeSize.Y, Cannon.BodySize.Y),
                        Cannon.PipeSize.X + Cannon.BodySize.X);
                }

                return new RectangleF(_position.X - size.X / 2f, _position.Y - size.Y / 2f, size.X, size.Y);
            }
        }

        public Cannon(UID uid, GameDimension gameDimension, Vector2 position,
            Direction2D direction, float bulletSpeed, float timeBetweenShots) :
            base(uid, gameDimension, position)
        {
            _direction = direction;
            _bulletSpeed = bulletSpeed;
            _timeBetweenShots = timeBetweenShots;
        }

        public static Cannon FromStream(BinaryReader reader)
        {
            UID uid;
            GameDimension gameDimension;
            Vector2 position;
            GameObject.FromStream(reader, out uid, out gameDimension, out position);

            Direction2D direction = (Direction2D)reader.ReadInt32();
            float bulletSpeed = reader.ReadSingle();
            float timeBetweenShots = reader.ReadSingle();

            return new Cannon(uid, gameDimension, position, direction, bulletSpeed, timeBetweenShots);
        }
    }
}