﻿using System.IO;
using Flai;
using Microsoft.Xna.Framework;

namespace DarkLight.Model.GameObjects
{
    // TODO: Should this be a GameObject...?
    public class Portal : GameObject
    {
        private readonly Direction2D _direction;
        private readonly int _length;

        private readonly PortalDestinationRef _destinationRef;
        private Portal _destination;

        public Vector2 StartPoint
        {
            get { return _position; }
        }

        public Vector2 EndPoint
        {
            get { return _position + _direction.ToUnitVector() * _length * Tile.Size; }
        }

        public Portal Destination
        {
            get { return _destination; }
        }

        public Segment2D Segment
        {
            get { return new Segment2D(this.StartPoint, this.EndPoint); }
        }

        public Direction2D NormalDirection
        {
            get { return _direction.RotateRight(); }
        }

        public override RectangleF CollisionArea
        {
            get
            {
                const int Offset = 2;
                Vector2 min = Vector2.Min(this.StartPoint, this.EndPoint);
                Vector2 max = Vector2.Max(this.StartPoint, this.EndPoint);

                if (_direction.ToAlignment() == Alignment.Vertical)
                {
                    return new RectangleF(min.X - Offset, min.Y, Offset * 2f, max.Y - min.Y);
                }
                else
                {
                    return new RectangleF(min.X, min.Y - Offset, max.X - min.X, Offset * 2f);
                }
            }
        }

        private Portal(UID uid, GameDimension gameDimension, Vector2 position, Direction2D direction, int length, PortalDestinationRef destinationRef) :
            base(uid, gameDimension, position)
        {
            _direction = direction;
            _length = length;
            _destinationRef = destinationRef;
        }

        public static Portal FromStream(BinaryReader reader)
        {
            UID uid;
            GameDimension gameDimension;
            Vector2 position;
            GameObject.FromStream(reader, out uid, out gameDimension, out position);

            Direction2D direction = (Direction2D)reader.ReadInt32();
            int length = reader.ReadInt32();

            PortalDestinationRef destinationRef = null;
            if (reader.ReadBoolean())
            {
                string destinationRoomId = reader.ReadString();
                string destinationPortalId = reader.ReadString();
                destinationRef = new PortalDestinationRef(destinationRoomId, destinationPortalId);
                // tadaa.. the "PortalInfo"/"RoomInfo" stuff could clean this up a bit.. maybe?
            }

            return new Portal(uid, gameDimension, position, direction, length, destinationRef);
        }

        // ýeah REMOVE
        public Room DestRoom { get; private set; }

        public void ResolveDestination(RoomManager roomManager)
        {
            if (_destinationRef != null)
            {
                Room room = roomManager.GetRoomById(_destinationRef.RoomId);
                this.DestRoom = room;
                _destination = room.GameObjects.GetGameObjectById(_destinationRef.PortalId) as Portal;
            }
        }

        // TODO: Don't do this if not absolutely necessary... would it be possible to save this data in to the XXXLoader and then use the info there?
        private class PortalDestinationRef
        {
            public string RoomId { get; private set; }
            public string PortalId { get; private set; }

            public PortalDestinationRef(string roomId, string portalId)
            {
                this.RoomId = roomId;
                this.PortalId = portalId;
            }
        }

        // nope nope nope.. too ugly. A: make it cleaner, B: move to some other class, like "PortalHelper" or something (or to PortalPhysicsBody or something!!)
        // it'd be great if the player wouldn't even have to know about the portals, and the Portal class itself could handle Player/gameobject going through it
        public bool IsSegmegntIntersect(Segment2D testSegment, out Vector2 newPosition)
        {
            if (_destination == null || !Segment2D.Intersects(testSegment, this.Segment))
            {
                newPosition = default(Vector2);
                return false;
            }

            Segment2D testSegmentLocal = new Segment2D(testSegment.Start - this.StartPoint, testSegment.End - this.EndPoint);
            if (this.NormalDirection == _destination.NormalDirection)
            {
                if (this.NormalDirection.ToAlignment() == Alignment.Horizontal)
                {
                    testSegmentLocal.End.X *= -1;
                }
                else
                {
                    testSegmentLocal.End.Y *= -1;
                }
            }

            if (this.NormalDirection == Direction2D.Left)
            {
                if (this.StartPoint.X - testSegment.Start.X > 0)
                {
                    newPosition = _destination.StartPoint + testSegmentLocal.End; // assuming destination.NormalDir == Right
                    return true;
                }
            }
            else if (this.NormalDirection == Direction2D.Right)
            {
                if (this.StartPoint.X - testSegment.Start.X < 0)
                {
                    newPosition = _destination.StartPoint + testSegmentLocal.End; // assuming destination.NormalDir == Left
                    return true;
                }
            }
            else if (this.NormalDirection == Direction2D.Up)
            {
                if (this.StartPoint.Y - testSegment.Start.Y > 0)
                {
                    newPosition = _destination.EndPoint + testSegmentLocal.End; // assuming destination.NormalDir == Down
                    return true;
                }
            }
            else if (this.NormalDirection == Direction2D.Down)
            {
                if (this.StartPoint.Y - testSegment.Start.Y < 0)
                {
                    newPosition = _destination.StartPoint + testSegmentLocal.End; // assuming destination.NormalDir == Up
                    return true;
                }
            }

            newPosition = default(Vector2);
            return false;
        }
    }
}