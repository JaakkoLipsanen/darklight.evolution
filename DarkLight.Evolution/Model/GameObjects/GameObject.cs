﻿using System.IO;
using Flai;
using Microsoft.Xna.Framework;

namespace DarkLight.Model.GameObjects
{
    // TODO: Create a "PhysicsBody" for each game object and create "PhysicsSystem" or something for each room? Not sure how to fit tiles into it though..
    public abstract class GameObject
    {
        private readonly UID _uid;
        protected readonly GameDimension _gameDimension; // Should this be changeable..? Atm Player can't be GameDimension, but that might be okay.. or not..
        protected Vector2 _position;

        public UID UID
        {
            get { return _uid; }
        }

        public GameDimension GameDimension
        {
            get { return _gameDimension; }
        }

        public Vector2 Position
        {
            get { return _position; }
        }

        public abstract RectangleF CollisionArea { get; } // TODO: PhysicsBody?
        public virtual RectangleF VisualArea { get { return this.CollisionArea; } }

        protected GameObject(UID uid, GameDimension gameDimension, Vector2 position)
        {
            _uid = uid;
            _gameDimension = gameDimension;
            _position = position;
        }

        public virtual void PreUpdate(UpdateContext updateContext) { }
        public virtual void Update(UpdateContext updateContext) { }
        public virtual void PostUpdate(UpdateContext updateContext) { }

        protected static void FromStream(BinaryReader reader, out UID uid, out GameDimension gameDimension, out Vector2 position)
        {
            uid = UID.FromString(reader.ReadString());
            gameDimension = (GameDimension)reader.ReadInt32();
            position = reader.ReadGeneric<Vector2i>() * Tile.Size;
        }
    }
}
