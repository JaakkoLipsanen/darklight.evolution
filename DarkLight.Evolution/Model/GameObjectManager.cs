﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkLight.Model.GameObjects;

namespace DarkLight.Model
{
    public class GameObjectManager : IEnumerable<GameObject>
    {
        private readonly List<GameObject> _gameObjects;
        private readonly Dictionary<UID, GameObject> _gameObjectsById;

        public int Count
        {
            get { return _gameObjects.Count; }
        }

        public GameObjectManager(IEnumerable<GameObject> gameObjects)
        {
            _gameObjects = new List<GameObject>();
            _gameObjectsById = new Dictionary<UID, GameObject>();

            foreach (GameObject gameObject in gameObjects)
            {
                this.Add(gameObject);
            }
        }

        public void Add(GameObject gameObject)
        {
            if (_gameObjectsById.ContainsKey(gameObject.UID))
            {
                throw new ArgumentException("gameObject");
            }

            _gameObjects.Add(gameObject);
            _gameObjectsById.Add(gameObject.UID, gameObject);
        }

        public IEnumerable<T> GetOfType<T>()
           where T : GameObject
        {
            return _gameObjects.OfType<T>();
        }

        public IEnumerable<GameObject> GetGameObjects(GameDimension gameDimension)
        {
            return _gameObjects.Where(gameObject => gameObject.GameDimension.IsFlagSet(gameDimension));
        }

        public GameObject GetGameObjectById(string id)
        {
            return _gameObjectsById[id];
        }

        public GameObject this[int index]
        {
            get { return _gameObjects[index]; }
        }

        #region Implementation of IEnumerable<T>

        public IEnumerator<GameObject> GetEnumerator()
        {
            return _gameObjects.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _gameObjects.GetEnumerator();
        }

        #endregion
    }
}
