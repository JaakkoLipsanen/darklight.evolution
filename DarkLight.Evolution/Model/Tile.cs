﻿using Flai;
using Microsoft.Xna.Framework;

namespace DarkLight.Model
{
    public enum TileType
    {
        Air = 0,
        Solid,
        Lava,
    }

    public static class Tile
    {
        public const int Size = 24; // this should be totally irrelevant, right? so maybe some small number could be good (1? 2? 4?) to have good precision

        public static int WorldToTileCoordinate(float x)
        {
            return x >= 0 ? (int)(x / Tile.Size) : (int)(FlaiMath.Floor(x / Tile.Size));
        }

        public static Vector2i WorldToTileCoordinate(Vector2 v)
        {
            return new Vector2i(
                Tile.WorldToTileCoordinate(v.X),
                Tile.WorldToTileCoordinate(v.Y));
        }

        public static RectangleF GetTileBounds(int x, int y)
        {
            return new RectangleF(x * Tile.Size, y * Tile.Size, Tile.Size, Tile.Size);
        }

        // This method should return whether tile is walkable or not
        public static bool IsSolid(TileType type)
        {
            return type == TileType.Solid;
        }

        public static bool IsKilling(TileType tile)
        {
            return tile == TileType.Lava;
        }

        public static bool DestroysBullet(TileType type)
        {
            return type == TileType.Solid || type == TileType.Lava;
        }

        public static bool PreventsDimensionChange(TileType tile)
        {
            return tile == TileType.Lava || tile == TileType.Solid;
        }

        public static bool IsVisible(TileType tile)
        {
            return tile != TileType.Air;
        }
    }
}
