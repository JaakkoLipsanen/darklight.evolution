﻿using System;
using Microsoft.Xna.Framework;

namespace DarkLight.Model
{
    [Flags]
    public enum GameDimension
    {
        Dark = 2,
        Light = 4,
        Both = Dark | Light,
    }

    public static class GameDimensionExtensions
    {
        public static Color ToColor(this GameDimension gameDimension)
        {
            switch (gameDimension)
            {
                case GameDimension.Dark:
                    return new Color(32, 32, 32);

                case GameDimension.Light:
                    return Color.White;

                default:
                    throw new ArgumentException("Invalid argument");
            }
        }

        public static GameDimension Opposite(this GameDimension gameDimension)
        {
            switch (gameDimension)
            {
                case GameDimension.Dark:
                    return GameDimension.Light;

                case GameDimension.Light:
                    return GameDimension.Dark;

                default:
                    throw new ArgumentException("Invalid argument");
            }
        }

        public static bool IsFlagSet(this GameDimension gameDimension, GameDimension other)
        {
            return (gameDimension & other) == other;
        }
    }
}
