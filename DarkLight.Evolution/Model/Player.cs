﻿
using DarkLight.Model.GameObjects;
using Flai;
using Flai.General;
using Microsoft.Xna.Framework;

namespace DarkLight.Model
{
    // SUGGESTION: Separate "Player" and "Player<Body/Physics/Collision> etc>
    // TODO: Cleanup (possibly remove variables too?)
    // TODO: Because of late-jumping, now if the player is sliding and late-jumps,
    // the player will not "wall-jump" but instead does a normal jump, so horizontal
    // velocity doesn't change. Need to do "_wasSliding" or something like that... enum?
    // !!! TODO: PhysicsBody stuff.. you could make bodies "Triggers" (non-colliding) for example for bullets. and then have "Collision" event which bullets could subscribe to. tadaa. imba
    public class Player
    {
        #region Constants

        // This is currently only used in the Player.Area property, so I *guess* it could be moved there
        // (though since Vector2 can't be const, there'd be "const float PlayerWidth/Height".
        // But on the other hand, "Size" is something that other parts of the code could potentially need
        // later and feels like it's okay that it's public.. So let's keep this public, at least for now
        private static readonly Vector2 Size = new Vector2(Tile.Size * 1, Tile.Size * 1);

        // TODO: i mean come on...
        private const float LateJumpBias = 0.075f;
        private const float LateDimensionChangeBias = 0.1f;
        public const float HorizontalSpeed = Tile.Size * 20;

        #endregion

        #region Variables

        private Room _currentRoom;

        private GameDimension _gameDimension = GameDimension.Light;

        // TODO: uugllyy!! could these all be put in PhysicsBody/PlayerPhysicsBody/something elsE?
        private Vector2 _position;
        private Vector2 _velocity;
        private float _targetHorizontalVelocity = 0f; // ....
        private bool _isJumping = false; // Is jumping opposed to "isFalling" etc // :/ ...

        // omg..
        private float _timeInAir = 0f;
        private float _timeSinceDimensionChangeAttempt = float.MaxValue;

        #endregion

        #region Properties

        private bool IsOnGround
        {
            get { return this.IsTouchingFromDirection(Direction2D.Down); }
        }

        private bool IsSliding
        {
            get { return !this.IsOnGround && (this.IsTouchingFromDirection(Direction2D.Left) || this.IsTouchingFromDirection(Direction2D.Right)); }
        }

        private bool CanChangeDimension
        {
            get { return !_currentRoom.IsOccupied(this.RoundedArea, _gameDimension.Opposite()); }
        }

        public Room CurrentRoom
        {
            get { return _currentRoom; }
        }

        public GameDimension GameDimension
        {
            get { return _gameDimension; }
        }

        public Vector2 Position
        {
            get { return _position; }
        }

        public float TargetHorizontalVelocity
        {
            get { return _targetHorizontalVelocity; }
            set { _targetHorizontalVelocity = value; }
        }

        public RectangleF Area
        {
            get { return new RectangleF(_position.X - Player.Size.X / 2f, _position.Y - Player.Size.Y / 2f, Player.Size.X, Player.Size.Y); }
        }

        public RectangleF RoundedArea
        {
            get { return RectangleF.GetRounded(this.Area); } // todo: is this property needed...? probably not..
        }

        // TODO: all this stuff is just for controller.. could it be moved to be somehow less visible/only visible to controller?
        public bool CanJump
        {
            get { return this.IsOnGround || this.IsSliding || (_timeInAir < Player.LateJumpBias); }
        }

        // TODO: Look CanJump
        public bool IsJumping
        {
            get { return _isJumping; }
        }

        #endregion

        public Player(Room startingRoom, Vector2 startingPosition)
        {
            _currentRoom = startingRoom;
            _position = startingPosition;
        }

        #region Public Methods

        public void Update(UpdateContext updateContext)
        {
            Vector2 previousPosition = _position;
            this.UpdateTimeInAir(updateContext);
            this.CheckForLateDimensionChange(updateContext);

            this.UpdateGravity(updateContext);
            this.UpdateHorizontalVelocity(updateContext);
            this.HandleCollisions(updateContext);
            this.UpdateIsJumping(updateContext);

            this.CheckDidMoveThroughPortal(updateContext, previousPosition);
        }

        private void CheckDidMoveThroughPortal(UpdateContext updateContext, Vector2 previousPosition)
        {
            Segment2D movementSegment = new Segment2D(previousPosition, _position);
            foreach (Portal portal in _currentRoom.GameObjects.GetOfType<Portal>())
            {
                Vector2 newPosition;
                if (portal.IsSegmegntIntersect(movementSegment, out newPosition))
                {
                    _currentRoom = portal.DestRoom;
                    _position = newPosition;
                    if (portal.NormalDirection != portal.Destination.NormalDirection.Inverse())
                    {
                        if (portal.NormalDirection == portal.Destination.NormalDirection)
                        {
                            if (portal.NormalDirection.ToAlignment() == Alignment.Horizontal)
                            {
                                _velocity.X *= -1;
                            }
                            else
                            {
                                _velocity.Y *= -1;
                            }
                        }
                    }

                    return;
                }
            }
        }

        private void CheckForLateDimensionChange(UpdateContext updateContext)
        {
            _timeSinceDimensionChangeAttempt += updateContext.DeltaSeconds;
            if (_timeSinceDimensionChangeAttempt < Player.LateDimensionChangeBias && this.CanChangeDimension)
            {
                _gameDimension = _gameDimension.Opposite();
                _timeSinceDimensionChangeAttempt = float.MaxValue;
            }
        }

        private void UpdateTimeInAir(UpdateContext updateContext)
        {
            if (this.IsOnGround || this.IsSliding)
            {
                _timeInAir = 0;
            }
            else
            {
                _timeInAir += updateContext.DeltaSeconds;
            }
        }

        public void Jump()
        {
            if (!this.IsTouchingFromDirection(Direction2D.Up))
            {
                if (this.CanJump)
                {
                    const float JumpPower = -Tile.Size * 32.5f;
                    _velocity.Y = JumpPower;
                    _isJumping = true;
                }

                // Wall jump
                if (!this.IsOnGround)
                {
                    if (this.IsTouchingFromDirection(Direction2D.Left))
                    {
                        _targetHorizontalVelocity = Player.HorizontalSpeed;
                        _velocity.X = _targetHorizontalVelocity;
                    }
                    else if (this.IsTouchingFromDirection(Direction2D.Right))
                    {
                        _targetHorizontalVelocity = -Player.HorizontalSpeed;
                        _velocity.X = _targetHorizontalVelocity;
                    }
                }
            }
        }


        public void Fall(UpdateContext updateContext)
        {
            if (this.IsJumping)
            {
                if (_velocity.Y < 0)
                {
                    _velocity.Y -= Tile.Size * 4 * updateContext.DeltaSeconds;
                }

                const float FallPower = Tile.Size * 0;
                _velocity.Y += FallPower * updateContext.DeltaSeconds;
            }
        }

        public bool TryChangeDimension()
        {
            if (this.CanChangeDimension)
            {
                _timeSinceDimensionChangeAttempt = float.MaxValue;
                _gameDimension = _gameDimension.Opposite();
                return true;
            }

            _timeSinceDimensionChangeAttempt = 0f;
            return false;
        }

        #endregion

        #region Private Methods

        #region Update Gravity

        private void UpdateGravity(UpdateContext updateContext)
        {
            // Set vertical velocity to zero if the player is on ground AND the vertical velocity is "towards ground" at the moment
            if (this.IsOnGround && _velocity.Y >= 0)
            {
                _velocity.Y = 0;
            }
            else
            {
                this.CheckIfIsCollidingFromTop(updateContext);

                float gravity = World.Gravity * updateContext.DeltaSeconds;
                if (this.IsSliding)
                {
                    gravity *= 0.75f;
                }

                const float MaximumVerticalVelocity = Tile.Size * 95;
                _velocity.Y = MathHelper.Clamp(_velocity.Y + gravity, -MaximumVerticalVelocity, MaximumVerticalVelocity);
            }
        }

        #endregion

        #region Handle Collisions

        private void HandleCollisions(UpdateContext updateContext)
        {
            // BLAAH!! This doesn't really belong here...
            if (!(this.IsSliding && FlaiMath.Abs(_velocity.X) < Player.HorizontalSpeed / 2f))
            {
                _position.X += _velocity.X * updateContext.DeltaSeconds;
            }
            this.HandleCollisions(updateContext, Alignment.Horizontal);

            _position.Y += _velocity.Y * updateContext.DeltaSeconds;
            this.HandleCollisions(updateContext, Alignment.Vertical);
        }

        private void HandleCollisions(UpdateContext updateContext, Alignment alignment)
        {
            RectangleF playerBounds = this.RoundedArea;

            // Static Tiles          
            int leftTile = Tile.WorldToTileCoordinate(playerBounds.Left);
            int topTile = Tile.WorldToTileCoordinate(playerBounds.Top);
            int rightTile = Tile.WorldToTileCoordinate(FlaiMath.Ceiling(playerBounds.Right / Tile.Size) * Tile.Size) - 1;
            int bottomTile = Tile.WorldToTileCoordinate(FlaiMath.Ceiling(playerBounds.Bottom / Tile.Size) * Tile.Size) - 1;

            ReadOnlyTileMap<TileType> tileMap = _currentRoom.TileMap.GetTileMapForDimension(_gameDimension);
            for (int y = topTile; y <= bottomTile; y++)
            {
                for (int x = leftTile; x <= rightTile; x++)
                {
                    TileType tile = tileMap.IsInsideBounds(x, y) ? tileMap[x, y] : TileType.Solid;
                    if (Tile.IsKilling(tile))
                    {
                        this.OnDeath();
                        return;
                    }
                    else if (Tile.IsSolid(tile))
                    {
                        Vector2 depth;
                        if (playerBounds.GetIntersectionDepth(Tile.GetTileBounds(x, y), alignment, out depth))
                        {
                            _position += depth;
                            playerBounds = this.RoundedArea;
                        }
                    }
                }
            }
        }

        #endregion

        #region Update Horizontal Velocity

        private void UpdateHorizontalVelocity(UpdateContext updateContext)
        {
            const float HorizontalAccelerationMultiplierGround = 6f;
            const float HorizontalAccelerationMultiplierAir = 4f;

            float targetHorizontalVelocity = (this.IsJumping && _targetHorizontalVelocity == 0) ? _velocity.X : _targetHorizontalVelocity;

            float accelerationMultiplier = this.IsOnGround ? HorizontalAccelerationMultiplierGround : HorizontalAccelerationMultiplierAir;
            Sign movementDirection = FlaiMath.GetSign(targetHorizontalVelocity - _velocity.X);

            float horizontalVelocityChange = Player.HorizontalSpeed * accelerationMultiplier * movementDirection.ToInt() * updateContext.DeltaSeconds;

            _velocity.X += horizontalVelocityChange;
            if (FlaiMath.GetSign(targetHorizontalVelocity - _velocity.X) != movementDirection)
            {
                _velocity.X = targetHorizontalVelocity;
            }

            // If player can't move that way, set velocity.X = 0
            if (_velocity.X != 0 && this.IsTouchingFromDirection(_velocity.X > 0 ? Direction2D.Right : Direction2D.Left))
            {
                _velocity.X = 0;
            }
        }

        #endregion

        #region Update Is Jumping

        private void UpdateIsJumping(UpdateContext updateContext)
        {
            if (_isJumping && this.IsOnGround)
            {
                _isJumping = false;
            }
        }

        #endregion

        #region Misc

        private bool IsTouchingFromDirection(Direction2D direction)
        {
            return _currentRoom.IsOccupied(this.RoundedArea.GetSideAreaPlusOne(direction), _gameDimension);
        }

        private void CheckIfIsCollidingFromTop(UpdateContext updateContext)
        {
            if (this.IsTouchingFromDirection(Direction2D.Up))
            {
                this.HandleCollisions(updateContext, Alignment.Vertical);
                _velocity.Y = FlaiMath.Max(0, _velocity.Y); // Set vertical velocity to zero unless the player is falling down
            }
        }

        private void OnDeath()
        {
        }

        #endregion

        #endregion
    }
}
