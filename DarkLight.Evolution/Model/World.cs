﻿using Flai;

namespace DarkLight.Model
{
    public class World
    {
        public const float Gravity = Tile.Size * 75f;

        private readonly Player _player;
        private readonly RoomManager _roomManager;

        public Player Player
        {
            get { return _player; }
        }

        // WorldMap vs Map?? _world.Map vs _world.WorldMap.. Blaahh.. Maybe just Map
        public RoomManager RoomManager
        {
            get { return _roomManager; }
        }

        public Room CurrentRoom
        {
            get { return _player.CurrentRoom; }
        }

        public GameDimension ActiveGameDimension
        {
            get { return _player.GameDimension; }
        }


        public World(Player player, RoomManager roomManager)
        {
            _player = player;
            _roomManager = roomManager;
        }

        public void Update(UpdateContext updateContext)
        {
            _player.Update(updateContext);
        }
    }
}