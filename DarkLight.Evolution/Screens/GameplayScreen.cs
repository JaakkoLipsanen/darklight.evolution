﻿using DarkLight.Controller;
using DarkLight.IO;
using DarkLight.Model;
using DarkLight.View;
using Flai;
using Flai.Graphics;
using Flai.ScreenManagement;

namespace DarkLight.Screens
{
    // todo: move Level/LevelRenderer/LevelController to deeper? "LevelManager" or something? could be a good idea 
    public class GameplayScreen : GameScreen
    {
        private readonly string _levelPath;
        private Level _level;
        private LevelRenderer _levelRenderer;
        private LevelController _levelController;

        public GameplayScreen(string levelPath)
        {
            _levelPath = levelPath;
            Cursor.IsVisible = false;
        }

        protected override void LoadContent()
        {
            _level = LevelLoader.Load(_levelPath);
            _levelRenderer = new LevelRenderer(_level);
            _levelController = new LevelController(_level);

            _levelRenderer.LoadContent();
        }

        protected override void Update(UpdateContext updateContext, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            if (this.IsActive)
            {
                _levelController.Control(updateContext);
                _level.Update(updateContext);
                _levelRenderer.Update(updateContext);
            }
        }

        protected override void Draw(GraphicsContext graphicsContext)
        {
            _levelRenderer.Draw(graphicsContext);
        }
    }
}
