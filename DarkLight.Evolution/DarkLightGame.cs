using System;
using System.Diagnostics;
using System.Windows.Forms;
using DarkLight.IO;
using DarkLight.Screens;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Cursor = Flai.Cursor;

namespace DarkLight
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class DarkLightGame : FlaiGame
    {
        public DarkLightGame()
        {
            _contentProvider.RootDirectory = "DarkLight.Evolution.Content";

            base.IsFixedTimeStep = true;
            base.WindowTitle = "DarkLight.Evolution";
            base.ClearColor = Color.Black;
            Cursor.IsVisible = true;
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _screenManager.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            _screenManager.PreDraw(graphicsContext);
            _screenManager.Draw(graphicsContext);
        }

        protected override void AddInitialScreens()
        {
            string levelPath;
            if (LevelFileHelper.FindLevelPathFromArguments(_commandLineArgs, out levelPath))
            {
                _screenManager.AddScreen(new GameplayScreen(levelPath));
            }
            else
            {
                // <else go to main menu>
                _screenManager.AddScreen(new GameplayScreen(@"F:\Users\jaakko\Desktop\s.darklight"));
            }
        }

        protected override void InitializeGraphicsSettings()
        {
            _graphicsDeviceManager.PreferredBackBufferWidth = 1280;
            _graphicsDeviceManager.PreferredBackBufferHeight = 720;
            _graphicsDeviceManager.PreferMultiSampling = true;
            _graphicsDeviceManager.SynchronizeWithVerticalRetrace = true;
        }
    }

    #region Entry Point

    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            if (Debugger.IsAttached)
            {
                using(DarkLightGame game = new DarkLightGame())
                {
                    game.Run();
                }
            }
            else
            {
                try
                {
                    using (DarkLightGame game = new DarkLightGame())
                    {
                        game.Run();
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
        }
    }

    #endregion
}