﻿
namespace DarkLight.Editor.WPF
{
    public static class EditorGlobals
    {
        public const string FileExtension = ".darklight";
        public const string FileExtensionFilter = "Dark Light Level(*" + EditorGlobals.FileExtension + ")|*" + EditorGlobals.FileExtension; // blaah.. cleaner with string.Format but can't be const then
        public const string GameExecutableFilter = "Dark Light Game|DarkLight*.exe";
    }
}
