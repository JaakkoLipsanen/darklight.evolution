﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using DarkLight.Editor.Tools;
using DarkLight.Editor.WPF.Properties;
using Flai.DataStructures;
using Flai.Editor;
using Flai.Editor.Misc;
using Microsoft.Win32;

namespace DarkLight.Editor.WPF.ViewModel
{
    public class EditorGameViewModel : GameViewModelBase<EditorGame>
    {
        #region Fields

        // TODO: Put these to some other static class?
        private readonly ReadOnlyArray<TileType> _tiles = new ReadOnlyArray<TileType>(
            new[] { TileType.Solid, TileType.Lava });

        private readonly ReadOnlyArray<Type> _gameObjectTypes = new ReadOnlyArray<Type>(
            new[] { typeof(Cannon), typeof(MovingPlatform) });

        private readonly ReadOnlyArray<GameDimension> _gameDimensions = new ReadOnlyArray<GameDimension>(
            new[] { GameDimension.Light, GameDimension.Dark, GameDimension.Both });

        private string _currentLevelPath = null;

        #endregion

        #region Properties

        public EditorUser User
        {
            get { return _game.Level.User; }
        }

        public EditorWorld World
        {
            get { return _game.Level.World; }
        }

        public ReadOnlyArray<TileType> TileList
        {
            get { return _tiles; }
        }

        public ReadOnlyArray<Type> GameObjectTypes
        {
            get { return _gameObjectTypes; }
        }

        public ReadOnlyArray<GameDimension> GameDimensions
        {
            get { return _gameDimensions; }
        }

        public DelegateCommand PlayLevelCommand { get; private set; }
        public DelegateCommand NewLevelCommand { get; private set; }
        public DelegateCommand SaveLevelCommand { get; private set; }
        public DelegateCommand SaveLevelAsCommand { get; private set; }
        public DelegateCommand OpenLevelCommand { get; private set; }
        public DelegateCommand ChangeGamePathCommand { get; private set; }

        public DelegateCommand AddEmptyRoomCommand { get; private set; }
        public DelegateCommand RemoveSelectedRoomCommand { get; private set; }

        #endregion

        public EditorGameViewModel()
            : base(new EditorGame())
        {
            _game.PropertyChanged += this.GamePropertyChanged;

            this.PlayLevelCommand = new DelegateCommand(this.PlayLevel);
            this.NewLevelCommand = new DelegateCommand(this.LoadNewLevel);
            this.SaveLevelCommand = new DelegateCommand(() => this.SaveLevel());
            this.SaveLevelAsCommand = new DelegateCommand(() => this.SaveLevelAs());
            this.OpenLevelCommand = new DelegateCommand(this.LoadLevel);
            this.ChangeGamePathCommand = new DelegateCommand(() => this.SelectGameExecutablePath());

            this.AddEmptyRoomCommand = new DelegateCommand(this.AddEmptyRoom);
            this.RemoveSelectedRoomCommand = new DelegateCommand(this.RemoveSelectedRoom, () => this.World.Rooms.Count > 1);
        }

        private void AddEmptyRoom()
        {
            this.World.Rooms.AddEmptyRoom();
        }

        private void RemoveSelectedRoom()
        {
            this.World.Rooms.Remove(this.World.Rooms.SelectedRoom);
        }

        private void GamePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Level")
            {
                base.OnPropertyChanged("");
            }
        }

        #region OnXXX List Selection Changed

        public void OnGameObjectTemplateListSelectionChanged(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                this.User.CurrentlyAddingTile = null;
                this.User.ToolManager.SetActiveTool<GameObjectTool>();
            }
        }

        public void OnTileListSelectionChanged(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                this.User.CurrentlyAddingGameObjectType = null;
                if (!EditorToolHelper.IsTileTool(this.User.ToolManager.ActiveTool))
                {
                    this.User.ToolManager.SetActiveTool<TileBrushTool>();
                }
            }
        }

        public void OnToolListSelectionChanged(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 0)
            {
                // Set the currently adding tile to non-null value if the selected tool is for manipulating tiles
                if (EditorToolHelper.IsTileTool(e.AddedItems[0].GetType()))
                {
                    if (this.User.CurrentlyAddingTile == null)
                    {
                        this.User.CurrentlyAddingTile = TileType.Solid;
                    }
                }
                else
                {
                    this.User.CurrentlyAddingTile = null;
                }

                // Unselect all game objects if the tool is not for manipulating game objects
                if (!EditorToolHelper.IsGameObjectTool(e.AddedItems[0].GetType()))
                {
                    this.User.SelectedGameObject = null;
                }
            }
        }

        public void OnRoomGameObjectSelectionChanged(SelectionChangedEventArgs e)
        {
            // Set the active tool to GameObjectTool if the current tool is not for manipulating game objects
            if (e.AddedItems.Count != 0)
            {
                if (!EditorToolHelper.IsGameObjectTool(this.User.ToolManager.ActiveTool))
                {
                    this.User.ToolManager.SetActiveTool<GameObjectTool>();
                }
            }
        }

        #endregion

        #region Play/Save/Open Level

        // Save the level and try to start the .exe
        private void PlayLevel()
        {
            if (this.SaveLevel())
            {
                if (File.Exists(SettingsProxy.GamePath) || this.SelectGameExecutablePath())
                {
                    Process.Start(SettingsProxy.GamePath, _currentLevelPath);
                }
            }
        }

        // Save level by trying to save to _currentLevelPath if possible
        private bool SaveLevel()
        {
            if (File.Exists(_currentLevelPath))
            {
                this.SaveLevel(_currentLevelPath);
                return true;
            }

            return this.SaveLevelAs();
        }

        // Shows dialog which prompts user to select the path to game executable
        private bool SelectGameExecutablePath()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = EditorGlobals.GameExecutableFilter,
                InitialDirectory = string.IsNullOrWhiteSpace(SettingsProxy.GamePath) ? null : Path.GetDirectoryName(SettingsProxy.GamePath),
            };

            if (openFileDialog.ShowDialog() == true)
            {
                SettingsProxy.GamePath = openFileDialog.FileName;
                return true;
            }

            return false;
        }

        // Shows dialog which prompts user to select the path where to save the level
        private bool SaveLevelAs()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                DefaultExt = EditorGlobals.FileExtension,
                Filter = EditorGlobals.FileExtensionFilter,
                InitialDirectory = SettingsProxy.LevelFolderPath
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                this.SaveLevel(saveFileDialog.FileName);
                SettingsProxy.LevelFolderPath = Path.GetDirectoryName(saveFileDialog.FileName);
                return true;
            }

            return false;
        }

        // Shows dialog which prompts user to select the path which level to load
        private void LoadLevel()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = EditorGlobals.FileExtensionFilter,
                InitialDirectory = SettingsProxy.LevelFolderPath
            };

            if (openFileDialog.ShowDialog() == true)
            {
                this.LoadLevel(openFileDialog.FileName);
                SettingsProxy.LevelFolderPath = Path.GetDirectoryName(openFileDialog.FileName);
            }
        }

        // Saves the level to the path specified as a parameter
        private void SaveLevel(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Create))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    this.Game.Level.Save(writer);
                    _currentLevelPath = filePath;
                }
            }
        }

        // Loads the level from the path specified as a parameter
        private void LoadLevel(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    EditorLevel level = EditorLevel.FromStream(reader);
                    if (level == null)
                    {
                        MessageBox.Show("There was an error opening the level");
                    }
                    else
                    {
                        this.Game.Level = level;
                        _currentLevelPath = filePath;
                    }
                }
            }
        }

        private void LoadNewLevel()
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to create a new level? All unsaved progress will be lost", "Warning", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                this.Game.Level = EditorLevel.CreateEmpty();
            }
        }

        #endregion
    }
}