﻿using Flai.Editor.Controls;

namespace DarkLight.Editor.WPF.Controls
{
    // Since XAML doesn't support controls with generic parameters, I have to wrap
    // GamePanelBase to non-generic class
    public class GamePanel : GamePanelBase<EditorGame>
    {
    }
}
