﻿using System.Windows;
using DarkLight.Editor.WPF.Properties;
using DarkLight.Editor.WPF.Windows;

namespace DarkLight.Editor.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            this.MainWindow = new MainWindow();
            this.MainWindow.Show();
        }
    }
}
