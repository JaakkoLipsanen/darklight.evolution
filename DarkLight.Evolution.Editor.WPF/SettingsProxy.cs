﻿using DarkLight.Editor.WPF.Properties;

namespace DarkLight.Editor.WPF
{
    public static class SettingsProxy
    {
        public static string GamePath
        {
            get { return Settings.Default["GamePath"] as string; }
            set
            {
                Settings.Default["GamePath"] = value;
                Settings.Default.Save();
            }
        }

        public static string LevelFolderPath
        {
            get { return Settings.Default["LevelFolderPath"] as string; }
            set
            {
                Settings.Default["LevelFolderPath"] = value; 
                Settings.Default.Save();
            }
        }
    }
}
