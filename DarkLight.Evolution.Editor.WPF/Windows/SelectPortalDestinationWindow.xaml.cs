﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using Flai;

namespace DarkLight.Editor.WPF.Windows
{
    // TODO: Maybe add an indication to the portal list if the portal has a destination?
    public partial class SelectPortalDestinationWindow : Window
    {
        private readonly Portal _portal; // better name?

        public SelectPortalDestinationWindow(EditorWorld world, Portal portal)
        {
            _portal = portal;
            this.Resources["PortalToVisibilityConverter"] = new PortalToVisibilityConverter(_portal);
            this.Resources["RoomToVisibilityConverter"] = new RoomToVisibilityConverter(_portal);

            this.InitializeComponent();
            base.DataContext = world;

            // TODO: Initial room to portal's own room?
            this.ChangeSelectedRoom(portal.Destination == null ? portal.OwnerRoom : portal.Destination.OwnerRoom);
        }

        private void ChangeSelectedRoom(EditorRoom room)
        {
            _roomListView.SelectedValue = room;
            _portalListView.Items.Clear();

            IEnumerable<Portal> query = room.GameObjects.GetAllOfType<Portal>().Where(p =>
                p != _portal &&
                p.Length == _portal.Length &&
                p.NormalDirection.ToAlignment() == _portal.NormalDirection.ToAlignment());

            // TODO: Maybe show all, but just put invalid portals read-only/not selectable
            foreach (Portal portal in query)
            {
                _portalListView.Items.Add(portal);
            }
        }

        private void OnRoomListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                return;
            }

            EditorRoom selectedRoom = e.AddedItems[0] as EditorRoom;
            if (selectedRoom == null)
            {
                return;
            }

            this.ChangeSelectedRoom(selectedRoom);
        }

        private void OnOkButtonClicked(object sender, RoutedEventArgs e)
        {
            Portal selectedPortal = _portalListView.SelectedValue as Portal;
            if (selectedPortal != null)
            {
                // TODO: Create an action
                _portal.Destination = selectedPortal;
                if (_twoWayCheckBox.IsChecked == true)
                {
                    selectedPortal.Destination = _portal;
                }
            }

            this.Close();
        }

        private void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #region Portal Converter

        private class PortalToVisibilityConverter : IMultiValueConverter
        {
            private readonly Portal _activeChangePortal;

            public PortalToVisibilityConverter(Portal portal)
            {
                _activeChangePortal = portal;
            }

            #region IMultiValueConverter Members

            public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
            {
                Portal listPortal = (Portal)values[0];
                Rectangle rect = (Rectangle)values[1];
                object selectedValue = values[2];

                bool isCurrentDestination = listPortal == _activeChangePortal.Destination;
                bool isSelected = listPortal == selectedValue;
                if (isCurrentDestination && isSelected)
                {
                    rect.Fill = new SolidColorBrush(Color.FromRgb(192, 0, 0));
                }
                else if (isCurrentDestination)
                {
                    rect.Fill = Brushes.Red;
                }
                else if (isSelected)
                {
                    rect.Fill = Brushes.Black;
                }
                else
                {
                    return Visibility.Hidden;
                }

                return Visibility.Visible;
            }

            public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
            {
                throw new NotSupportedException();
            }

            #endregion
        }

        #endregion

        #region Room Converter

        private class RoomToVisibilityConverter : IMultiValueConverter
        {
            private readonly Portal _activeChangePortal;

            public RoomToVisibilityConverter(Portal portal)
            {
                _activeChangePortal = portal;
            }

            #region IMultiValueConverter Members

            public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
            {
                EditorRoom listRoom = (EditorRoom)values[0];
                Rectangle rect = (Rectangle)values[1];
                object selectedValue = values[2];

                bool isCurrentDestination = _activeChangePortal.Destination != null &&
                                            listRoom == _activeChangePortal.Destination.OwnerRoom;
                bool isSelected = listRoom == selectedValue;
                if (isCurrentDestination && isSelected)
                {
                    rect.Fill = new SolidColorBrush(Color.FromRgb(192, 0, 0));
                }
                else if (isCurrentDestination)
                {
                    rect.Fill = Brushes.Red;
                }
                else if (isSelected)
                {
                    rect.Fill = Brushes.Black;
                }
                else
                {
                    return Visibility.Hidden;
                }

                return Visibility.Visible;
            }

            public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
            {
                throw new NotSupportedException();
            }

            #endregion
        }

        #endregion

        private void OnResetButtonClicked(object sender, RoutedEventArgs e)
        {
            _portal.Destination = null;
            this.Close();
        }
    }
}