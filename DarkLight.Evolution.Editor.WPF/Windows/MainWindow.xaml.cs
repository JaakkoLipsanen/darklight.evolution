﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DarkLight.Editor.Model.GameObjects;
using DarkLight.Editor.WPF.ViewModel;
using Flai.Editor;
using Xceed.Wpf.Toolkit.PropertyGrid;

namespace DarkLight.Editor.WPF.Windows
{
    public partial class MainWindow : Window
    {
        private readonly EditorGameViewModel _viewModel;
        public MainWindow()
        {
            this.InitializeComponent();

            _viewModel = new EditorGameViewModel();
            base.DataContext = _viewModel;
        }

        private void OnToolBarLoaded(object sender, RoutedEventArgs e)
        {
            WpfHelper.RemoveToolBarOverflow(sender as ToolBar);
        }

        private void DisableKeyboardInput(object sender, KeyEventArgs e)
        {
            WpfHelper.DisableKeyboardInput(e); // before, I checked if key was arrow! should be okay though
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            _mainDockPanel.Focus();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to close the editor? All unsaved progress will be lost", "Warning", MessageBoxButton.YesNo);
            e.Cancel = result != MessageBoxResult.Yes; // cancel if not "Yes"
            // TODO: Try to save? "Save? Yes/No/Cancel" maybe?
        }

        private void OnPropetyGridPortalDestinationButtonClicked(object sender, RoutedEventArgs e)
        {
            // Extract the Portal instance from Tag (set in XAML)
            object tag = ((FrameworkElement)sender).Tag;
            if (tag is PropertyItem)
            {
                Portal portal = ((PropertyItem)tag).Instance as Portal;
                if (portal == null)
                {
                    return;
                }

                SelectPortalDestinationWindow window = new SelectPortalDestinationWindow(_viewModel.World, portal);
                window.ShowDialog();
            }
        }

        /* List SelectionChanged events */
        private void OnGameObjectTemplateListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _viewModel.OnGameObjectTemplateListSelectionChanged(e);
        }

        private void OnTileListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _viewModel.OnTileListSelectionChanged(e);
        }

        private void OnToolListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _viewModel.OnToolListSelectionChanged(e);
        }

        private void OnRoomGameObjectSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _viewModel.OnRoomGameObjectSelectionChanged(e);
        }
        /* ----- */
    }
}
