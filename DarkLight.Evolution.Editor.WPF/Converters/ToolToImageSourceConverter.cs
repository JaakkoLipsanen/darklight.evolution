﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using DarkLight.Editor.Tools;

namespace DarkLight.Editor.WPF.Converters
{
    public class ToolToImageSourceConverter : IValueConverter
    {
        private static ToolToImageSourceConverter _default;
        public static ToolToImageSourceConverter Default
        {
            get { return _default ?? (_default = new ToolToImageSourceConverter()); }
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Type type = value.GetType();
            if (type == typeof(FillTool))
            {
                return "/Resources/Tools/PaintCanIcon.png";
            }
            else if (type == typeof(TileBrushTool))
            {
                return "/Resources/Tools/TileBrushIcon.png";
            }
            else if (type == typeof(ResizeLevelTool))
            {
                return "/Resources/Tools/ResizeLevelIcon.png";
            }
            else if (type == typeof(SelectionTool))
            {
                return "/Resources/Tools/SelectionIcon.png";
            }
            else if (type == typeof(GameObjectTool))
            {
                return "/Resources/Tools/GameObjectIcon.png";
            }
            else if (type == typeof(MovingPlatformTool))
            {
                return "/Resources/Tools/MovingPlatformIcon.png";
            }
            else if (type == typeof (PortalTool))
            {
                return "/Resources/Tools/PortalIcon.png";
            }

            return "/Resources/UnknownIcon.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
}
