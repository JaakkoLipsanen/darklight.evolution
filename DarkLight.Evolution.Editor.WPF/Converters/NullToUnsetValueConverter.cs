﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DarkLight.Editor.WPF.Converters
{
    public class NullToUnsetValueConverter : IValueConverter
    {
        private static NullToUnsetValueConverter _default;
        public static NullToUnsetValueConverter Default
        {
            get { return _default ?? (_default = new NullToUnsetValueConverter()); }
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ?? DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
}