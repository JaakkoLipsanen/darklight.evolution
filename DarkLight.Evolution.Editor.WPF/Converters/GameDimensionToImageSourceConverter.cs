﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using DarkLight.Editor.Model;

namespace DarkLight.Editor.WPF.Converters
{
    public class GameDimensionToImageSourceConverter : IValueConverter
    {
        private static readonly Color LightColor = Colors.White;
        private static readonly Color DarkColor = new Color { R = 64, G = 64, B = 64, A = 255 };

        private static GameDimensionToImageSourceConverter _default;
        public static GameDimensionToImageSourceConverter Default
        {
            get { return _default ?? (_default = new GameDimensionToImageSourceConverter()); }
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is GameDimension)
            {
                GameDimension gameDimension = (GameDimension)value;
                if (gameDimension == GameDimension.Dark)
                {
                    return new SolidColorBrush(GameDimensionToImageSourceConverter.DarkColor);
                }
                else if (gameDimension == GameDimension.Light)
                {
                    return new SolidColorBrush(GameDimensionToImageSourceConverter.LightColor);
                }
                else if (gameDimension == GameDimension.Both)
                {
                    return new LinearGradientBrush(GameDimensionToImageSourceConverter.LightColor, GameDimensionToImageSourceConverter.DarkColor, new Point(0.49, 0.49), new Point(0.51, 0.51));
                }
            }

            return new SolidColorBrush(Colors.Purple);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
}
