﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using DarkLight.Editor.Model.GameObjects;

namespace DarkLight.Editor.WPF.Converters
{
    public class GameObjectTypeToImageSourceConverter : IValueConverter
    {
        private static GameObjectTypeToImageSourceConverter _default;
        public static GameObjectTypeToImageSourceConverter Default
        {
            get { return _default ?? (_default = new GameObjectTypeToImageSourceConverter()); }
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Type type = value as Type;
            if (type == typeof(Cannon))
            {
                return "/Resources/GameObjects/CannonIcon.png";
            }
            else if (type == typeof(MovingPlatform))
            {
                return "/Resources/GameObjects/MovingPlatformIcon.png";
            }
            else if (type == typeof (Portal))
            {
                return "/Resources/Tools/PortalIcon.png";
            }

            return "/Resources/UnknownIcon.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
}
