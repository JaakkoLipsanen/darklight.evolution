﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using DarkLight.Editor.Model;

namespace DarkLight.Editor.WPF.Converters
{
    public class TileTypeToImageSourceConverter : IValueConverter
    {
        private static TileTypeToImageSourceConverter _default;
        public static TileTypeToImageSourceConverter Default
        {
            get { return _default ?? (_default = new TileTypeToImageSourceConverter()); }
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((TileType)value)
            {
                case TileType.Solid:
                    return "/Resources/Tiles/SolidTile.png";

                case TileType.Lava:
                    return "/Resources/Tiles/LavaTile.png";

                case TileType.Air:
                    return "/Resources/Tiles/AirTile.png";

                default:
                    return "/Resources/UnknownIcon.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
}
