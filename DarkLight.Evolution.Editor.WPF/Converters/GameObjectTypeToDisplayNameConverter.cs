﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Flai;

namespace DarkLight.Editor.WPF.Converters
{
    public class GameObjectTypeToDisplayNameConverter : IValueConverter
    {
        private static GameObjectTypeToDisplayNameConverter _default;
        public static GameObjectTypeToDisplayNameConverter Default
        {
            get { return _default ?? (_default = new GameObjectTypeToDisplayNameConverter()); }
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Common.AddSpaceBeforeCaps(((Type)value).Name);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
}
