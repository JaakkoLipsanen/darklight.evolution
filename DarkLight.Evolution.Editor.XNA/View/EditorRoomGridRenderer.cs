﻿using DarkLight.Editor.Model;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.View
{
    public class EditorRoomGridRenderer : FlaiRenderer
    {
        private readonly EditorRoom _room;
        public EditorRoomGridRenderer(EditorRoom room)
        {
            _room = room;
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            this.DrawRoomOutsideDimmed(graphicsContext);
            this.DrawRoomGrid(graphicsContext);
        }

        private void DrawRoomGrid(GraphicsContext graphicsContext)
        {
            RectangleF cameraArea = _room.Camera.GetArea(graphicsContext.GraphicsDevice);
            int left = (int)FlaiMath.Max(0, cameraArea.Left / Tile.Size);
            int top = (int)FlaiMath.Max(0, cameraArea.Top / Tile.Size);
            int right = (int)FlaiMath.Min(_room.Width, cameraArea.Right / Tile.Size + 1);
            int bottom = (int)FlaiMath.Min(_room.Height, cameraArea.Bottom / Tile.Size + 1);

            const float GridThickness = 1f;
            const float BaseAlpha = 0.8f;

            // Draw vertical lines
            for (int x = left; x < right; x++)
            {
                float alpha = (x % 8) == 0 ? BaseAlpha : ((x % 4 == 0) ? BaseAlpha / 2f : BaseAlpha / 4f);
                graphicsContext.PrimitiveRenderer.DrawLine(graphicsContext, new Vector2i(x, top) * Tile.Size, new Vector2i(x, bottom) * Tile.Size, Color.White * alpha, GridThickness / _room.Camera.Zoom);
            }

            // Draw horizontal lines
            for (int y = top; y < bottom; y++)
            {
                float alpha = (y % 8) == 0 ? BaseAlpha : ((y % 4 == 0) ? BaseAlpha / 2f : BaseAlpha / 4f);
                graphicsContext.PrimitiveRenderer.DrawLine(graphicsContext, new Vector2i(left, y) * Tile.Size, new Vector2i(right, y) * Tile.Size, Color.White * alpha, GridThickness / _room.Camera.Zoom);
            }

            // Draw the room borders bolded
            const float BorderThickness = 2f;
            graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext, new RectangleF(0, 0, _room.Width * Tile.Size, _room.Height * Tile.Size), Color.DimGray, BorderThickness / _room.Camera.Zoom);
        }

        private void DrawRoomOutsideDimmed(GraphicsContext graphicsContext)
        {
            Color color = Color.Black * 0.5f;
            RectangleF cameraArea = _room.Camera.GetArea(graphicsContext.GraphicsDevice);

            Range cameraAreaVerticalRange = new Range(FlaiMath.Max(0, cameraArea.Top), FlaiMath.Min(_room.Height * Tile.Size, cameraArea.Bottom));
            if (cameraArea.Left < 0)
            {
                graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, new RectangleF(cameraArea.Left, cameraAreaVerticalRange.Min, -cameraArea.Left, cameraAreaVerticalRange.Length), color);
            }

            if (cameraArea.Right > _room.Width * Tile.Size)
            {
                graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, new RectangleF(_room.Width * Tile.Size, cameraAreaVerticalRange.Min, cameraArea.Right - _room.Width * Tile.Size, cameraAreaVerticalRange.Length), color);
            }

            if (cameraArea.Top < 0)
            {
                graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, new RectangleF(cameraArea.Left, cameraArea.Top, cameraArea.Width, -cameraArea.Top), color);
            }

            if (cameraArea.Bottom > _room.Height * Tile.Size)
            {
                graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, new RectangleF(cameraArea.Left, _room.Height * Tile.Size, cameraArea.Width, cameraArea.Bottom - _room.Height * Tile.Size), color);
            }
        }
    }
}
