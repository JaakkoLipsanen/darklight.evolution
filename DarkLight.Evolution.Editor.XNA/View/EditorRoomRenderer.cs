﻿using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using Flai;
using Flai.Graphics;

namespace DarkLight.Editor.View
{
    public class EditorRoomRenderer : FlaiRenderer
    {
        private readonly EditorWorld _world;
        private readonly EditorRoom _room;
        private readonly EditorRoomMapRenderer _roomMapRenderer;
        private readonly EditorRoomGridRenderer _roomGridRenderer;

        public EditorRoomRenderer(EditorWorld world, EditorRoom room)
        {
            _world = world;
            _room = room;

            _roomMapRenderer = new EditorRoomMapRenderer(_room);
            _roomGridRenderer = new EditorRoomGridRenderer(_room);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            // Draw the background grid
            _roomGridRenderer.Draw(graphicsContext);
            this.DrawGameObjects(graphicsContext);
        }

        private void DrawGameObjects(GraphicsContext graphicsContext)
        {
            if (_world.ActiveGameDimension == GameDimension.Both)
            {
                this.DrawGameDimension(graphicsContext, GameDimension.Both, EditorRenderer.ForegroundDimensionAlpha);
            }
            else
            {
                this.DrawGameDimension(graphicsContext, _world.ActiveGameDimension.Opposite(), EditorRenderer.BackgroundDimensionAlpha);
                this.DrawGameDimension(graphicsContext, _world.ActiveGameDimension, EditorRenderer.ForegroundDimensionAlpha);
            }
        }

        private void DrawGameDimension(GraphicsContext graphicsContext, GameDimension gameDimension, float alpha)
        {
            _roomMapRenderer.Draw(graphicsContext, gameDimension, alpha);
            this.DrawGameObjects(graphicsContext, gameDimension, alpha);
        }

        private void DrawGameObjects(GraphicsContext graphicsContext, GameDimension gameDimension, float alpha)
        {
            ICamera2D camera = _serviceContainer.GetService<ICameraManager2D>().ActiveCamera;
            RectangleF cameraArea = camera.GetArea(graphicsContext.GraphicsDevice);

            foreach (IGameObject gameObject in _room.GameObjects.GetAllAt(cameraArea, gameDimension))
            {
                gameObject.Draw(graphicsContext, gameDimension, alpha);
            }
        }
    }
}
