﻿using DarkLight.Editor.Model;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.View
{
    public class EditorUserRenderer : FlaiRenderer
    {
        private readonly EditorUser _user;
        public EditorUserRenderer(EditorUser user)
        {
            _user = user;
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            if (_user.ToolManager.ActiveTool != null)
            {
                _user.ToolManager.ActiveTool.Draw(graphicsContext);
            }

            this.DrawSelectedGameObjectOutlines(graphicsContext);
            this.DrawSelectionArea(graphicsContext);            
        }

        private void DrawSelectedGameObjectOutlines(GraphicsContext graphicsContext)
        {
            if (_user.SelectedGameObject != null)
            {
                ICamera2D camera = graphicsContext.Services.GetService<ICameraManager2D>().ActiveCamera;
                graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext, _user.SelectedGameObject.VisualArea, Color.Red, 2f / camera.Zoom);
            }
        }

        private void DrawSelectionArea(GraphicsContext graphicsContext)
        {
            if (_user.SelectionRectangle.HasValue)
            {
                ICamera2D camera = _serviceContainer.GetService<ICameraManager2D>().ActiveCamera;
                Rectangle selectionArea = _user.SelectionRectangle.RealArea;

                // draw the outlines and fill
                graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext, selectionArea, Color.Blue, 1f / camera.Zoom);
                graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, selectionArea, Color.Blue * 0.5f);
            }
        }
    }
}
