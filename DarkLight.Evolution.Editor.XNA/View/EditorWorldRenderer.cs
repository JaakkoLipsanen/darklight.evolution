﻿using System.Collections.Generic;
using System.Linq;
using DarkLight.Editor.Model;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.View
{
    public class EditorWorldRenderer : FlaiRenderer
    {
        private readonly EditorWorld _world;
        private readonly Dictionary<EditorRoom, EditorRoomRenderer> _roomRenderers;

        public EditorWorldRenderer(EditorWorld world)
        {
            _world = world;
            _roomRenderers = _world.Rooms.ToDictionary(
                room => room, // key
                room => new EditorRoomRenderer(_world, room)); // value

            // Subscribe to RoomAdded event so that new EditorRoomRenderer can be created whenever a room is added
            _world.Rooms.RoomAdded += this.OnRoomAdded;
            _world.Rooms.RoomRemoved += this.OnRoomRemoved;
            _world.Rooms.SelectedRoomChanged += OnSelectedRoomChanged;
        }

        protected override void LoadContentInner()
        {
            this.ChangeActiveCamera(_world.SelectedRoom.Camera);
            foreach (EditorRoomRenderer roomRenderer in _roomRenderers.Values)
            {
                roomRenderer.LoadContent();
            }
        }

        protected override void UnloadInner()
        {
            foreach (EditorRoomRenderer roomRenderer in _roomRenderers.Values)
            {
                roomRenderer.Unload();
            }
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _roomRenderers[_world.SelectedRoom].Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            this.DrawBackground(graphicsContext);
            _roomRenderers[_world.SelectedRoom].Draw(graphicsContext);
        }

        private void DrawBackground(GraphicsContext graphicsContext)
        {
            graphicsContext.GraphicsDevice.Clear(Color.LightSlateGray);
        }

        private void OnRoomAdded(EditorRoom room)
        {
            EditorRoomRenderer renderer = new EditorRoomRenderer(_world, room);
            _roomRenderers.Add(room, renderer);
            if (this.IsLoaded)
            {
                renderer.LoadContent();
            }
        }

        private void OnRoomRemoved(EditorRoom room)
        {
            // this throws an exception if room is not found
            EditorRoomRenderer renderer = _roomRenderers[room];
            _roomRenderers.Remove(room);

            if (this.IsLoaded)
            {
                renderer.Unload();
            }
        }

        private void OnSelectedRoomChanged(EditorRoom oldRoom, EditorRoom newRoom)
        {
            this.ChangeActiveCamera(newRoom.Camera);
        }

        private void ChangeActiveCamera(EditorRoomCamera camera)
        {
            ICameraManager2D cameraManager = _serviceContainer.GetService<ICameraManager2D>();
            if (cameraManager.ActiveCamera == camera)
            {
                // The camera is already active
                return;
            }

            // Remove the active camera from the camera manager
            const string CameraName = "SelectedRoomCamera";
            if (cameraManager.ContainsCamera(CameraName))
            {
                cameraManager.RemoveCamera(CameraName);
            }

            cameraManager.AddCamera(CameraName, camera, true);
        }
    }
}
