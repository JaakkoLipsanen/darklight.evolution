﻿using DarkLight.Editor.Model;
using Flai;
using Flai.Graphics;

namespace DarkLight.Editor.View
{
    public class EditorLevelRenderer : FlaiRenderer
    {
        private readonly EditorLevel _level;
        private readonly EditorWorldRenderer _worldRenderer;
        private readonly EditorUserRenderer _userRenderer;

        public EditorUserRenderer UserRenderer
        {
            get { return _userRenderer; }
        }

        public EditorLevelRenderer(EditorLevel level)
        {
            _level = level;

            _worldRenderer = new EditorWorldRenderer(_level.World);
            _userRenderer = new EditorUserRenderer(_level.User);
        }

        protected override void LoadContentInner()
        {
            _serviceContainer.AddService<ICameraManager2D>(new CameraManager2D());
            _worldRenderer.LoadContent();
            _userRenderer.LoadContent();
        }

        protected override void UnloadInner()
        {
            _userRenderer.Unload();
            _worldRenderer.Unload();
            _serviceContainer.RemoveService<ICameraManager2D>();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _worldRenderer.Update(updateContext);
            _userRenderer.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            ICamera2D camera = graphicsContext.Services.GetService<ICameraManager2D>().ActiveCamera;
            graphicsContext.SpriteBatch.Begin(camera.GetTransform(graphicsContext.GraphicsDevice));

            // Draw the editor
            _worldRenderer.Draw(graphicsContext);
            _userRenderer.Draw(graphicsContext);

            graphicsContext.SpriteBatch.End();
        }
    }
}
