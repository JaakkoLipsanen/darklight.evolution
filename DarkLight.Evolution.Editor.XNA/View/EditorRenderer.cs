﻿using System;
using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.View
{
    public static class EditorRenderer
    {
        public const float ForegroundDimensionAlpha = 1f;
        public const float BackgroundDimensionAlpha = 0.3f;

        public static Color PortalColor = Color.Blue;
        public static Color LavaColor = Color.Red;
        public static Color UnknownColor = Color.Purple;

        public static void DrawGameObject(GraphicsContext graphicsContext, GameObject gameObject, GameDimension gameDimension, float alpha = 1f)
        {
            (gameObject as IGameObject).Draw(graphicsContext, gameDimension, alpha);
        }

        public static void DrawTile(GraphicsContext graphicsContext, TileType tileType, Vector2 position, GameDimension gameDimension, float alpha = 1f)
        {
            if (tileType == TileType.Air)
            {
                return;
            }

            Color gameDimensionColor = EditorRenderer.GetGameDimensionColor(gameDimension);
            switch (tileType)
            {
                case TileType.Solid:
                    graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, position, gameDimensionColor * alpha, 0f, Vector2.Zero, Tile.Size);
                    break;
                case TileType.Lava:
                    graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, position, Color.Lerp(gameDimensionColor, EditorRenderer.LavaColor, 0.85f) * alpha, 0f, Vector2.Zero, Tile.Size);
                    break;

                default:
                    graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, position, EditorRenderer.UnknownColor * alpha, 0f, Vector2.Zero, Tile.Size);
                    break;
            }
        }

        public static Color GetGameDimensionColor(GameDimension gameDimension)
        {
            switch (gameDimension)
            {
                case GameDimension.Both:
                    return new Color(92, 92, 92);

                case GameDimension.Dark:
                    return Color.Black;

                case GameDimension.Light:
                    return Color.White;

                default:
                    throw new ArgumentException("Invalid game dimension");
            }
        }
    }
}
