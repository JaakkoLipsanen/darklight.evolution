﻿using System;
using DarkLight.Editor.Model;
using Flai;
using Flai.Graphics;

namespace DarkLight.Editor.View
{
    public class EditorRoomMapRenderer : FlaiRenderer
    {
        private readonly EditorRoom _room;
        public EditorRoomMapRenderer(EditorRoom room)
        {
            _room = room;
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            throw new NotImplementedException("Use other Draw overloads!");
        }

        public void Draw(GraphicsContext graphicsContext, GameDimension gameDimension, float alpha)
        {
            ICamera2D camera = _serviceContainer.GetService<ICameraManager2D>().ActiveCamera;
            RectangleF cameraArea = camera.GetArea(graphicsContext.GraphicsDevice); // camera.GetArea(graphicsContext.GraphicsDevice);

            int left = Math.Max(0, (int)(cameraArea.Left / Tile.Size));
            int right = Math.Min(_room.Width - 1, (int)(cameraArea.Right / Tile.Size) + 1);
            int top = Math.Max(0, (int)(cameraArea.Top / Tile.Size));
            int bottom = Math.Min(_room.Height - 1, (int)(cameraArea.Bottom / Tile.Size) + 1);

            for (int x = left; x <= right; x++)
            {
                for (int y = top; y <= bottom; y++)
                {
                    Vector2i position = new Vector2i(x, y) * Tile.Size;
                    if (gameDimension == GameDimension.Both)
                    {
                        if (_room.Map.DarkTiles[x, y] == _room.Map.LightTiles[x, y])
                        {
                            EditorRenderer.DrawTile(graphicsContext, _room.Map.DarkTiles[x, y], position, GameDimension.Both, alpha);
                        }
                        else
                        {
                            EditorRenderer.DrawTile(graphicsContext, _room.Map.LightTiles[x, y], position, GameDimension.Light, alpha); // ?
                            EditorRenderer.DrawTile(graphicsContext, _room.Map.DarkTiles[x, y], position, GameDimension.Dark, alpha);
                        }
                    }
                    else if (gameDimension == GameDimension.Dark)
                    {
                        EditorRenderer.DrawTile(graphicsContext, _room.Map.DarkTiles[x, y], position, GameDimension.Dark, alpha);
                    }
                    else // if (gameDimension == GameDimension.Light)
                    {
                        EditorRenderer.DrawTile(graphicsContext, _room.Map.LightTiles[x, y], position, GameDimension.Light, alpha);
                    }
                }
            }
        }
    }
}
