﻿using System.ComponentModel;
using DarkLight.Editor.Model;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DarkLight.Editor.View
{
    public class EditorRoomCamera : ICamera2D, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly Camera2D _camera;
        private readonly EditorRoom _room;
        private bool _isLocked = false;

        private Vector2 _positionBeforeLock;
        private float _zoomBeforeLock;

        public bool IsLocked
        {
            get { return _isLocked; }
            set
            {
                if (_isLocked != value)
                {
                    _isLocked = value;
                    if (_isLocked)
                    {
                        _positionBeforeLock = this.Position;
                        _zoomBeforeLock = this.Zoom;
                        this.LockCamera();
                    }
                    else
                    {
                        this.Position = _positionBeforeLock;
                        this.Zoom = _zoomBeforeLock;
                    }

                    this.PropertyChanged.InvokeIfNotNull(this, "IsLocked");
                }
            }
        }

        public EditorRoomCamera(EditorRoom room)
        {
            _room = room;
            _camera = new Camera2D(new Vector2i(_room.Width, _room.Height) * Tile.Size / 2f);

            _room.Map.Resized += this.OnMapSizeChanged;

            // TODO: Unsubscribe from this when the room is removed!
            // TODO: Maybe not necessary though, it shouldn't really affect anything..
            FlaiGame.Current.ResolutionChanged += this.OnResolutionChanged;
        }

        private void OnResolutionChanged(Size oldResolution, Size newResolution)
        {
            if (_isLocked)
            {
                this.LockCamera();
            }
        }

        private void LockCamera()
        {
            _camera.Position = new Vector2i(_room.Width * Tile.Size, _room.Height * Tile.Size) / 2f;

            float horizontalZoom = FlaiGame.Current.ScreenSize.Width / (float)((_room.Width + 2) * Tile.Size);
            float verticalZoom = FlaiGame.Current.ScreenSize.Height / (float)((_room.Height + 2) * Tile.Size);

            _camera.Zoom = FlaiMath.Min(horizontalZoom, verticalZoom);

            // !! <Fix zoom> !!
        }

        private void OnMapSizeChanged(Direction2D resizeSide, int resizeAmount)
        {
            if (_isLocked)
            {
                this.LockCamera();
            }
        }

        #region Implementation of ICamera2D

        public Vector2 Position
        {
            get { return _camera.Position; }
            set
            {
                if (!_isLocked)
                {
                    // clamp the value
                    const float ClampOffset = 4 * Tile.Size;
                    value = new Vector2(
                        FlaiMath.Clamp(value.X, -ClampOffset, _room.Width * Tile.Size + ClampOffset),
                        FlaiMath.Clamp(value.Y, -ClampOffset, _room.Height * Tile.Size + ClampOffset));

                    _camera.Position = value;
                }
            }
        }

        public float Rotation
        {
            get { return _camera.Rotation; } // 0
        }

        public float Zoom
        {
            get { return _camera.Zoom; }
            set
            {
                if (!_isLocked)
                {
                    const float MinZoom = 0.01f;
                    const float MaxZoom = 2.5f;
                    float minZoom = FlaiMath.Max(MinZoom, FlaiMath.Min(
                        0.75f,
                        FlaiGame.Current.ViewportSize.Width / (_room.Width * Tile.Size * 2f),
                        FlaiGame.Current.ViewportSize.Height / (_room.Height * Tile.Size * 2f)));

                    _camera.Zoom = FlaiMath.Clamp(value, minZoom, MaxZoom);
                }
            }
        }

        public Matrix GetTransform(GraphicsDevice graphicsDevice)
        {
            return _camera.GetTransform(graphicsDevice);
        }

        public Matrix GetTransform(Size viewportSize)
        {
            return _camera.GetTransform(viewportSize);
        }

        public Matrix GetTransformForWithoutSpriteBatch(GraphicsDevice graphicsDevice)
        {
            return _camera.GetTransformForWithoutSpriteBatch(graphicsDevice);
        }

        public Matrix GetTransformForWithoutSpriteBatch(Size viewportSize)
        {
            return _camera.GetTransformForWithoutSpriteBatch(viewportSize);
        }

        public RectangleF GetArea(GraphicsDevice graphicsDevice)
        {
            return _camera.GetArea(graphicsDevice);
        }

        public RectangleF GetArea(Size screenSize)
        {
            return _camera.GetArea(screenSize);
        }

        public Vector2 ScreenToWorld(Vector2 v)
        {
            return this.ScreenToWorld(FlaiGame.Current.GraphicsDevice, v);
        }

        public Vector2 ScreenToWorld(GraphicsDevice graphicsDevice, Vector2 v)
        {
            return _camera.ScreenToWorld(graphicsDevice, v);
        }

        public Vector2 ScreenToWorld(Size screenSize, Vector2 v)
        {
            return _camera.ScreenToWorld(screenSize, v);
        }

        #endregion
    }
}