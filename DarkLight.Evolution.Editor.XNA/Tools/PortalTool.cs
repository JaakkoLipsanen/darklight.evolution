﻿using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using DarkLight.Editor.View;
using Flai;
using Flai.Graphics;
using Flai.Input;

namespace DarkLight.Editor.Tools
{
    // TODO: Allow linking destinations from this tool! maybe like drag the middle mouse button down to the other portal
    // TODO: <GENERAL COMMENT> change stuff to internal?
    // TODO: "EditorUser.SelectedGameObject" not kept in sync in this tool
    // TODO: There is a bug somewhere in the moving.. The portal can "teleport" to some place... idk, investigate
    public class PortalTool : EditorTool
    {
        #region Enums

        private enum PortalToolState
        {
            None,
            Adding,
            Moving,
        }

        #endregion

        #region Fields

        private PortalToolState _state = PortalToolState.None;

        private Portal _movingPortal;
        private PortalIndex _movingIndex;
        private Segment2Di _movingPortalStartSegment;

        private Vector2i _addingStartIndex;
        private Vector2i _addingEndIndex;

        #endregion

        public PortalTool(EditorWorld world, EditorUser user)
            : base(world, user)
        {
        }

        public override void Update(UpdateContext updateContext)
        {
            if (_state == PortalToolState.None)
            {
                if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
                {
                    this.HandleNewMouseButtonPress();
                }
            }

            if (_state == PortalToolState.Adding)
            {
                this.UpdateAdding(updateContext);
            }
            else if (_state == PortalToolState.Moving)
            {
                this.UpdateMoving(updateContext);
            }
            else
            {
                this.UpdateNone(updateContext);
            }
        }

        #region Update Adding

        private void UpdateAdding(UpdateContext updateContext)
        {
            if (!updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
            {
                // flush
                if (_addingStartIndex != _addingEndIndex)
                {
                    _user.ActionStack.Push(new AddPortalAction(_addingStartIndex, _addingEndIndex));
                }

                _state = PortalToolState.None;
                return;
            }

            Vector2i mouseIndex = _user.RoundedMouseTileIndex;
            Vector2i delta = mouseIndex - _addingStartIndex;
            if (FlaiMath.Abs(delta.X) > FlaiMath.Abs(delta.Y))
            {
                _addingEndIndex = new Vector2i(mouseIndex.X, _addingStartIndex.Y);
            }
            else
            {
                _addingEndIndex = new Vector2i(_addingStartIndex.X, mouseIndex.Y);
            }
        }

        #endregion

        #region Update Moving

        private void UpdateMoving(UpdateContext updateContext)
        {
            if (!updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
            {
                if (_movingPortal.TileSegment != _movingPortalStartSegment)
                {
                    Vector2i difference = (_movingIndex == PortalIndex.Start) ?
                        (_movingPortal.StartIndex - _movingPortalStartSegment.Start) :
                        (_movingPortal.EndIndex - _movingPortalStartSegment.End);

                    _user.ActionStack.Push(new MovePortalAction(_movingPortal, _movingIndex, difference));
                }

                _movingPortal = null;
                _state = PortalToolState.None;
                return;
            }

            Vector2i mouseIndex = _user.RoundedMouseTileIndex;
            if (_movingIndex == PortalIndex.Start)
            {
                Vector2i delta = mouseIndex - _movingPortalStartSegment.End;
                if (delta == Vector2i.Zero)
                {
                    return;
                }

                if (FlaiMath.Abs(delta.X) > FlaiMath.Abs(delta.Y))
                {
                    _movingPortal.StartIndex = new Vector2i(mouseIndex.X, _movingPortalStartSegment.End.Y);
                    _movingPortal.Direction = delta.X > 0 ? Direction2D.Left : Direction2D.Right;
                    _movingPortal.Length = FlaiMath.Abs(delta.X);
                }
                else
                {
                    _movingPortal.StartIndex = new Vector2i(_movingPortalStartSegment.Start.X, mouseIndex.Y);
                    _movingPortal.Direction = delta.Y > 0 ? Direction2D.Up : Direction2D.Down;
                    _movingPortal.Length = FlaiMath.Abs(delta.Y);
                }
            }
            else if (_movingIndex == PortalIndex.End)
            {
                Vector2i delta = mouseIndex - _movingPortalStartSegment.Start;
                if (delta == Vector2i.Zero)
                {
                    return;
                }

                if (FlaiMath.Abs(delta.X) > FlaiMath.Abs(delta.Y))
                {
                    _movingPortal.Direction = delta.X > 0 ? Direction2D.Right : Direction2D.Left;
                    _movingPortal.Length = FlaiMath.Abs(mouseIndex.X - _movingPortalStartSegment.Start.X);
                }
                else
                {
                    _movingPortal.Direction = delta.Y > 0 ? Direction2D.Down : Direction2D.Up;
                    _movingPortal.Length = FlaiMath.Abs(mouseIndex.Y - _movingPortalStartSegment.Start.Y);
                }
            }
        }

        #endregion

        #region Update None

        private void UpdateNone(UpdateContext updateContext)
        {
            // Flip normal
            if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Right))
            {
                Portal portal = _world.SelectedRoom.GameObjects.GetPortalAt(_user.RoundedMouseTileIndex);
                if (portal != null)
                {
                    _user.ActionStack.Push(new FlipPortalAction(portal));
                }
            }
        }

        #endregion

        #region Misc

        private void HandleNewMouseButtonPress()
        {
            PortalIndex portalIndex;
            Portal portal = _world.SelectedRoom.GameObjects.GetPortalIndexAt(_user.RoundedMouseTileIndex, out portalIndex);
            if (portal != null)
            {
                _state = PortalToolState.Moving;
                _movingPortal = portal;
                _movingIndex = portalIndex;
                _movingPortalStartSegment = _movingPortal.TileSegment;
            }
            else
            {
                _state = PortalToolState.Adding;
                _addingStartIndex = _user.RoundedMouseTileIndex;
                _addingEndIndex = _user.RoundedMouseTileIndex;
            }
        }

        #endregion

        public override void Draw(GraphicsContext graphicsContext)
        {
            if (_state == PortalToolState.Adding)
            {
                graphicsContext.PrimitiveRenderer.DrawLine(graphicsContext, _addingStartIndex * Tile.Size, _addingEndIndex * Tile.Size, EditorRenderer.PortalColor);

                // Normal
                Vector2i delta = _addingEndIndex - _addingStartIndex;
                Direction2D normalDirection = delta.X == 0 ? (delta.Y > 0 ? Direction2D.Left : Direction2D.Right) : (delta.X > 0 ? Direction2D.Down : Direction2D.Up);
                graphicsContext.PrimitiveRenderer.DrawLine(graphicsContext, (_addingStartIndex + _addingEndIndex) * Tile.Size / 2f, EditorRenderer.PortalColor * 0.75f, normalDirection.ToRadians(), Tile.Size);
            }
        }

        #region Add Portal Action

        private class AddPortalAction : EditorAction
        {
            private readonly Portal _portal;
            public AddPortalAction(Vector2i start, Vector2i end)
            {
                _portal = new Portal(start, end);
            }

            public override void Undo()
            {
                this.SelectedRoomOnCreation.GameObjects.Remove(_portal);
            }

            public override void Redo()
            {
                this.Execute();
            }

            protected override void OnInitialized()
            {
                this.Execute();
            }

            private void Execute()
            {
                this.SelectedRoomOnCreation.GameObjects.Add(_portal);
            }
        }

        #endregion

        #region Move Portal Action

        private class MovePortalAction : EditorAction
        {
            private readonly Portal _portal;
            private readonly PortalIndex _portalIndex;
            private readonly Vector2i _moveAmount;

            public MovePortalAction(Portal portal, PortalIndex portalIndex, Vector2i moveAmount)
            {
                _portal = portal;
                _portalIndex = portalIndex;
                _moveAmount = moveAmount;
            }

            public override void Undo()
            {
                this.ChangeIndex(-_moveAmount);
            }

            public override void Redo()
            {
                this.ChangeIndex(_moveAmount);
            }

            private void ChangeIndex(Vector2i moveAmount)
            {
                if (_portalIndex == PortalIndex.End)
                {
                    Vector2i newIndex = _portal.EndIndex + moveAmount;
                    if (newIndex.X == _portal.StartIndex.X) // Horizontal
                    {
                        _portal.Direction = newIndex.Y > _portal.StartIndex.Y ? Direction2D.Down : Direction2D.Up;
                        _portal.Length = FlaiMath.Abs(newIndex.Y - _portal.StartIndex.Y);
                    }
                    else // if (oldIndex.Y == _portal.StartIndex.Y)
                    {
                        _portal.Direction = newIndex.X > _portal.StartIndex.X ? Direction2D.Right : Direction2D.Left;
                        _portal.Length = FlaiMath.Abs(newIndex.X - _portal.StartIndex.X);
                    }
                }
                else // if (_portalIndex == PortalIndex.Start)
                {
                    Vector2i newIndex = _portal.StartIndex + moveAmount;
                    if (newIndex.X == _portal.EndIndex.X)
                    {
                        _portal.Length = FlaiMath.Abs(newIndex.Y - _portal.EndIndex.Y);
                        _portal.Direction = newIndex.Y > _portal.EndIndex.Y ? Direction2D.Up : Direction2D.Down;
                    }
                    else // if (oldIndex.Y == _portal.StartIndex.Y)
                    {
                        _portal.Length = FlaiMath.Abs(newIndex.X - _portal.EndIndex.X);
                        _portal.Direction = newIndex.X > _portal.EndIndex.X ? Direction2D.Left : Direction2D.Right;
                    }

                    _portal.StartIndex = newIndex;
                }
            }
        }

        #endregion

        #region Flip Portal Action

        private class FlipPortalAction : EditorAction
        {
            private readonly Portal _portal;
            public FlipPortalAction(Portal portal)
            {
                _portal = portal;
                _portal.FlipNormal();
            }

            public override void Undo()
            {
                _portal.FlipNormal(); ;
            }

            public override void Redo()
            {
                _portal.FlipNormal();
            }
        }

        #endregion
    }
}
