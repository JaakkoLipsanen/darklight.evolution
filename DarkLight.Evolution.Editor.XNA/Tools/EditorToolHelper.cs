﻿using System;
using DarkLight.Editor.Model.GameObjects;

namespace DarkLight.Editor.Tools
{
    public static class EditorToolHelper
    {
        public static bool IsTileTool(EditorTool tool)
        {
            return tool != null && EditorToolHelper.IsTileTool(tool.GetType());
        }

        public static bool IsTileTool<T>()
            where T : EditorTool
        {
            return EditorToolHelper.IsTileTool(typeof(T));
        }

        public static bool IsTileTool(Type type)
        {
            return type == typeof(TileBrushTool) || type == typeof(FillTool);
        }

        public static bool IsGameObjectTool(EditorTool tool)
        {
            return tool != null && EditorToolHelper.IsGameObjectTool(tool.GetType());
        }

        public static bool IsGameObjectTool<T>()
            where T : EditorTool
        {
            return EditorToolHelper.IsGameObjectTool(typeof(T));
        }

        public static bool IsGameObjectTool(Type type)
        {
            return type == typeof(GameObjectTool) || type == typeof(MovingPlatformTool) || type == typeof(PortalTool);
        }
    }
}
