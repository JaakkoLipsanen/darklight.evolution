﻿using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using Flai;
using Flai.Graphics;
using Flai.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DarkLight.Editor.Tools
{
    public class MovingPlatformTool : EditorTool
    {
        #region Fields

        // OH MY GOD horrible name :P But "_movingPlatform" "_platformMoving" etc just are so hard to understand :P
        private MovingPlatform _platformUnderMovement;

        // moving stuff
        private int _movingIndex = -1;
        private Vector2i _moveStartPosition = Vector2i.Zero;

        #endregion

        public MovingPlatformTool(EditorWorld world, EditorUser user)
            : base(world, user)
        {
        }

        #region Update

        public override void Update(UpdateContext updateContext)
        {
            if (_user.SelectedGameObject == null || updateContext.InputState.IsKeyPressed(Keys.LeftControl))
            {
                this.SelectMovingPlatform(updateContext);
            }
            else if (_user.SelectedGameObject is MovingPlatform)
            {
                MovingPlatform selectedMovingPlatform = (MovingPlatform)_user.SelectedGameObject;

                // Flush the moving if the selected moving platform has been changed
                if (_platformUnderMovement != null && _platformUnderMovement != selectedMovingPlatform)
                {
                    this.FlushMoving();
                }

                if (_platformUnderMovement != null) // if(isMoving)
                {
                    this.UpdateMoving(updateContext, selectedMovingPlatform);
                }
                else if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
                {
                    this.HandleNewMouseButtonPress(selectedMovingPlatform);
                }
                else if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Right))
                {
                    this.RemovePoint(selectedMovingPlatform);
                }
            }
        }

        #endregion

        #region Misc

        private void SelectMovingPlatform(UpdateContext updateContext)
        {
            if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
            {
                _user.SelectedGameObject = _world.SelectedRoom.GameObjects.GetAt<MovingPlatform>(_user.MousePosition);
            }
        }

        private void UpdateMoving(UpdateContext updateContext, MovingPlatform movingPlatform)
        {
            if (!updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
            {
                this.FlushMoving();
                return;
            }

            movingPlatform.MoveLoopPointAt(_movingIndex, _user.RoundedMouseTileIndex);
        }

        private void HandleNewMouseButtonPress(MovingPlatform movingPlatform)
        {
            // If the mouse is over a loop point index, start moving it
            int loopPointAtMouse = MovingPlatformTool.GetLoopPointIndex(movingPlatform, _user.MousePosition);
            if (loopPointAtMouse >= 0)
            {
                _platformUnderMovement = movingPlatform;
                _movingIndex = loopPointAtMouse;
                _moveStartPosition = movingPlatform.LoopPoints[_movingIndex];
            }
            // otherwise add new point
            else
            {
                this.AddPoint(movingPlatform);
            }
        }

        #region AddPoint/RemovePoint

        private void AddPoint(MovingPlatform movingPlatform)
        {
            int index = MovingPlatformTool.GetIndexForNewPoint(movingPlatform, _user.MousePosition);

            // if the index is positive, then insert the new point to the index and start dragging
            if (index >= 0)
            {
                _user.ActionStack.Push(new MovingPlatformIndexAddAction(movingPlatform, _user.RoundedMouseTileIndex, index));

                // start moving the point
                _platformUnderMovement = movingPlatform;
                _movingIndex = index;
                _moveStartPosition = movingPlatform.LoopPoints[_movingIndex];
            }
            // otherwise add the point to the end of the points list
            else
            {
                if (movingPlatform.LoopPoints[movingPlatform.LoopPoints.Count - 1] != _user.RoundedMouseTileIndex)
                {
                    _user.ActionStack.Push(new MovingPlatformIndexAddAction(movingPlatform, _user.RoundedMouseTileIndex, movingPlatform.LoopPoints.Count));
                }
            }
        }

        private void RemovePoint(MovingPlatform movingPlatform)
        {
            // Start from index = 1 because the first point can't be removed
            for (int i = 1; i < movingPlatform.LoopPoints.Count; i++)
            {
                if (movingPlatform.LoopPoints[i] == _user.RoundedMouseTileIndex)
                {
                    _user.ActionStack.Push(new MovingPlatformIndexRemoveAction(movingPlatform, i));
                    return;
                }
            }
        }

        #endregion

        private void FlushMoving()
        {
            if (_platformUnderMovement == null)
            {
                return;
            }

            if (_user.RoundedMouseTileIndex != _moveStartPosition)
            {
                _user.ActionStack.Push(new MovingPlatformIndexMoveAction(_platformUnderMovement, _moveStartPosition, _user.RoundedMouseTileIndex, _movingIndex));
            }

            _platformUnderMovement = null;
            _movingIndex = -1;
        }

        #endregion

        #region Draw

        public override void Draw(GraphicsContext graphicsContext)
        {
            MovingPlatform selectedMovingPlatform = _user.SelectedGameObject as MovingPlatform;
            if (selectedMovingPlatform == null)
            {
                return;
            }

            // Draw lines
            for (int i = 0; i < selectedMovingPlatform.LoopPoints.Count - 1; i++)
            {
                graphicsContext.PrimitiveRenderer.DrawLine(graphicsContext, selectedMovingPlatform.LoopPoints[i] * Tile.Size, selectedMovingPlatform.LoopPoints[i + 1] * Tile.Size, Color.Yellow, 4f);
            }

            // Draw points
            for (int i = 0; i < selectedMovingPlatform.LoopPoints.Count; i++)
            {
                Color color;
                if (i == _movingIndex)
                {
                    color = Color.Yellow;
                }
                else if (i == 0)
                {
                    color = Color.Blue;
                }
                else if (i == selectedMovingPlatform.LoopPoints.Count - 1)
                {
                    color = Color.Red;
                }
                else
                {
                    color = Color.Green;
                }

                graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, selectedMovingPlatform.LoopPoints[i] * Tile.Size, Tile.Size, color);
            }
        }

        #endregion

        #region Static GetIndexXXX Helpers

        private static int GetLoopPointIndex(MovingPlatform movingPlatform, Vector2 position, float maxDistance = Tile.Size)
        {
            for (int i = 0; i < movingPlatform.LoopPoints.Count; i++)
            {
                if (Vector2.Distance(position, movingPlatform.LoopPoints[i] * Tile.Size) < maxDistance)
                {
                    return i;
                }
            }

            return -1;
        }

        private static int GetIndexForNewPoint(MovingPlatform movingPlatform, Vector2 position, float maxDistance = Tile.Size)
        {
            for (int i = 0; i < movingPlatform.LoopPoints.Count - 1; i++)
            {
                Segment2D segment = new Segment2D(movingPlatform.LoopPoints[i] * Tile.Size, movingPlatform.LoopPoints[i + 1] * Tile.Size);
                if (Segment2D.MinimumDistance(segment, position) < maxDistance)
                {
                    return i + 1;
                }
            }

            return -1;
        }

        #endregion

        #region MovingPlatformIndexAddAction

        private class MovingPlatformIndexAddAction : EditorAction
        {
            private readonly MovingPlatform _movingPlatform;
            private readonly Vector2i _position;
            private readonly int _index;

            public MovingPlatformIndexAddAction(MovingPlatform movingPlatform, Vector2i position, int index)
            {
                _movingPlatform = movingPlatform;
                _position = position;
                _index = index;
            }

            public override void Undo()
            {
                _movingPlatform.RemoveLoopPointAt(_index);
            }

            public override void Redo()
            {
                this.Execute();
            }

            protected override void OnInitialized()
            {
                this.Execute();
            }

            private void Execute()
            {
                _movingPlatform.AddLoopPointTo(_index, _position);
            }
        }

        #endregion

        #region MovingPlatformIndexRemoveAction

        private class MovingPlatformIndexRemoveAction : EditorAction
        {
            private readonly MovingPlatform _movingPlatform;
            private readonly Vector2i _position;
            private readonly int _index;

            public MovingPlatformIndexRemoveAction(MovingPlatform movingPlatform, int index)
            {
                _movingPlatform = movingPlatform;
                _position = movingPlatform.LoopPoints[index];
                _index = index;
            }

            public override void Undo()
            {
                _movingPlatform.AddLoopPointTo(_index, _position);
            }

            public override void Redo()
            {
                this.Execute();
            }

            protected override void OnInitialized()
            {
                this.Execute();
            }

            private void Execute()
            {
                _movingPlatform.RemoveLoopPointAt(_index);
            }
        }

        #endregion

        #region MovingPlatformIndexMoveAction

        private class MovingPlatformIndexMoveAction : EditorAction
        {
            private readonly MovingPlatform _movingPlatform;
            private readonly Vector2i _newPosition;
            private readonly Vector2i _oldPosition;
            private readonly int _index;

            public MovingPlatformIndexMoveAction(MovingPlatform movingPlatform, Vector2i oldPosition, Vector2i newPosition, int index)
            {
                _movingPlatform = movingPlatform;
                _oldPosition = oldPosition;
                _newPosition = newPosition;
                _index = index;
            }

            public override void Undo()
            {
                _movingPlatform.MoveLoopPointAt(_index, _oldPosition);
            }

            public override void Redo()
            {
                this.Execute();
            }

            private void Execute()
            {
                _movingPlatform.MoveLoopPointAt(_index, _newPosition);
            }
        }

        #endregion
    }
}
