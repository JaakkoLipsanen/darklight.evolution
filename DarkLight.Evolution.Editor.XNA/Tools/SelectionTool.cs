﻿using System;
using DarkLight.Editor.Model;
using DarkLight.Editor.View;
using Flai;
using Flai.Graphics;
using Flai.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DarkLight.Editor.Tools
{
    public class SelectionTool : EditorTool
    {
        private Vector2i _anchorPoint;
        private Vector2i _freePoint;

        private Vector2i _previousMouseIndex;

        // copying
        private TileInfo[] _copyDataSource;
        private Rectangle _sourceArea;

        public SelectionTool(EditorWorld world, EditorUser user)
            : base(world, user)
        {
        }

        #region Update

        public override void Update(UpdateContext updateContext)
        {
            if (updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
            {
                // If LeftControl is pressed down, then start copying the tiles
                if (updateContext.InputState.IsKeyPressed(Keys.LeftControl) && _user.SelectionRectangle.HasValue)
                {
                    this.UpdateCopying(updateContext);
                }
                else
                {
                    this.UpdateSelecting(updateContext);
                }
            }
            else
            {
                // Flush if needed
                if (_copyDataSource != null)
                {
                    this.FlushCopying(updateContext);
                }

                this.CheckForDeleteAction(updateContext);
            }

            _previousMouseIndex = _user.MouseTileIndex;
        }

        #region Update Copying

        private void UpdateCopying(UpdateContext updateContext)
        {
            // Create the copy-data if copying just started
            if (_copyDataSource == null)
            {
                this.CreateCopyData();
                _sourceArea = _user.SelectionRectangle.TileArea;
            }

            Vector2 previousMouseWorldPosition = _world.SelectedRoom.Camera.ScreenToWorld(updateContext.InputState.PreviousMousePosition);
            bool isMouseWithinWorld = _user.SelectionRectangle.RealArea.Contains(previousMouseWorldPosition) || _user.SelectionRectangle.RealArea.Contains(_user.MousePosition);

            // move the copied tiles area
            if (isMouseWithinWorld && _user.MouseTileIndex != _previousMouseIndex)
            {
                Vector2i difference = _user.MouseTileIndex - _previousMouseIndex;
                Rectangle newArea = _user.SelectionRectangle.TileArea.Offset(difference);
                if (newArea.Left < 0)
                {
                    newArea.X += -newArea.Left;
                }
                else if (newArea.Right > _world.SelectedRoom.Width)
                {
                    newArea.X -= (newArea.Right - _world.SelectedRoom.Width);
                }

                if (newArea.Top < 0)
                {
                    newArea.Y += -newArea.Top;
                }
                else if (newArea.Bottom > _world.SelectedRoom.Height)
                {
                    newArea.Y -= (newArea.Bottom - _world.SelectedRoom.Height);
                }

                _user.SelectionRectangle.TileArea = newArea;
                _anchorPoint = new Vector2i(newArea.Left, newArea.Top);
                _freePoint = new Vector2i(newArea.Right, newArea.Bottom);
            }
        }

        #endregion

        #region Update Selecting

        private void UpdateSelecting(UpdateContext updateContext)
        {
            // Set anchor point if the mouse press is new
            if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
            {
                _anchorPoint = _user.MouseTileIndex;
            }

            if (_user.MouseTileIndex == _anchorPoint)
            {
                _freePoint = _anchorPoint;
            }
            else
            {
                if (_user.MousePosition.X > _anchorPoint.X * Tile.Size)
                {
                    _freePoint.X = (int)Math.Ceiling(_user.MousePosition.X / Tile.Size);
                }
                else
                {
                    _freePoint.X = (int)(_user.MousePosition.X / Tile.Size);
                }

                if (_user.MousePosition.Y > _anchorPoint.Y * Tile.Size)
                {
                    _freePoint.Y = (int)Math.Ceiling(_user.MousePosition.Y / Tile.Size);
                }
                else
                {
                    _freePoint.Y = (int)(_user.MousePosition.Y / Tile.Size);
                }
            }

            // find the min/max X and Y
            Vector2i min = Vector2i.Max(Vector2i.Zero, Vector2i.Min(_anchorPoint, _freePoint));
            Vector2i max = Vector2i.Min(Vector2i.Max(_anchorPoint, _freePoint), new Vector2i(_world.SelectedRoom.Width, _world.SelectedRoom.Height));
            _user.SelectionRectangle.TileArea = new Rectangle(min.X, min.Y, max.X - min.X, max.Y - min.Y);
        }

        #endregion

        private void FlushCopying(UpdateContext updateContext)
        {
            if (_copyDataSource != null)
            {
                if (updateContext.InputState.WasKeyPressed(Keys.LeftControl))
                {
                    _user.ActionStack.Push(new SelectionCopyAction(_world.ActiveGameDimension, _copyDataSource, _sourceArea, new Vector2i(_user.SelectionRectangle.TileArea.X, _user.SelectionRectangle.TileArea.Y)));
                }

                _copyDataSource = null;
            }
        }

        private void CheckForDeleteAction(UpdateContext updateContext)
        {
            // Delete selection
            if (updateContext.InputState.IsNewKeyPress(Keys.Delete))
            {
                if (this.CheckIfSelectedAreaContainsDeletable())
                {
                    _user.ActionStack.Push(new SelectionDeleteAction(_user.SelectionRectangle.TileArea, _world.ActiveGameDimension));
                }
            }
        }

        #endregion

        #region Draw

        public override void Draw(GraphicsContext graphicsContext)
        {
            if (_copyDataSource != null && _user.SelectionRectangle.HasValue)
            {
                bool lightTileFirst = _world.ActiveGameDimension == GameDimension.Light;

                const float CopyTileAlpha = 0.5f;
                float lightAlpha = (_world.ActiveGameDimension == GameDimension.Dark ? EditorRenderer.BackgroundDimensionAlpha : 1f) * CopyTileAlpha;
                float darkAlpha = (_world.ActiveGameDimension == GameDimension.Light ? EditorRenderer.BackgroundDimensionAlpha : 1f) * CopyTileAlpha;

                Rectangle selectionArea = _user.SelectionRectangle.TileArea;
                for (int y = 0; y < _sourceArea.Height; y++)
                {
                    for (int x = 0; x < _sourceArea.Width; x++)
                    {
                        TileInfo tileInfo = _copyDataSource[x + y * _sourceArea.Width];
                        Vector2 position = new Vector2(x + selectionArea.X, y + selectionArea.Y) * Tile.Size;
                        if (lightTileFirst)
                        {
                            if (tileInfo.DarkTile != null)
                            {
                                EditorRenderer.DrawTile(graphicsContext, tileInfo.DarkTile.Value, position, GameDimension.Dark, darkAlpha);
                            }

                            if (tileInfo.LightTile != null)
                            {
                                EditorRenderer.DrawTile(graphicsContext, tileInfo.LightTile.Value, position, GameDimension.Light, lightAlpha);
                            }
                        }
                        else
                        {
                            if (tileInfo.DarkTile != null)
                            {
                                EditorRenderer.DrawTile(graphicsContext, tileInfo.DarkTile.Value, position, GameDimension.Dark, darkAlpha);
                            }

                            if (tileInfo.LightTile != null)
                            {
                                EditorRenderer.DrawTile(graphicsContext, tileInfo.LightTile.Value, position, GameDimension.Light, lightAlpha);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Misc

        private void CreateCopyData()
        {
            Rectangle selectionArea = _user.SelectionRectangle.TileArea;
            _copyDataSource = new TileInfo[selectionArea.Width * selectionArea.Height];

            for (int y = selectionArea.Top; y < selectionArea.Bottom; y++)
            {
                for (int x = selectionArea.Left; x < selectionArea.Right; x++)
                {
                    TileInfo tileInfo = new TileInfo { Index = new Vector2i(x, y) };
                    if (_world.ActiveGameDimension.IsFlagSet(GameDimension.Dark))
                    {
                        TileType tileType = _world.SelectedRoom.Map.DarkTiles[x, y];
                        if (tileType != TileType.Air)
                        {
                            tileInfo.DarkTile = _world.SelectedRoom.Map.DarkTiles[x, y];
                        }
                    }

                    if (_world.ActiveGameDimension.IsFlagSet(GameDimension.Light))
                    {
                        TileType tileType = _world.SelectedRoom.Map.LightTiles[x, y];
                        if (tileType != TileType.Air)
                        {
                            tileInfo.LightTile = _world.SelectedRoom.Map.LightTiles[x, y];
                        }
                    }

                    _copyDataSource[(x - selectionArea.Left) + (y - selectionArea.Top) * selectionArea.Width] = tileInfo;
                }
            }
        }

        private bool CheckIfSelectedAreaContainsDeletable()
        {
            if (_user.SelectionRectangle.HasValue)
            {
                Rectangle area = _user.SelectionRectangle.TileArea;
                for (int y = area.Top; y < area.Bottom; y++)
                {
                    for (int x = area.Left; x < area.Right; x++)
                    {
                        if (_world.ActiveGameDimension.IsFlagSet(GameDimension.Dark) && (_world.SelectedRoom.Map.DarkTiles[x, y] != TileType.Air))
                        {
                            return true;
                        }
                        else if (_world.ActiveGameDimension.IsFlagSet(GameDimension.Light) && (_world.SelectedRoom.Map.LightTiles[x, y] != TileType.Air))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        #endregion

        #region SelectionDeleteAction

        private class SelectionDeleteAction : EditorAction
        {
            private readonly Rectangle _selectionArea;
            private readonly GameDimension _gameDimension;
            private readonly TileInfo[] _deletedTiles;

            public SelectionDeleteAction(Rectangle selectionArea, GameDimension gameDimension)
            {
                _selectionArea = selectionArea;
                _gameDimension = gameDimension;

                _deletedTiles = new TileInfo[selectionArea.Width * selectionArea.Height];
            }

            #region Undo

            public override void Undo()
            {
                foreach (TileInfo tileInfo in _deletedTiles)
                {
                    if (tileInfo.DarkTile != null)
                    {
                        this.SelectedRoomOnCreation.Map.DarkTiles[tileInfo.Index] = tileInfo.DarkTile.Value;
                    }

                    if (tileInfo.LightTile != null)
                    {
                        this.SelectedRoomOnCreation.Map.LightTiles[tileInfo.Index] = tileInfo.LightTile.Value;
                    }
                }
            }

            #endregion

            #region Redo

            public override void Redo()
            {
                for (int y = _selectionArea.Top; y < _selectionArea.Bottom; y++)
                {
                    for (int x = _selectionArea.Left; x < _selectionArea.Right; x++)
                    {
                        if (_gameDimension.IsFlagSet(GameDimension.Dark))
                        {
                            this.SelectedRoomOnCreation.Map.DarkTiles[x, y] = TileType.Air;
                        }

                        if (_gameDimension.IsFlagSet(GameDimension.Light))
                        {
                            this.SelectedRoomOnCreation.Map.LightTiles[x, y] = TileType.Air;
                        }
                    }
                }
            }

            #endregion

            #region OnInitialized

            // Initial Execute
            protected override void OnInitialized()
            {
                for (int y = _selectionArea.Top; y < _selectionArea.Bottom; y++)
                {
                    for (int x = _selectionArea.Left; x < _selectionArea.Right; x++)
                    {
                        TileInfo tileInfo = new TileInfo { Index = new Vector2i(x, y) };
                        if (_gameDimension.IsFlagSet(GameDimension.Dark))
                        {
                            tileInfo.DarkTile = this.SelectedRoomOnCreation.Map.DarkTiles[x, y];
                            this.SelectedRoomOnCreation.Map.DarkTiles[x, y] = TileType.Air;
                        }

                        if (_gameDimension.IsFlagSet(GameDimension.Light))
                        {
                            tileInfo.LightTile = this.SelectedRoomOnCreation.Map.LightTiles[x, y];
                            this.SelectedRoomOnCreation.Map.LightTiles[x, y] = TileType.Air;
                        }

                        _deletedTiles[(x - _selectionArea.Left) + (y - _selectionArea.Top) * _selectionArea.Width] = tileInfo;
                    }
                }
            }

            #endregion
        }

        #endregion

        #region SelectionCopyAction

        private class SelectionCopyAction : EditorAction
        {
            private readonly GameDimension _gameDimension;
            private readonly TileInfo[] _sourceTiles;
            private readonly Rectangle _sourceArea;
            private readonly Vector2i _destinationTopLeft;
            private TileInfo[] _destinationTiles; // :(

            public SelectionCopyAction(GameDimension gameDimension, TileInfo[] sourceTiles, Rectangle sourceArea, Vector2i destinationTopLeft)
            {
                _gameDimension = gameDimension;
                _sourceTiles = sourceTiles;
                _sourceArea = sourceArea;
                _destinationTopLeft = destinationTopLeft;
            }

            #region Undo

            public override void Undo()
            {
                //  TODO: Gotta catch em alll
                foreach (TileInfo tileInfo in _destinationTiles)
                {
                    if (tileInfo.DarkTile != null)
                    {
                        this.SelectedRoomOnCreation.Map.DarkTiles[tileInfo.Index] = tileInfo.DarkTile.Value;
                    }

                    if (tileInfo.LightTile != null)
                    {
                        this.SelectedRoomOnCreation.Map.LightTiles[tileInfo.Index] = tileInfo.LightTile.Value;
                    }
                }

                // !! erhm.. why is this needed?
                foreach (TileInfo tileInfo in _sourceTiles)
                {
                    if (tileInfo.DarkTile != null)
                    {
                        this.SelectedRoomOnCreation.Map.DarkTiles[tileInfo.Index] = tileInfo.DarkTile.Value;
                    }

                    if (tileInfo.LightTile != null)
                    {
                        this.SelectedRoomOnCreation.Map.LightTiles[tileInfo.Index] = tileInfo.LightTile.Value;
                    }
                }
            }

            #endregion

            #region Redo and OnInitialized

            public override void Redo()
            {
                this.Execute();
            }

            protected override void OnInitialized()
            {
                this.CalculateDestinationTiles();
                this.Execute();
            }

            #endregion

            #region Execute

            private void Execute()
            {
                Rectangle destinationArea = new Rectangle(_destinationTopLeft.X, _destinationTopLeft.Y, _sourceArea.Width, _sourceArea.Height);
                for (int y = destinationArea.Top; y < destinationArea.Bottom; y++)
                {
                    for (int x = destinationArea.Left; x < destinationArea.Right; x++)
                    {
                        TileInfo tileInfo = _sourceTiles[(x - destinationArea.Left) + (y - destinationArea.Top) * _sourceArea.Width];
                        if (tileInfo.DarkTile != null)
                        {
                            this.SelectedRoomOnCreation.Map.DarkTiles[x, y] = tileInfo.DarkTile.Value;
                        }

                        if (tileInfo.LightTile != null)
                        {
                            this.SelectedRoomOnCreation.Map.LightTiles[x, y] = tileInfo.LightTile.Value;
                        }
                    }
                }
            }

            #endregion

            #region Get Destination Tiles

            private void CalculateDestinationTiles()
            {
                Rectangle destinationArea = new Rectangle(_destinationTopLeft.X, _destinationTopLeft.Y, _sourceArea.Width, _sourceArea.Height);
                _destinationTiles = new TileInfo[destinationArea.Width * destinationArea.Height];

                for (int y = destinationArea.Top; y < destinationArea.Bottom; y++)
                {
                    for (int x = destinationArea.Left; x < destinationArea.Right; x++)
                    {
                        TileInfo tileInfo = new TileInfo { Index = new Vector2i(x, y) };
                        if (_gameDimension.IsFlagSet(GameDimension.Dark))
                        {
                            tileInfo.DarkTile = this.SelectedRoomOnCreation.Map.DarkTiles[x, y];
                        }

                        if (_gameDimension.IsFlagSet(GameDimension.Light))
                        {
                            tileInfo.LightTile = this.SelectedRoomOnCreation.Map.LightTiles[x, y];
                        }

                        _destinationTiles[(x - destinationArea.Left) + (y - destinationArea.Top) * destinationArea.Width] = tileInfo;
                    }
                }
            }

            #endregion
        }

        #endregion
    }
}
