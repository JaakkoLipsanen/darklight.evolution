﻿using System.Collections.Generic;
using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using DarkLight.Editor.View;
using Flai;
using Flai.Graphics;
using Flai.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DarkLight.Editor.Tools
{
    public class GameObjectTool : EditorTool
    {
        #region GameObjectToolState

        private enum GameObjectToolState
        {
            None,
            Adding,
            Selecting,
            Moving,
            Copying,
        }

        #endregion

        #region Fields

        private GameObjectToolState _toolState = GameObjectToolState.None;

        // Add
        private readonly HashSet<GameObject> _addedGameObjects = new HashSet<GameObject>();

        // Move
        private GameObject _movingGameObject;
        private Vector2i _movingStartIndex;
        private Vector2 _moveDragStartPosition = Vector2i.MinValue;

        // Copying
        private GameObject _copiedGameObject;
        private Vector2i _copyingStartIndex;
        private Vector2 _copyDragStartPosition = Vector2i.MinValue;

        #endregion

        public GameObjectTool(EditorWorld world, EditorUser user)
            : base(world, user)
        {
        }

        #region Update

        public override void Update(UpdateContext updateContext)
        {
            GameObjectToolState oldToolState = _toolState;
            if (_toolState == GameObjectToolState.None)
            {
                if (_user.CurrentlyAddingGameObject != null)
                {
                    _toolState = GameObjectToolState.Adding;
                }
                else if (_user.SelectedGameObject != null && updateContext.InputState.IsKeyPressed(Keys.LeftAlt))
                {
                    _toolState = GameObjectToolState.Copying;
                }
                else if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
                {
                    _toolState = GameObjectToolState.Selecting;
                }
                else if (_user.SelectedGameObject != null && updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
                {
                    _toolState = GameObjectToolState.Moving;
                }
            }

            bool wasToolStateChanged = (oldToolState != _toolState);
            if (_toolState == GameObjectToolState.Adding)
            {
                this.UpdateAdding(updateContext, wasToolStateChanged);
            }
            else if (_toolState == GameObjectToolState.Selecting)
            {
                this.UpdateSelecting(updateContext, wasToolStateChanged);
            }
            else if (_toolState == GameObjectToolState.Moving)
            {
                this.UpdateMoving(updateContext, wasToolStateChanged);
            }
            else if (_toolState == GameObjectToolState.Copying)
            {
                this.UpdateCopying(updateContext, wasToolStateChanged);
            }
            else // if(_toolState == GameObjectToolState.None)
            {
                this.UpdateNone(updateContext, wasToolStateChanged);
            }
        }

        #endregion

        #region Update Adding

        private void UpdateAdding(UpdateContext updateContext, bool isFirstTime)
        {
            // Make sure that if this is the first time we are updating State.Adding, added game objects is empty
            if (isFirstTime)
            {
                _addedGameObjects.Clear();
            }

            if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
            {
                _user.SelectedGameObject = null;

                GameObject gameObject = GameObjectHelper.CreateDeepClone(_user.CurrentlyAddingGameObject);
                gameObject.GridIndex = _user.MouseTileIndex;
                gameObject.GameDimension = _world.ActiveGameDimension;

                _world.SelectedRoom.GameObjects.Add(gameObject);
                _addedGameObjects.Add(gameObject);
            }

            // If at least one game object is added and left shift is not pressed, flush the action to the stack and move to State.None
            if ((_addedGameObjects.Count != 0 && !updateContext.InputState.IsKeyPressed(Keys.LeftShift)) || updateContext.InputState.IsNewMouseButtonPress(MouseButton.Right))
            {
                _user.CurrentlyAddingGameObjectType = null;
                if (_addedGameObjects.Count != 0)
                {
                    _user.ActionStack.Push(new GameObjectAddMultipleAction(new HashSet<GameObject>(_addedGameObjects)));
                    _addedGameObjects.Clear();
                }

                _toolState = GameObjectToolState.None;
            }
        }

        #endregion

        #region Update Selecting

        private void UpdateSelecting(UpdateContext updateContext, bool isFirstTime)
        {
            _user.SelectedGameObject = _world.SelectedRoom.GameObjects.GetAt(_user.MousePosition);
            _toolState = GameObjectToolState.None;
        }

        #endregion

        #region Update Moving

        private void UpdateMoving(UpdateContext updateContext, bool isFirstTime)
        {
            if (!updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
            {
                // Add to action
                if (_movingGameObject != null && _movingGameObject.GridIndex - _movingStartIndex != Vector2i.Zero)
                {
                    _user.ActionStack.Push(new GameObjectMoveAction(_movingGameObject, _movingStartIndex - _movingGameObject.GridIndex));
                    _movingGameObject = null;
                }

                _movingGameObject = null;
                _toolState = GameObjectToolState.None;
                return;
            }

            if (_movingGameObject == null && _user.SelectedGameObject != null)
            {
                _moveDragStartPosition = _user.MousePosition;
                _movingGameObject = _user.SelectedGameObject;
                _movingStartIndex = _movingGameObject.GridIndex;
            }

            // move the game object
            if (_movingGameObject != null)
            {
                Vector2i dragTileOffset = Tile.WorldToTileCoordinate((_moveDragStartPosition - _user.MousePosition));
                _movingGameObject.GridIndex = _movingStartIndex - dragTileOffset; // Rounded??
            }
        }

        #endregion

        #region Update Copying

        private void UpdateCopying(UpdateContext updateContext, bool isFirstTime)
        {
            if (updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
            {
                if (_copyDragStartPosition == Vector2i.MinValue) // "is _copyDragStartPosition invalid?"
                {
                    _copyDragStartPosition = _user.MousePosition;
                }

                // Flush
                if (updateContext.InputState.IsKeyReleased(Keys.LeftAlt) || updateContext.InputState.IsMouseButtonReleased(MouseButton.Left))
                {
                    this.FlushCopying();
                }

                Vector2i dragTileOffset = Tile.WorldToTileCoordinate((_copyDragStartPosition - _user.MousePosition));
                if (_copiedGameObject == null && dragTileOffset != Vector2i.Zero)
                {
                    _copiedGameObject = GameObjectHelper.CreateDeepClone(_user.SelectedGameObject);
                    _copyingStartIndex = _copiedGameObject.GridIndex;

                    _user.SelectedGameObject = _copiedGameObject;
                    _world.SelectedRoom.GameObjects.Add(_copiedGameObject);
                }

                if (_copiedGameObject != null)
                {
                    // Update the copied game object position
                    _copiedGameObject.GridIndex = _copyingStartIndex - dragTileOffset;
                }
            }
            else if (updateContext.InputState.IsKeyReleased(Keys.LeftAlt) || _copiedGameObject != null)
            {
                this.FlushCopying();
            }
        }

        private void FlushCopying()
        {
            // Flush
            if (_copiedGameObject != null)
            {
                _user.ActionStack.Push(new GameObjectAddSingleAction(_copiedGameObject));
                _copiedGameObject = null;
            }

            _copyDragStartPosition = Vector2i.MinValue;
            _toolState = GameObjectToolState.None;
        }

        #endregion

        #region Update None

        private void UpdateNone(UpdateContext updateContext, bool isFirstTime)
        {
            // Delete, "Delete selected game object"
            if (updateContext.InputState.IsNewKeyPress(Keys.Delete))
            {
                if (_user.SelectedGameObject != null)
                {
                    _user.ActionStack.Push(new GameObjectDeleteAction(_user.SelectedGameObject));
                    _user.SelectedGameObject = null;
                }
            }
        }

        #endregion

        public override void Draw(GraphicsContext graphicsContext)
        {
            // TODO: Is _user.SelectedGameObject needed...? o
            if (_user.CurrentlyAddingGameObject != null)
            {
                _user.CurrentlyAddingGameObject.GridIndex = _user.MouseTileIndex;
                _user.CurrentlyAddingGameObject.GameDimension = _world.ActiveGameDimension;
                EditorRenderer.DrawGameObject(graphicsContext, _user.CurrentlyAddingGameObject, _world.ActiveGameDimension, 0.5f);
            }
        }

        #region GameObjectMoveAction

        private class GameObjectMoveAction : EditorAction
        {
            private readonly GameObject _movedGameObject;
            private readonly Vector2i _moveAmount;

            public GameObjectMoveAction(GameObject movedGameObject, Vector2i moveAmount)
            {
                _movedGameObject = movedGameObject;
                _moveAmount = moveAmount;
            }

            public override void Undo()
            {
                _movedGameObject.GridIndex += _moveAmount;
            }

            public override void Redo()
            {
                _movedGameObject.GridIndex -= _moveAmount;
            }
        }

        #endregion

        #region GameObjectAddAction

        private class GameObjectAddMultipleAction : EditorAction
        {
            private readonly HashSet<GameObject> _addedGameObjects;
            public GameObjectAddMultipleAction(HashSet<GameObject> addedGameObjects)
            {
                _addedGameObjects = addedGameObjects;
            }

            public override void Undo()
            {
                this.SelectedRoomOnCreation.GameObjects.RemoveAll(gameObject => _addedGameObjects.Contains(gameObject));
            }

            public override void Redo()
            {
                this.SelectedRoomOnCreation.GameObjects.AddAll(_addedGameObjects);
            }
        }

        private class GameObjectAddSingleAction : EditorAction
        {
            private readonly GameObject _addedGameObject;
            public GameObjectAddSingleAction(GameObject addedGameObject)
            {
                _addedGameObject = addedGameObject;
            }

            public override void Undo()
            {
                this.SelectedRoomOnCreation.GameObjects.Remove(_addedGameObject);
            }

            public override void Redo()
            {
                this.SelectedRoomOnCreation.GameObjects.Add(_addedGameObject);
            }
        }

        #endregion

        #region GameObjectDeleteAction

        private class GameObjectDeleteAction : EditorAction
        {
            private readonly GameObject _removedGameObject;
            public GameObjectDeleteAction(GameObject removedGameObject)
            {
                _removedGameObject = removedGameObject;
            }

            public override void Undo()
            {
                this.SelectedRoomOnCreation.GameObjects.Add(_removedGameObject);
            }

            // I *think* this works
            public override void Redo()
            {
                this.Execute();
            }

            protected override void OnInitialized()
            {
                this.Execute();
            }

            private void Execute()
            {
                this.SelectedRoomOnCreation.GameObjects.Remove(_removedGameObject);
            }
        }

        #endregion
    }
}
