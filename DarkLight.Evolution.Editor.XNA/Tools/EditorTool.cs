﻿using DarkLight.Editor.Model;
using Flai;
using Flai.Graphics;

namespace DarkLight.Editor.Tools
{
    public abstract class EditorTool
    {
        protected readonly EditorWorld _world;
        protected readonly EditorUser _user;

        protected EditorTool(EditorWorld world, EditorUser user)
        {
            _world = world;
            _user = user;
        }

        public virtual void Reset() { }

        public abstract void Update(UpdateContext updateContext);
        public virtual void Draw(GraphicsContext graphicsContext) { }
    }
}
