﻿using System.Collections.Generic;
using DarkLight.Editor.Model;
using Flai;
using Flai.General;
using Flai.Input;

namespace DarkLight.Editor.Tools
{
    public class FillTool : EditorTool
    {
        // Using HashSet<Vector2i>'s would make more sense, but List<T> is faster so lets just use it
        private readonly List<Vector2i> _lightWorldPositions = new List<Vector2i>();
        private readonly List<Vector2i> _darkWorldPositions = new List<Vector2i>();
        private readonly Queue<Vector2i> _fillQueue = new Queue<Vector2i>();

        public FillTool(EditorWorld world, EditorUser user)
            : base(world, user)
        {
        }

        public override void Update(UpdateContext updateContext)
        {
            if (_user.CurrentlyAddingTile == null)
            {
                return;
            }

            if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
            {
                if (_user.MouseTileIndex.X >= 0 && _user.MouseTileIndex.Y >= 0 && _user.MouseTileIndex.X < _world.SelectedRoom.Width && _user.MouseTileIndex.Y < _world.SelectedRoom.Height)
                {
                    this.FloodFill(_user.MouseTileIndex);
                }
            }
        }

        #region Flood Fill

        private void FloodFill(Vector2i tileIndex)
        {
            if (_user.CurrentlyAddingTile == null)
            {
                return;
            }

            TileType? darkWorldTargetTile = null;
            TileType? lightWorldTargetTile = null;

            // Dark tiles
            if (_world.ActiveGameDimension.IsFlagSet(GameDimension.Dark))
            {
                TileType targetTile = _world.SelectedRoom.Map.DarkTiles[tileIndex];
                if (targetTile != _user.CurrentlyAddingTile)
                {
                    this.FloodFillIterative(tileIndex, targetTile, _user.CurrentlyAddingTile.Value, _world.SelectedRoom.Map.DarkTiles, _darkWorldPositions);
                    darkWorldTargetTile = targetTile;
                }
            }

            // Light tiles
            if (_world.ActiveGameDimension.IsFlagSet(GameDimension.Light))
            {
                TileType targetTile = _world.SelectedRoom.Map.LightTiles[tileIndex];
                if (targetTile != _user.CurrentlyAddingTile)
                {
                    this.FloodFillIterative(tileIndex, targetTile, _user.CurrentlyAddingTile.Value, _world.SelectedRoom.Map.LightTiles, _lightWorldPositions);
                    lightWorldTargetTile = targetTile;
                }
            }

            // Flush the action
            if (_lightWorldPositions.Count != 0 || _darkWorldPositions.Count != 0)
            {
                _user.ActionStack.Push(new FillAction(_user.CurrentlyAddingTile.Value,
                    _lightWorldPositions.ToArray(), lightWorldTargetTile,
                    _darkWorldPositions.ToArray(), darkWorldTargetTile));
            }

            _lightWorldPositions.Clear();
            _darkWorldPositions.Clear();
        }

        // okay, this could be made a lot more efficient by this taking a "IEditableTileMap<T> as a parameter instead of GameDimension
        // Not the naive algorithm, check here: http://en.wikipedia.org/wiki/Flood_fill
        private void FloodFillIterative(Vector2i tileIndex, TileType targetTile, TileType replacementTile, IEditableTileMap<TileType> tileMap, List<Vector2i> changedIndices)
        {
            if (tileMap[tileIndex] != targetTile)
            {
                return;
            }

            _fillQueue.Clear();
            _fillQueue.Enqueue(tileIndex);

            // while there are indices in the queue, continue looping
            while (_fillQueue.Count != 0)
            {
                Vector2i currentIndex = _fillQueue.Dequeue();
                if (tileMap[currentIndex] != targetTile)
                {
                    continue;
                }

                // Change the current tile
                tileMap[currentIndex] = replacementTile;
                changedIndices.Add(currentIndex);

                // move left until it's not allowed
                int left = currentIndex.X;
                for (int x = left - 1; x >= 0; x--)
                {
                    Vector2i index = new Vector2i(x, currentIndex.Y);
                    if (tileMap[index] == targetTile)
                    {
                        tileMap[index] = replacementTile;
                        changedIndices.Add(index);

                        left = x;
                    }
                    else
                    {
                        break;
                    }
                }

                // move right until it's not allowed
                int right = currentIndex.X;
                for (int x = right + 1; x < _world.SelectedRoom.Width; x++)
                {
                    Vector2i index = new Vector2i(x, currentIndex.Y);
                    if (tileMap[index] == targetTile)
                    {
                        tileMap[index] = replacementTile;
                        changedIndices.Add(index);

                        right = x;
                    }
                    else
                    {
                        break;
                    }
                }

                // Add new indices to fill queue
                for (int x = left; x <= right; x++)
                {
                    // Enqueue the tile to up
                    if (currentIndex.Y != 0 && tileMap[x, currentIndex.Y - 1] == targetTile)
                    {
                        _fillQueue.Enqueue(new Vector2i(x, currentIndex.Y - 1));
                    }

                    // Enqueue the tile to down
                    if (currentIndex.Y != _world.SelectedRoom.Height - 1 && tileMap[x, currentIndex.Y + 1] == targetTile)
                    {
                        _fillQueue.Enqueue(new Vector2i(x, currentIndex.Y + 1));
                    }
                }
            }
        }

        #endregion

        #region FillAction

        private class FillAction : EditorAction
        {
            private readonly TileType _replacingTile;

            private readonly Vector2i[] _originalLightWorldPositions;
            private readonly TileType? _originalLightWorldTile;

            private readonly Vector2i[] _originalDarkWorldPositions;
            private readonly TileType? _originalDarkWorldTile;

            public FillAction(TileType replacingTile, Vector2i[] lightWorldPositions, TileType? originalLightWorldTile, Vector2i[] darkWorldPositions, TileType? originalDarkWorldTile)
            {
                _replacingTile = replacingTile;

                _originalLightWorldPositions = lightWorldPositions;
                _originalLightWorldTile = originalLightWorldTile;

                _originalDarkWorldPositions = darkWorldPositions;
                _originalDarkWorldTile = originalDarkWorldTile;
            }

            public override void Undo()
            {
                // Dark tiles
                if (_originalDarkWorldTile.HasValue)
                {
                    foreach (Vector2i index in _originalDarkWorldPositions)
                    {
                        this.SelectedRoomOnCreation.Map.DarkTiles[index] = _originalDarkWorldTile.Value;
                    }
                }

                // Light tiles
                if (_originalLightWorldTile.HasValue)
                {
                    foreach (Vector2i index in _originalLightWorldPositions)
                    {
                        this.SelectedRoomOnCreation.Map.LightTiles[index] = _originalLightWorldTile.Value;
                    }
                }
            }

            public override void Redo()
            {
                // Dark tiles
                foreach (Vector2i index in _originalDarkWorldPositions)
                {
                    this.SelectedRoomOnCreation.Map.DarkTiles[index] = _replacingTile;
                }

                // Light tiles
                foreach (Vector2i index in _originalLightWorldPositions)
                {
                    this.SelectedRoomOnCreation.Map.LightTiles[index] = _replacingTile;
                }
            }
        }

        #endregion
    }
}
