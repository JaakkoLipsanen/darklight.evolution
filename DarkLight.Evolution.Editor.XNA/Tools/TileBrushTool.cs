﻿using System.Collections.Generic;
using DarkLight.Editor.Model;
using DarkLight.Editor.View;
using Flai;
using Flai.Graphics;
using Flai.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DarkLight.Editor.Tools
{
    public class TileBrushTool : EditorTool
    {
        #region Enums

        private enum AllowedDirection
        {
            Both,
            Horizontal,
            Vertical
        }

        private enum TileBrushToolState
        {
            None,
            Brush,
            Eraser,
        }

        #endregion

        private readonly HashSet<TileInfo> _changedTiles = new HashSet<TileInfo>();

        private Vector2i _previousMouseIndex;
        private Vector2i? _startIndex;
        private AllowedDirection _allowedDirection = AllowedDirection.Both;
        private TileBrushToolState _toolState;

        public TileBrushTool(EditorWorld world, EditorUser user)
            : base(world, user)
        {
        }

        #region Update

        public override void Update(UpdateContext updateContext)
        {
            const MouseButton BrushButton = MouseButton.Left;
            const MouseButton EraserButton = MouseButton.Right;

            if (_toolState == TileBrushToolState.None)
            {
                if (updateContext.InputState.IsMouseButtonPressed(BrushButton))
                {
                    if (_user.CurrentlyAddingTile != null)
                    {
                        _toolState = TileBrushToolState.Brush;
                    }
                }
                else if (updateContext.InputState.IsNewMouseButtonPress(EraserButton))
                {
                    _toolState = TileBrushToolState.Eraser;
                }
            }

            if (_toolState == TileBrushToolState.Brush)
            {
                this.UpdateBrush(updateContext, _user.CurrentlyAddingTile.Value, BrushButton);
            }
            else if (_toolState == TileBrushToolState.Eraser)
            {
                this.UpdateBrush(updateContext, TileType.Air, EraserButton);
            }

            _previousMouseIndex = _user.MouseTileIndex;
        }

        #endregion

        #region Draw

        public override void Draw(GraphicsContext graphicsContext)
        {
            if (_user.CurrentlyAddingTile == null)
            {
                return;
            }

            Color color = EditorRenderer.GetGameDimensionColor(_world.ActiveGameDimension.Opposite());

            // Draw the tile
            for (int x = 0; x < _user.BrushSize; x++)
            {
                for (int y = 0; y < _user.BrushSize; y++)
                {
                    EditorRenderer.DrawTile(graphicsContext, _user.CurrentlyAddingTile.Value, (_user.MouseTileIndex + new Vector2(x, y)) * Tile.Size, _world.ActiveGameDimension, 0.5f);
                }
            }

            // Draw border around the tile
            graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext,
                new RectangleF(_user.MouseTileIndex.X * Tile.Size, _user.MouseTileIndex.Y * Tile.Size, _user.BrushSize * Tile.Size, _user.BrushSize * Tile.Size),
                color * 0.75f, 2f / _world.SelectedRoom.Camera.Zoom);
        }

        #endregion

        #region Update Brush

        // meh at this brushButton.. But I guess it's okay..
        private void UpdateBrush(UpdateContext updateContext, TileType targetTileType, MouseButton brushButton)
        {
            // Flush
            if (!updateContext.InputState.IsMouseButtonPressed(brushButton))
            {
                if (_changedTiles.Count != 0)
                {
                    _user.ActionStack.Push(new TileBrushAction(_changedTiles.ToArray(), targetTileType));
                    _changedTiles.Clear();
                }

                _toolState = TileBrushToolState.None;
                _allowedDirection = AllowedDirection.Both;
                _startIndex = null;

                return;
            }

            Vector2i adjustedMouseIndex = _user.MouseTileIndex;
            this.UpdateAllowedDirection(updateContext, ref adjustedMouseIndex);
            foreach (Vector2i point in FlaiAlgorithms.GetPointsOnSegment(_previousMouseIndex, adjustedMouseIndex))
            {
                int xMin = FlaiMath.Clamp(point.X, 0, _world.SelectedRoom.Width);
                int xMax = FlaiMath.Clamp(point.X + _user.BrushSize, 0, _world.SelectedRoom.Width);
                int yMin = FlaiMath.Clamp(point.Y, 0, _world.SelectedRoom.Height);
                int yMax = FlaiMath.Clamp(point.Y + _user.BrushSize, 0, _world.SelectedRoom.Height);

                for (int x = xMin; x < xMax; x++)
                {
                    for (int y = yMin; y < yMax; y++)
                    {
                        Vector2i innerPoint = new Vector2i(x, y);
                        if (!_user.SelectionRectangle.HasValue || _user.SelectionRectangle.TileArea.Contains(innerPoint))
                        {
                            TileInfo tileInfo = new TileInfo { Index = innerPoint };
                            if (_world.ActiveGameDimension.IsFlagSet(GameDimension.Dark) && _world.SelectedRoom.Map.DarkTiles[innerPoint] != targetTileType)
                            {
                                tileInfo.DarkTile = _world.SelectedRoom.Map.DarkTiles[innerPoint];
                                _world.SelectedRoom.Map.DarkTiles[innerPoint] = targetTileType;
                            }

                            if (_world.ActiveGameDimension.IsFlagSet(GameDimension.Light) && _world.SelectedRoom.Map.LightTiles[innerPoint] != targetTileType)
                            {
                                tileInfo.LightTile = _world.SelectedRoom.Map.LightTiles[innerPoint];
                                _world.SelectedRoom.Map.LightTiles[innerPoint] = targetTileType;
                            }

                            if (tileInfo.DarkTile != null || tileInfo.LightTile != null)
                            {
                                _changedTiles.Add(tileInfo);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Update Allowed Direction

        private void UpdateAllowedDirection(UpdateContext updateContext, ref Vector2i mouseIndex)
        {
            if (updateContext.InputState.IsKeyPressed(Keys.LeftShift))
            {
                if (!_startIndex.HasValue)
                {
                    _startIndex = _previousMouseIndex;
                }

                if (_allowedDirection == AllowedDirection.Both)
                {
                    if (_startIndex.Value.X != mouseIndex.X)
                    {
                        _allowedDirection = AllowedDirection.Horizontal;
                    }
                    else if (_startIndex.Value.Y != mouseIndex.Y)
                    {
                        _allowedDirection = AllowedDirection.Vertical;
                    }
                }

                if (_allowedDirection == AllowedDirection.Vertical)
                {
                    _previousMouseIndex.X = _startIndex.Value.X;
                    mouseIndex.X = _startIndex.Value.X;
                }
                else if (_allowedDirection == AllowedDirection.Horizontal)
                {
                    _previousMouseIndex.Y = _startIndex.Value.Y;
                    mouseIndex.Y = _startIndex.Value.Y;
                }
            }
            else
            {
                _allowedDirection = AllowedDirection.Both;
                _startIndex = null;
            }
        }

        #endregion

        #region Tile Brush Action

        private class TileBrushAction : EditorAction
        {
            private readonly TileInfo[] _tileInfos;
            private readonly TileType _replacingTile;

            public TileBrushAction(TileInfo[] tilePositions, TileType replacingTile)
            {
                _tileInfos = tilePositions;
                _replacingTile = replacingTile;
            }

            public override void Undo()
            {
                foreach (TileInfo tileInfo in _tileInfos)
                {
                    if (tileInfo.DarkTile.HasValue)
                    {
                        this.SelectedRoomOnCreation.Map.DarkTiles[tileInfo.Index] = tileInfo.DarkTile.Value;
                    }

                    if (tileInfo.LightTile.HasValue)
                    {
                        this.SelectedRoomOnCreation.Map.LightTiles[tileInfo.Index] = tileInfo.LightTile.Value;
                    }
                }
            }

            public override void Redo()
            {
                foreach (TileInfo tileInfo in _tileInfos)
                {
                    if (tileInfo.DarkTile.HasValue)
                    {
                        this.SelectedRoomOnCreation.Map.DarkTiles[tileInfo.Index] = _replacingTile;
                    }

                    if (tileInfo.LightTile.HasValue)
                    {
                        this.SelectedRoomOnCreation.Map.LightTiles[tileInfo.Index] = _replacingTile;
                    }
                }
            }
        }

        #endregion
    }
}
