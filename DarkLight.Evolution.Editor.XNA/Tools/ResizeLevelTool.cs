﻿using DarkLight.Editor.Model;
using Flai;
using Flai.Graphics;
using Flai.Input;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.Tools
{
    public class ResizeLevelTool : EditorTool
    {
        private Direction2D? _resizeSide = null;
        private int _resizeSideIndex = 0;

        public ResizeLevelTool(EditorWorld world, EditorUser user)
            : base(world, user)
        {
        }

        #region Update

        public override void Update(UpdateContext updateContext)
        {
            Vector2 mouseWorldCoordinates = _world.SelectedRoom.Camera.ScreenToWorld(updateContext.InputState.MousePosition);

            // If resize side is null, then check user has just started dragging from any border
            if (_resizeSide == null)
            {
                this.CheckIsStartingResizing(updateContext, _world.SelectedRoom.Camera, mouseWorldCoordinates);
            }
            // otherwise update the dragging
            else
            {
                this.UpdateResizing(updateContext, mouseWorldCoordinates);
            }
        }

        #endregion

        #region Check Is Starting Resizing

        private void CheckIsStartingResizing(UpdateContext updateContext, ICamera2D camera, Vector2 mouseWorldCoordinates)
        {
            if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
            {
                float maximumDistance = Tile.Size / camera.Zoom;

                // Test left and right side
                if (mouseWorldCoordinates.Y > 0 && mouseWorldCoordinates.Y < _world.SelectedRoom.Height * Tile.Size)
                {
                    // Left side
                    if (MathHelper.Distance(mouseWorldCoordinates.X, 0) < maximumDistance)
                    {
                        _resizeSide = Direction2D.Left;
                        _resizeSideIndex = 0;
                        return;
                    }
                    // Right side
                    else if (MathHelper.Distance(mouseWorldCoordinates.X, _world.SelectedRoom.Width * Tile.Size) < maximumDistance)
                    {
                        _resizeSide = Direction2D.Right;
                        _resizeSideIndex = _world.SelectedRoom.Width;
                        return;
                    }
                }

                // Test top and bottom sides
                if (mouseWorldCoordinates.X > 0 && mouseWorldCoordinates.X < _world.SelectedRoom.Width * Tile.Size)
                {
                    // Top side
                    if (MathHelper.Distance(mouseWorldCoordinates.Y, 0) < maximumDistance)
                    {
                        _resizeSide = Direction2D.Up;
                        _resizeSideIndex = 0;
                        return;
                    }
                    // Bottom side
                    else if (MathHelper.Distance(mouseWorldCoordinates.Y, _world.SelectedRoom.Height * Tile.Size) < maximumDistance)
                    {
                        _resizeSide = Direction2D.Down;
                        _resizeSideIndex = _world.SelectedRoom.Height;
                        return;
                    }
                }
            }
        }

        #endregion

        #region Update Resizing

        private void UpdateResizing(UpdateContext updateContext, Vector2 mouseWorldCoordinates)
        {
            // Flush
            if (!updateContext.InputState.IsMouseButtonPressed(MouseButton.Left))
            {
                if (!_resizeSide.HasValue)
                {
                    return;
                }

                int resizeAmount = _resizeSideIndex;
                if (_resizeSide == Direction2D.Right || _resizeSide == Direction2D.Down)
                {
                    // Decrease room width/height from the resize amount
                    resizeAmount -= (_resizeSide == Direction2D.Right) ? _world.SelectedRoom.Width : _world.SelectedRoom.Height;
                }

                // make sure that the resizeAmount will not make the room too small (under EditorRoomMap.MinimumSize)
                Vector2i newSize = new Vector2i(_world.SelectedRoom.Width, _world.SelectedRoom.Height) + _resizeSide.Value.ToUnitVector() * resizeAmount;
                if (newSize.X < EditorRoomMap.MinimumSize || newSize.Y < EditorRoomMap.MinimumSize)
                {
                    resizeAmount += (EditorRoomMap.MinimumSize - FlaiMath.Min(newSize.X, newSize.Y)) * _resizeSide.Value.Sign();
                }

                if (resizeAmount != 0)
                {
                    _user.ActionStack.Push(new ResizeLevelAction(_resizeSide.Value, resizeAmount));
                }

                _resizeSide = null;
            }
            // Update side position
            else
            {
                if (_resizeSide == Direction2D.Left || _resizeSide == Direction2D.Right)
                {
                    _resizeSideIndex = (int)FlaiMath.Round(mouseWorldCoordinates.X / Tile.Size);
                    if (_resizeSide == Direction2D.Left)
                    {
                        _resizeSideIndex = FlaiMath.Min(_resizeSideIndex, _world.SelectedRoom.Width - 1);
                    }
                    else // if(_currentResizeSide == Direction2D.Right)
                    {
                        _resizeSideIndex = FlaiMath.Max(_resizeSideIndex, 1);
                    }

                }
                else // if(_currentResizeSide == Direction2D.Up || _currentResizeSide == Direction2D.Down)
                {
                    _resizeSideIndex = (int)FlaiMath.Round(mouseWorldCoordinates.Y / Tile.Size);
                    if (_resizeSide == Direction2D.Up)
                    {
                        _resizeSideIndex = FlaiMath.Min(_resizeSideIndex, _world.SelectedRoom.Height - 1);
                    }
                    else // if(_currentResizeSide == Direction2D.Down)
                    {
                        _resizeSideIndex = FlaiMath.Max(_resizeSideIndex, 1);
                    }
                }
            }
        }

        #endregion

        #region Draw

        public override void Draw(GraphicsContext graphicsContext)
        {
            int left = _resizeSide == Direction2D.Left ? _resizeSideIndex : 0;
            int right = _resizeSide == Direction2D.Right ? _resizeSideIndex : _world.SelectedRoom.Width;
            int top = _resizeSide == Direction2D.Up ? _resizeSideIndex : 0;
            int bottom = _resizeSide == Direction2D.Down ? _resizeSideIndex : _world.SelectedRoom.Height;

            const float BorderThickness = 6f;
            float thickness = BorderThickness / _world.SelectedRoom.Camera.Zoom;
            graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext, new RectangleF(new Vector2(left, top) * Tile.Size - Vector2.One * thickness / 2f, new Vector2(right, bottom) * Tile.Size + Vector2.One * thickness / 2f), Color.Purple, thickness);
        }

        #endregion

        #region ResizeLevelAction

        private class ResizeLevelAction : EditorAction
        {
            private readonly Direction2D _resizeDirection;
            private readonly int _resizeAmount;
            private TileInfo[] _undoTileData; // can't be readonly :(

            public ResizeLevelAction(Direction2D resizeDirection, int resizeAmount)
            {
                _resizeDirection = resizeDirection;
                _resizeAmount = resizeAmount;
            }

            public override void Undo()
            {
                // Reset size
                this.SelectedRoomOnCreation.Map.Resize(_resizeDirection, -_resizeAmount);

                // Reset tiles
                if (_undoTileData != null)
                {
                    foreach (TileInfo tileInfo in _undoTileData)
                    {
                        this.SelectedRoomOnCreation.Map.DarkTiles[tileInfo.Index] = tileInfo.DarkTile.Value;
                        this.SelectedRoomOnCreation.Map.LightTiles[tileInfo.Index] = tileInfo.LightTile.Value;
                    }
                }
            }

            public override void Redo()
            {
                this.Execute();
            }

            protected override void OnInitialized()
            {
                this.SaveUndoData();
                this.Execute();
            }

            private void Execute()
            {
                this.SelectedRoomOnCreation.Map.Resize(_resizeDirection, _resizeAmount);
            }

            #region Save Undo Data

            private void SaveUndoData()
            {
                if (_resizeDirection == Direction2D.Left || _resizeDirection == Direction2D.Up)
                {
                    // If user is resizing to outside of the level, there is no need to save tile any tile data
                    if (_resizeAmount < 0)
                    {
                        return;
                    }

                    if (_resizeDirection == Direction2D.Left)
                    {
                        this.SaveUndoData(new Rectangle(0, 0, _resizeAmount, this.SelectedRoomOnCreation.Height));
                    }
                    else // if (_resizeDirection == Direction2D.Up)
                    {
                        this.SaveUndoData(new Rectangle(0, 0, this.SelectedRoomOnCreation.Width, _resizeAmount));
                    }
                }
                else // if (_resizeDirection == Direction2D.Right || _resizeDirection == Direction2D.Down)
                {
                    // If user is resizing to outside of the level, there is no need to save tile any tile data
                    if (_resizeAmount > 0)
                    {
                        return;
                    }

                    if (_resizeDirection == Direction2D.Right)
                    {
                        this.SaveUndoData(new Rectangle(this.SelectedRoomOnCreation.Width + _resizeAmount, 0, -_resizeAmount, this.SelectedRoomOnCreation.Height));
                    }
                    else // if (_resizeDirection == Direction2D.Down)
                    {
                        this.SaveUndoData(new Rectangle(0, this.SelectedRoomOnCreation.Height + _resizeAmount, this.SelectedRoomOnCreation.Width, -_resizeAmount));
                    }
                }
            }

            private void SaveUndoData(Rectangle area)
            {
                _undoTileData = new TileInfo[area.Width * area.Height];
                for (int y = area.Top; y < area.Bottom; y++)
                {
                    for (int x = area.Left; x < area.Right; x++)
                    {
                        _undoTileData[(x - area.Left) + (y - area.Top) * area.Width] = new TileInfo
                        {
                            Index = new Vector2i(x, y),
                            LightTile = this.SelectedRoomOnCreation.Map.LightTiles[x, y],
                            DarkTile = this.SelectedRoomOnCreation.Map.DarkTiles[x, y]
                        };
                    }
                }
            }

            #endregion
        }

        #endregion
    }
}
