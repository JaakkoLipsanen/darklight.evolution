﻿using System;
using System.Collections.Generic;
using System.Linq;
using DarkLight.Editor.Model;
using Flai;
using Flai.Input;

namespace DarkLight.Editor.Controller
{
    public class EditorWorldController : FlaiController
    {
        private readonly EditorWorld _world;
        private readonly Dictionary<EditorRoom, EditorRoomCameraController> _cameraControllers = new Dictionary<EditorRoom, EditorRoomCameraController>();

        public EditorWorldController(EditorWorld world)
        {
            _world = world;
            _cameraControllers = _world.Rooms.ToDictionary(
                room => room, // key
                room => new EditorRoomCameraController(room)); // value

            _world.Rooms.RoomAdded += this.OnRoomAdded;
            _world.Rooms.RoomRemoved += this.OnRoomRemoved;
        }

        private void OnRoomAdded(EditorRoom room)
        {
            _cameraControllers.Add(room, new EditorRoomCameraController(room));
        }

        private void OnRoomRemoved(EditorRoom room)
        {
            if (!_cameraControllers.Remove(room))
            {
                throw new ArgumentException("_cameraControllers is not in sync!");
            }
        }

        public override void Control(UpdateContext updateContext)
        {
            _cameraControllers[_world.SelectedRoom].Control(updateContext);
        }
    }
}
