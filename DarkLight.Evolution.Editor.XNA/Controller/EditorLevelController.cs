﻿using DarkLight.Editor.Model;
using Flai;
using Flai.Input;

namespace DarkLight.Editor.Controller
{
    public class EditorLevelController : FlaiController
    {
        private readonly EditorLevel _level;
        private readonly EditorWorldController _worldController;

        public EditorLevelController(EditorLevel level)
        {
            _level = level;
            _worldController = new EditorWorldController(_level.World);
        }

        public override void Control(UpdateContext updateContext)
        {
            _worldController.Control(updateContext);
        }
    }
}
