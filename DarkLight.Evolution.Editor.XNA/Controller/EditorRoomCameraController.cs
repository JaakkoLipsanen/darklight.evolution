﻿using DarkLight.Editor.Model;
using Flai;
using Flai.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DarkLight.Editor.Controller
{
    public class EditorRoomCameraController : FlaiController
    {
        private readonly EditorRoom _room;
        public EditorRoomCameraController(EditorRoom room)
        {
            _room = room;
        }

        public override void Control(UpdateContext updateContext)
        {
            if (_room.Camera.IsLocked)
            {
                return;
            }

            this.HandleMovement(updateContext);
            this.HandleZooming(updateContext);
        }

        private void HandleMovement(UpdateContext updateContext)
        {
            const int CameraSpeed = Tile.Size * 20;
            Vector2 cameraMovement = Vector2.Zero;
            if (updateContext.InputState.IsKeyPressed(Keys.Left))
            {
                cameraMovement -= Vector2.UnitX * CameraSpeed;
            }

            if (updateContext.InputState.IsKeyPressed(Keys.Right))
            {
                cameraMovement += Vector2.UnitX * CameraSpeed;
            }

            if (updateContext.InputState.IsKeyPressed(Keys.Up))
            {
                cameraMovement -= Vector2.UnitY * CameraSpeed;
            }

            if (updateContext.InputState.IsKeyPressed(Keys.Down))
            {
                cameraMovement += Vector2.UnitY * CameraSpeed;
            }

            // multiply the camera movement amount with delta seconds
            cameraMovement *= updateContext.DeltaSeconds;

            // Middle mouse button dragging
            if (updateContext.InputState.WasMouseButtonPressed(MouseButton.Middle) && updateContext.InputState.IsMouseButtonPressed(MouseButton.Middle))
            {
                cameraMovement -= updateContext.InputState.MousePositionDelta;
            }

            // Scale the value depending on the camera's zoom level and set the value to camera
            _room.Camera.Position += cameraMovement / _room.Camera.Zoom;
        }

        private void HandleZooming(UpdateContext updateContext)
        {
            if (updateContext.InputState.ScrollWheelDelta != 0)
            {
                const float ScrollWheelMultiplier = 0.175f;
                Vector2 oldMousePosition = _room.Camera.ScreenToWorld(updateContext.InputState.MousePosition);
                float previousZoom = _room.Camera.Zoom;

                // Calculate the new zoom
                _room.Camera.Zoom += (updateContext.InputState.ScrollWheelDelta / 120f) * ScrollWheelMultiplier * _room.Camera.Zoom;

                // Adjust the camera position so that the camera zooms "in to" the mouse
                _room.Camera.Position = oldMousePosition + (previousZoom / _room.Camera.Zoom) * (_room.Camera.Position - oldMousePosition);
            }
            else
            {
                // Yaaay!! :P Okay, what this does, is that if the room's size is changed (for example by undoing/redoing
                // some change-room-size action), the min/max zoom will be appropiately clamped
                _room.Camera.Zoom += 0;
            }
        }
    }
}
