﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DarkLight.Editor.DataStructures;
using DarkLight.Editor.Model.GameObjects;
using DarkLight.Editor.View;
using Flai;
using Flai.General;

namespace DarkLight.Editor.Model
{
    public class EditorRoom : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly EditorRoomMap _roomMap;
        private readonly EditorGameObjectContainer _gameObjectContainer;
        private readonly EditorRoomCamera _camera;

        private IRoomManager _roomManager;
        private string _id = null;

        [Browsable(false)]
        public EditorRoomMap Map
        {
            get { return _roomMap; }
        }

        [Browsable(false)]
        public EditorGameObjectContainer GameObjects
        {
            get { return _gameObjectContainer; }
        }

        [Browsable(false)]
        public EditorRoomCamera Camera
        {
            get { return _camera; }
        }

        [Browsable(false)]
        public int Width
        {
            get { return _roomMap.Width; }
        }

        [Browsable(false)]
        public int Height
        {
            get { return _roomMap.Height; }
        }

        public string Id
        {
            get { return _id; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return;
                }

                if (_id != value)
                {
                    _id = value;
                    this.PropertyChanged.InvokeIfNotNull(this, "Id");
                }
            }
        }

        public EditorRoom(EditorRoomMap roomMap, IEnumerable<GameObject> gameObjects, PropertyCollection properties)
        {
            _roomMap = roomMap;
            _gameObjectContainer = new EditorGameObjectContainer();
            _camera = new EditorRoomCamera(this);

            _gameObjectContainer.GameObjectAdding += this.OnGameObjectAdding;
            _roomMap.Resized += this.OnRoomMapResized;

            _gameObjectContainer.AddAll(gameObjects); // meh.. otherwise room hasn't subscribed to container.GameObjectAdding
        }

        public void Initialize(IRoomManager roomManager, string initialId)
        {
            if (_roomManager != null)
            {
                if (_roomManager != roomManager)
                {
                    throw new ArgumentException("Invalid manager!");
                }

                if (string.IsNullOrWhiteSpace(this.Id) || _roomManager.IsIdUsed(this.Id))
                {
                    this.Id = initialId;
                }

                return;
            }

            _roomManager = roomManager;

            // don't overwrite if loaded from file
            if (string.IsNullOrWhiteSpace(_id))
            {
                this.Id = initialId;
            }
        }

        private void OnRoomMapResized(Direction2D resizeDirection, int resizeAmount)
        {
            if (resizeDirection == Direction2D.Left || resizeDirection == Direction2D.Up)
            {
                _gameObjectContainer.MoveAll(resizeDirection.ToUnitVector() * resizeAmount);
                _camera.Position += resizeDirection.ToUnitVector() * resizeAmount * Tile.Size;
            }
        }

        private void OnGameObjectAdding(GameObject addedGameObject)
        {
            ((IGameObject)addedGameObject).RegisterToRoom(this, _gameObjectContainer.GenerateIdForType(addedGameObject.Type));
        }
    }
}