﻿using System;
using Flai;

namespace DarkLight.Editor.Model
{
    public struct TileInfo : IEquatable<TileInfo>
    {
        public Vector2i Index;
        public TileType? DarkTile;
        public TileType? LightTile;

        #region Overrides of Object

        public override int GetHashCode()
        {
            return this.Index.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is TileInfo)
            {
                return this.Equals((TileInfo)obj);
            }
            return base.Equals(obj);
        }

        #endregion

        #region IEquatable<TileInfo> Members

        public bool Equals(TileInfo other)
        {
            return other.Index == this.Index;
        }

        #endregion

        public override string ToString()
        {
            return 
                "Index: " + this.Index + ", " +
                "LightTile: " + (this.LightTile == null ? "None" : this.LightTile.ToString()) + ", " +
                "DarkTile: " + (this.DarkTile == null ? "None" : this.DarkTile.ToString());
        }
    }
}
