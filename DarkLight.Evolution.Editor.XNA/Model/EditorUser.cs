﻿using System;
using System.ComponentModel;
using DarkLight.Editor.DataStructures;
using DarkLight.Editor.Model.GameObjects;
using Flai;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.Model
{
    // I think it'll be best to make this a singleton.
    public class EditorUser : INotifyPropertyChanged
    {
        #region Fields

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly EditorWorld _world;
        private readonly EditorToolManager _toolManager;
        private readonly EditorActionStack _actionStack;
        private readonly EditorSelectionRectangle _selectionRectangle = new EditorSelectionRectangle();

        // GameObjectTracker class etc?
        private GameObject _selectedGameObject = null;

        private int _brushSize = 1;
        private TileType? _currentlyAddingTile = TileType.Solid;
        private GameObject _currentlyAddingGameObject = null;

        // uglyy!!
        private bool _isAddRoomActionRunning;
        private bool _isRemoveRoomActionRunning;
        private bool _isChangeRoomActionRunning;

        #endregion

        #region Properties

        public EditorActionStack ActionStack
        {
            get { return _actionStack; }
        }

        public EditorToolManager ToolManager
        {
            get { return _toolManager; }
        }

        public EditorSelectionRectangle SelectionRectangle
        {
            get { return _selectionRectangle; }
        }

        public GameObject SelectedGameObject
        {
            get { return _selectedGameObject; }
            set
            {
                if (_selectedGameObject != value)
                {
                    _selectedGameObject = value;
                    this.OnPropertyChanged("SelectedGameObject");
                }
            }
        }

        // I don't think that there's any reason why this should be null.. hmmph
        public TileType? CurrentlyAddingTile
        {
            get { return _currentlyAddingTile; }
            set
            {
                if (_currentlyAddingTile != value)
                {
                    _currentlyAddingTile = value;
                    this.OnPropertyChanged("CurrentlyAddingTile");
                }
            }
        }

        public GameObject CurrentlyAddingGameObject
        {
            get { return _currentlyAddingGameObject; }
        }

        public Type CurrentlyAddingGameObjectType
        {
            get { return _currentlyAddingGameObject != null ? _currentlyAddingGameObject.Type : null; }
            set
            {
                Type oldType = _currentlyAddingGameObject != null ? _currentlyAddingGameObject.Type : null;
                if (oldType != value)
                {
                    _currentlyAddingGameObject = (value != null) ? GameObjectHelper.CreateDefaultGameObject(value) : null;
                    this.OnPropertyChanged("CurrentlyAddingGameObjectType");
                }
            }
        }

        // In theory the OnPropertyChanges is redundant because this value is only changed by the WPF UI..
        public int BrushSize
        {
            get { return _brushSize; }
            set
            {
                if (_brushSize != value)
                {
                    _brushSize = value;
                    this.OnPropertyChanged("BrushSize");
                }
            }
        }

        public Vector2 MousePosition { get; private set; }
        public Vector2i MouseTileIndex { get; private set; }
        public Vector2i RoundedMouseTileIndex { get; private set; }

        #endregion

        public EditorUser(EditorWorld world)
        {
            _world = world;
            _toolManager = new EditorToolManager();
            _actionStack = new EditorActionStack(_world, this);

            _toolManager.CreateTools(_world, this);
            _world.Rooms.RoomAdded += this.OnRoomAdded;
            _world.Rooms.RoomRemoved += this.OnRoomRemoved;
            _world.Rooms.SelectedRoomChanged += this.OnSelectedRoomChanged;
            _world.SelectedRoom.GameObjects.GameObjectRemoved += this.OnGameObjectRemoved;
        }

        public void Update(UpdateContext updateContext)
        {
            this.UpdateMousePositions(updateContext);
            _toolManager.ActiveTool.Update(updateContext);
        }

        private void UpdateMousePositions(UpdateContext updateContext)
        {
            // In Controller or here..?
            this.MousePosition = _world.SelectedRoom.Camera.ScreenToWorld(updateContext.InputState.MousePosition);
            this.MouseTileIndex = Tile.WorldToTileCoordinate(this.MousePosition);
            this.RoundedMouseTileIndex = Tile.WorldToTileCoordinateRounded(this.MousePosition);
            this.OnPropertyChanged("MouseTileIndex"); // Notify to WPF that the mouse tile index has changed
        }

        #region Event Handlers

        private void OnGameObjectRemoved(GameObject gameObject)
        {
            if (_selectedGameObject == gameObject)
            {
                this.SelectedGameObject = null;
            }
        }

        // ALL OF THESE ARE SUPER UGLY :||
        private void OnRoomAdded(EditorRoom room)
        {
            if (!_isAddRoomActionRunning)
            {
                _actionStack.Push(new DelegateEditorAction(() =>
                {
                    _isRemoveRoomActionRunning = true;
                    _world.Rooms.Remove(room);
                    _isRemoveRoomActionRunning = false;
                }, () =>
                {
                    _isAddRoomActionRunning = true;
                    _world.Rooms.Add(room);
                    _isAddRoomActionRunning = false;
                }));
            }
        }

        private void OnRoomRemoved(EditorRoom room)
        {
            if (!_isRemoveRoomActionRunning)
            {
                _actionStack.Push(new DelegateEditorAction(() =>
                {
                    _isAddRoomActionRunning = true;
                    _world.Rooms.Add(room);
                    _isAddRoomActionRunning = false;
                }, () =>
                {
                    _isRemoveRoomActionRunning = true;
                    _world.Rooms.Remove(room);
                    _isRemoveRoomActionRunning = false;
                }));
            }
        }


        private void OnSelectedRoomChanged(EditorRoom oldRoom, EditorRoom newRoom)
        {
            if (!_isChangeRoomActionRunning)
            {
                _actionStack.Push(new DelegateEditorAction(() =>
                {
                    // Undo
                    _isChangeRoomActionRunning = true;
                    _world.SelectedRoom = oldRoom;
                    _isChangeRoomActionRunning = false;
                }, () =>
                {
                    // Redo
                    _isChangeRoomActionRunning = true;
                    _world.SelectedRoom = newRoom;
                    _isChangeRoomActionRunning = false;
                }));
            }

            // this could be somehow wiser. either by doing _world.GameObjectRemoved event or by
            // subscribing to every room's GameObjectRemoved event.. hmmm
            oldRoom.GameObjects.GameObjectRemoved -= this.OnGameObjectRemoved;
            newRoom.GameObjects.GameObjectRemoved += this.OnGameObjectRemoved;

            this.SelectedGameObject = null;
            _toolManager.ActiveTool.Reset();
        }

        #endregion

        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged.InvokeIfNotNull(this, propertyName);
        }
    }
}