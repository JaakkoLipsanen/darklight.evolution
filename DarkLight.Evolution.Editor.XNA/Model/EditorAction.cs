﻿using System;
using Flai.DataStructures;

namespace DarkLight.Editor.Model
{
    // Hmm.. I could of course also make _world to be initialized in the constructor from static EditorWorld.Instance (or via services)...
    public abstract class EditorAction : IUndoRedo
    {
        private bool _initialized = false;

        protected EditorWorld World { get; private set; }
        protected EditorRoom SelectedRoomOnCreation { get; private set; } // blaah
        protected EditorUser User { get; private set; }

        public void Initialize(EditorWorld world, EditorUser user)
        {
            if (_initialized)
            {
                throw new InvalidOperationException("The action is already initialized!");
            }

            this.World = world;
            this.SelectedRoomOnCreation = this.World.SelectedRoom;
            this.User = user;

            _initialized = true;
            this.OnInitialized();
        }

        public abstract void Undo();
        public abstract void Redo();

        protected virtual void OnInitialized() { }
    }

    public class DelegateEditorAction : EditorAction
    {
        private readonly Action _undoAction;
        private readonly Action _redoAction;

        public DelegateEditorAction(Action undoAction, Action redoAction)
        {
            _undoAction = undoAction;
            _redoAction = redoAction;
        }

        public override void Undo()
        {
            _undoAction();
        }

        public override void Redo()
        {
            _redoAction();
        }

        public static DelegateEditorAction FromFunction<T>(Action<T> undoAction, Action<T> redoAction, T value)
        {
            return new DelegateEditorAction(() => undoAction(value), () => redoAction(value));
        }
    }
}