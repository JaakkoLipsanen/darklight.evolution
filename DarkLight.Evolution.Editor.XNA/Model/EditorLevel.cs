﻿using System.IO;
using DarkLight.Editor.IO;
using Flai;

namespace DarkLight.Editor.Model
{
    public class EditorLevel
    {
        private readonly EditorUser _user;
        private readonly EditorWorld _world;

        public EditorWorld World
        {
            get { return _world; }
        }

        public EditorUser User
        {
            get { return _user; }
        }

        public EditorLevel(EditorWorld world, EditorUser user)
        {
            _world = world;
            _user = user;
        }

        public void Update(UpdateContext updateContext)
        {
            _user.Update(updateContext);
        }

        public void Save(BinaryWriter writer)
        {
            EditorLevelSerializer.Save(writer, this);
        }

        public static EditorLevel FromStream(BinaryReader reader)
        {
            return EditorLevelSerializer.FromStream(reader);
        }

        public static EditorLevel CreateEmpty()
        {
            return EditorLevelSerializer.CreateEmpty();
        }
    }
}
