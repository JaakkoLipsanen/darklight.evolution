﻿
using Flai;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.Model
{
    public enum TileType
    {
        Air = 0,
        Solid,
        Lava,
    }

    public static class Tile
    {
        public const int Size = 16;

        public static int WorldToTileCoordinate(float x)
        {
            return (int)(x < 0 ? (x - Tile.Size) : x) / Tile.Size;
        }

        public static Vector2i WorldToTileCoordinate(float x, float y)
        {
            return Tile.WorldToTileCoordinate(new Vector2(x, y));
        }

        public static Vector2i WorldToTileCoordinate(Vector2 position)
        {
            return new Vector2i(Tile.WorldToTileCoordinate(position.X), Tile.WorldToTileCoordinate(position.Y));
        }

        public static Vector2i WorldToTileCoordinateRounded(Vector2 position)
        {
            return new Vector2i(
                (int)FlaiMath.Round(position.X / Tile.Size),
                (int)FlaiMath.Round(position.Y / Tile.Size));
        }
    }
}
