﻿using System;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.Model
{
    public class EditorSelectionRectangle
    {
        private Rectangle? _selectionArea = null; // Selection area is in tiles

        public bool HasValue
        {
            get { return _selectionArea.HasValue; }
        }

        public Rectangle TileArea
        {
            get
            {
                if (!this.HasValue)
                {
                    throw new ArgumentException("Selection rectangle does not have a value!");
                }

                return _selectionArea.Value;
            }
            set
            {
                if (value == Rectangle.Empty || value.Width == 0 || value.Height == 0)
                {
                    _selectionArea = null;
                }
                else
                {
                    _selectionArea = value;
                }
            }
        }

        public Rectangle RealArea
        {
            get
            {
                if (this.HasValue)
                {
                    return new Rectangle(
                        _selectionArea.Value.X * Tile.Size, _selectionArea.Value.Y * Tile.Size,
                        _selectionArea.Value.Width * Tile.Size, _selectionArea.Value.Height * Tile.Size);
                }

                throw new ArgumentException("Selection rectangle does not have a value!");
            }
        }

        public void Unselect()
        {
            _selectionArea = null;
        }
    }
}
