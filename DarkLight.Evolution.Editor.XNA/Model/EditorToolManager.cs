﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using DarkLight.Editor.Tools;
using Flai;

namespace DarkLight.Editor.Model
{
    public class EditorToolManager : IEnumerable<EditorTool>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly ObservableCollection<EditorTool> _tools = new ObservableCollection<EditorTool>();
        private readonly ReadOnlyObservableCollection<EditorTool> _readOnlyTools;

        private EditorTool _activeTool;

        public ReadOnlyObservableCollection<EditorTool> Tools
        {
            get { return _readOnlyTools; }
        }

        public EditorTool ActiveTool
        {
            get { return _activeTool; }
            set
            {
                if (_activeTool != value)
                {
                    if (!_tools.Contains(value))
                    {
                        throw new ArgumentException("Invalid value");
                    }

                    _activeTool.Reset();
                    _activeTool = value;
                    this.PropertyChanged.InvokeIfNotNull(this, "ActiveTool");
                }
            }
        }

        public EditorToolManager()
        {
            _readOnlyTools = new ReadOnlyObservableCollection<EditorTool>(_tools);
        }

        public void CreateTools(EditorWorld world, EditorUser user)
        {
            _tools.Add(_activeTool = new TileBrushTool(world, user));
            _tools.Add(new FillTool(world, user));
            _tools.Add(new ResizeLevelTool(world, user));
            _tools.Add(new SelectionTool(world, user));
            _tools.Add(new GameObjectTool(world, user));
            _tools.Add(new MovingPlatformTool(world, user));
            _tools.Add(new PortalTool(world, user));
        }

        public void SetActiveTool<T>()
            where T : EditorTool
        {
            if (this.ActiveTool is T)
            {
                return;
            }

            this.ActiveTool = _tools.First(tool => tool is T);
        }

        #region Impementation of IEnumerable<T>

        public IEnumerator<EditorTool> GetEnumerator()
        {
            return _tools.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _tools.GetEnumerator();
        }

        #endregion
    }
}
