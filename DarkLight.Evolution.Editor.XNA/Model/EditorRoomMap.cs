﻿using System;
using Flai;
using Flai.General;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.Model
{
    public class EditorRoomMap
    {
        #region Fields and Properties

        public const int MinimumSize = 8;
        private static readonly Vector2i MinSize = new Vector2i(MinimumSize, MinimumSize);

        public event GenericEvent<Direction2D, int> Resized;

        private readonly ResizableTileMap<TileType> _darkTileMap;
        private readonly ResizableTileMap<TileType> _lightTileMap;

        public IEditableTileMap<TileType> DarkTiles
        {
            get { return _darkTileMap; }
        }

        public IEditableTileMap<TileType> LightTiles
        {
            get { return _lightTileMap; }
        }

        public int Width
        {
            // Dark and Light tile maps have same width
            get { return _darkTileMap.Width; }
        }

        public int Height
        {
            // Dark and Light tile maps have same height
            get { return _lightTileMap.Height; }
        }

        #endregion

        public EditorRoomMap(ResizableTileMap<TileType> darkTileMap, ResizableTileMap<TileType> lightTileMap)
        {
            if (darkTileMap.Width == 0 || darkTileMap.Height == 0 ||
                darkTileMap.Width != lightTileMap.Width || darkTileMap.Height != lightTileMap.Height)
            {
                throw new ArgumentException("Invalid tile maps");
            }

            _darkTileMap = darkTileMap;
            _lightTileMap = lightTileMap;
        }

        #region Resize

        // returns true if the map was actually resized
        public bool Resize(Direction2D resizeDirection, int resizeAmount)
        {
            if (resizeAmount == 0)
            {
                return false;
            }

            Vector2i oldSize = new Vector2i(_darkTileMap.Width, _darkTileMap.Height);
            Vector2i newSize = Vector2i.Max(EditorRoomMap.MinSize, oldSize + resizeDirection.ToUnitVector() * resizeAmount);
            Vector2i resizeAmountVector = resizeDirection.ToUnitVector() * (newSize - oldSize);
            if (oldSize == newSize)
            {
                return false;
            }

            if (resizeDirection == Direction2D.Right || resizeDirection == Direction2D.Down)
            {
                this.Resize(newSize.X, newSize.Y);
            }
            else if (resizeDirection == Direction2D.Left || resizeDirection == Direction2D.Up)
            {
                if (resizeAmount > 0)
                {
                    this.MoveTiles(new Rectangle(resizeAmountVector.X, resizeAmountVector.Y, oldSize.X - resizeAmountVector.X, oldSize.Y - resizeAmountVector.Y), -resizeAmountVector);
                    this.Resize(newSize.X, newSize.Y);
                }
                else
                {
                    this.Resize(newSize.X, newSize.Y);
                    this.MoveTiles(new Rectangle(0, 0, newSize.X + resizeAmountVector.X, newSize.Y + resizeAmountVector.Y), -resizeAmountVector);
                }
            }

            this.Resized.InvokeIfNotNull(resizeDirection, (resizeDirection.ToAlignment() == Alignment.Horizontal) ? resizeAmountVector.X : resizeAmountVector.Y);
            return true;
        }

        private void Resize(int newWidth, int newHeight)
        {
            if (this.Width == newWidth && this.Height == newHeight)
            {
                return;
            }

            _darkTileMap.Resize(newWidth, newHeight);
            _lightTileMap.Resize(newWidth, newHeight);
        }

        #endregion

        public void MoveTiles(Rectangle tileArea, Vector2i offset, GameDimension gameDimension = GameDimension.Both)
        {
            if (gameDimension.IsFlagSet(GameDimension.Dark))
            {
                _darkTileMap.MoveTiles(tileArea, offset);
            }

            if (gameDimension.IsFlagSet(GameDimension.Light))
            {
                _lightTileMap.MoveTiles(tileArea, offset);
            }
        }
    }
}
