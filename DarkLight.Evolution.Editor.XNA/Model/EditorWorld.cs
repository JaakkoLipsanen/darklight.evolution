﻿using System.Collections.Generic;
using DarkLight.Editor.DataStructures;
using Flai.General;

namespace DarkLight.Editor.Model
{
    public class EditorWorld
    {
        private readonly EditorRoomContainer _roomContainer;
        private GameDimension _activeGameDimension = GameDimension.Light;

        public EditorRoomContainer Rooms
        {
            get { return _roomContainer; }
        }

        // Okay this kinda breaks the "encapsulation" of EditorRoomContainer but whatever
        // Internal so that WPF cannot accidentaly bind to this
        internal EditorRoom SelectedRoom
        {
            get { return _roomContainer.SelectedRoom; }
            set { _roomContainer.SelectedRoom = value; }
        }

        public GameDimension ActiveGameDimension
        {
            get { return _activeGameDimension; }
            set { _activeGameDimension = value; }
        }

        public EditorWorld(ICollection<EditorRoom> rooms, PropertyCollection propertyCollection)
        {
            _roomContainer = new EditorRoomContainer(rooms);
        }
    }
}
