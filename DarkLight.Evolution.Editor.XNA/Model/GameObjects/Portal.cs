﻿using System;
using System.ComponentModel;
using System.IO;
using DarkLight.Editor.DataStructures;
using DarkLight.Editor.View;
using Flai;
using Flai.Graphics;
using Flai.IO;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.Model.GameObjects
{
    public enum PortalIndex
    {
        Start,
        End,
    }

    public class Portal : GameObject, IBinarySerializable
    {
        private const int VisualIndexRectangleSize = Tile.Size / 2;
        private Direction2D _direction = Direction2D.Down;
        private int _length = 4;

        private Portal _destination;
        private PortalDestinationRef _unresolvedDestination; // meh

        [Browsable(false)]
        public new GameDimension GameDimension
        {
            get { return base.GameDimension; }
            set { base.GameDimension = value; }
        }

        [Browsable(false)]
        public Vector2i StartIndex
        {
            get { return this.GridIndex; }
            set { this.GridIndex = value; }
        }

        [Browsable(false)]
        public Vector2i EndIndex
        {
            get { return this.StartIndex + _direction.ToUnitVector() * _length; }
        }

        public Direction2D Direction
        {
            get { return _direction; }
            set
            {
                if (_direction != value)
                {
                    _direction = value;
                    this.OnPropertyChanged("Direction");
                }
            }
        }

        public int Length
        {
            get { return _length; }
            set
            {
                if (value >= 1 && value != _length)
                {
                    _length = value;
                    this.OnPropertyChanged("Count");
                }
            }
        }

        [Browsable(false)]
        public Segment2Di TileSegment
        {
            get { return new Segment2Di(this.StartIndex, this.EndIndex); }
        }

        // always to right
        [Browsable(false)]
        public Direction2D NormalDirection
        {
            // Up -> Right, Right -> Down, Down -> Left, Left -> Up. it works :) !
            get { return _direction.RotateRight(); }
        }

        public Portal Destination
        {
            get { return _destination; }
            set
            {
                if (_destination != value)
                {
                    _destination = value;
                    this.OnPropertyChanged("Destination");
                }
            }
        }

        public override RectangleF VisualArea
        {
            get
            {
                const int Offset = Tile.Size;
                Vector2i min = Vector2i.Min(this.StartIndex, this.EndIndex) * Tile.Size;
                Vector2i max = Vector2i.Max(this.StartIndex, this.EndIndex) * Tile.Size;

                if (min.X == max.X)
                {
                    return new RectangleF(min.X - Offset, min.Y - Portal.VisualIndexRectangleSize / 2f, Offset * 2f, max.Y - min.Y + Portal.VisualIndexRectangleSize);
                }
                else
                {
                    return new RectangleF(min.X - Portal.VisualIndexRectangleSize / 2f, min.Y - Offset, max.X - min.X + Portal.VisualIndexRectangleSize, Offset * 2f);
                }
            }
        }

        // TODO: Destination portal ID (& room id? maybe create a "RoomReference" class or something? and then PortalReference.. dunno)

        public Portal()
        {
        }

        public Portal(Vector2i start, Vector2i end)
        {
            if (start == end || (start.X != end.X && start.Y != end.Y))
            {
                throw new ArgumentException("Invalid start/end pair, not vertical or horizontal");
            }

            this.StartIndex = start;
            if (start.X == end.X)
            {
                _direction = (end.Y - start.Y) > 0 ? Direction2D.Down : Direction2D.Up;
                _length = FlaiMath.Abs(end.Y - start.Y);
            }
            else // if(start.Y == end.Y)
            {
                _direction = (end.X - start.X) > 0 ? Direction2D.Right : Direction2D.Left;
                _length = FlaiMath.Abs(end.X - start.X);
            }
        }

        public void FlipNormal()
        {
            this.StartIndex += _direction.ToUnitVector() * _length;
            _direction = _direction.Opposite();
        }

        public override GameObject DeepClone()
        {
            return new Portal(this.StartIndex, this.EndIndex);
        }

        protected override void DrawInner(GraphicsContext graphicsContext, Color color, float alpha)
        {
            // plääh
            graphicsContext.PrimitiveRenderer.DrawLine(graphicsContext, this.StartIndex * Tile.Size, this.EndIndex * Tile.Size, EditorRenderer.PortalColor * 0.75f);
            graphicsContext.PrimitiveRenderer.DrawLine(graphicsContext, (this.StartIndex + this.EndIndex) * Tile.Size / 2f, EditorRenderer.PortalColor * 0.75f, this.NormalDirection.ToRadians(), Tile.Size);

            // Start/End rectangles
            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, this.StartIndex * Tile.Size, Tile.Size / 2f, Color.Red);
            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, this.EndIndex * Tile.Size, Tile.Size / 2f, Color.Green);

            // "No Destination" cross
            if (_destination == null)
            {
                graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, (this.StartIndex + this.EndIndex) * Tile.Size / 2f, Tile.Size * 1.25f, Color.Purple * 0.85f);
            }
        }

        #region Write/Read

        protected override void WriteToStream(BinaryWriter writer)
        { 
            writer.Write((int)_direction);
            writer.Write(_length);

            // TODO: Destination
            if (_destination == null)
            {
                writer.Write(false);
            }
            else
            {
                writer.Write(true);
                writer.Write(_destination.OwnerRoom.Id);
                writer.Write(_destination.Id);
            }
        }

        protected override void ReadFromStream(BinaryReader reader)
        {
            this.Direction = (Direction2D)reader.ReadInt32();
            this.Length = reader.ReadInt32();

            if (reader.ReadBoolean())
            {
                string destinationRoomId = reader.ReadString();
                string destinationId = reader.ReadString();
                _unresolvedDestination = new PortalDestinationRef(destinationRoomId, destinationId);
            }
        }

        // Super blaah!!
        public void ResolveDestination(EditorRoomContainer roomContainer)
        {
            if (_unresolvedDestination != null)
            {
                EditorRoom destinationRoom = roomContainer.GetById(_unresolvedDestination.RoomId);
                Portal destination = destinationRoom.GameObjects.GetById<Portal>(_unresolvedDestination.PortalId);
                if (destination == null)
                {
                    throw new ArgumentException("");
                }

                _unresolvedDestination = null;
                this.Destination = destination;
            }
        }

        #endregion

        private class PortalDestinationRef
        {
            public string RoomId { get; private set; }
            public string PortalId { get; private set; }

            public PortalDestinationRef(string roomId, string portalId)
            {
                this.RoomId = roomId;
                this.PortalId = portalId;
            }
        }
    }
}
