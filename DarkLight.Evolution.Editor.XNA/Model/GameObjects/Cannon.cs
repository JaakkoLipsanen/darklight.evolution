﻿using System.ComponentModel;
using System.IO;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.Model.GameObjects
{
    public class Cannon : GameObject
    {
        #region Fields

        private const float DefaultBulletSpeed = Tile.Size * 8;
        private const float DefaultTimeBetweenShots = 1f;

        public static readonly Vector2i BodySize = new Vector2i(1, 2) * Tile.Size;
        public static readonly Vector2i PipeSize = new Vector2i(1, 1) * Tile.Size;

        private float _bulletSpeed = Cannon.DefaultBulletSpeed;
        private float _timeBetweenShots = Cannon.DefaultTimeBetweenShots;
        private Direction2D _direction = Direction2D.Left;

        #endregion

        #region Properties

        [DisplayName("Bullet Speed")]
        [Description("The speed of bullets that are fired by this cannon")]
        public float BulletSpeed
        {
            get { return _bulletSpeed; }
            set
            {
                if (_bulletSpeed != value && value >= 0)
                {
                    _bulletSpeed = value;
                    this.OnPropertyChanged("BulletSpeed");
                }
            }
        }

        [DisplayName("Time Between Shots")]
        [Description("Time in seconds how often the cannon shoots bullets")]
        public float TimeBetweenShots
        {
            get { return _timeBetweenShots; }
            set
            {
                if (_timeBetweenShots != value && value >= 0)
                {
                    _timeBetweenShots = value;
                    this.OnPropertyChanged("TimeBetweenShots");
                }
            }
        }

        [DisplayName("Direction")]
        [Description("Direction where the cannon is facing to")]
        public Direction2D Direction
        {
            get { return _direction; }
            set
            {
                if (_direction != value)
                {
                    _direction = value;
                    this.OnPropertyChanged("Direction");
                }
            }
        }

        public override RectangleF VisualArea
        {
            get
            {
                Vector2i size;
                if (_direction == Direction2D.Left || _direction == Direction2D.Right)
                {
                    size = new Vector2i(Cannon.PipeSize.X + Cannon.BodySize.X, FlaiMath.Max(Cannon.PipeSize.Y, Cannon.BodySize.Y));
                }
                else
                {
                    size = new Vector2i(FlaiMath.Max(Cannon.PipeSize.Y, Cannon.BodySize.Y), Cannon.PipeSize.X + Cannon.BodySize.X);
                }

                Vector2 position = base.Position - size / 2f;
                return new RectangleF(position.X, position.Y, size.X, size.Y);
            }
        }

        #endregion

        public override GameObject DeepClone()
        {
            return new Cannon
            {
                GridIndex = _gridIndex,
                Direction = _direction,
                BulletSpeed = _bulletSpeed,
                GameDimension = _gameDimension,
                TimeBetweenShots = _timeBetweenShots,
            };
        }

        #region Draw

        protected override void DrawInner(GraphicsContext graphicsContext, Color color, float alpha)
        {
            Vector2 size;
            if (_direction == Direction2D.Left || _direction == Direction2D.Right)
            {
                size = Cannon.BodySize;
            }
            else
            {
                size = new Vector2(Cannon.BodySize.Y, Cannon.BodySize.X);
            }

            // Body
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, this.Position + _direction.Inverse().ToUnitVector() * 0.5f * Tile.Size, color * alpha, 0f, size);

            // Pipe
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, this.Position + _direction.ToUnitVector() * 0.5f * Tile.Size, color * alpha, 0f, Tile.Size);
        }

        #endregion

        #region Write/Read

        protected override void WriteToStream(BinaryWriter writer)
        {
            writer.Write((int)_direction);
            writer.Write(_bulletSpeed);
            writer.Write(_timeBetweenShots);
        }

        protected override void ReadFromStream(BinaryReader reader)
        {
            this.Direction = (Direction2D)reader.ReadInt32();
            this.BulletSpeed = reader.ReadSingle();
            this.TimeBetweenShots = reader.ReadSingle();
        }

        #endregion
    }
}
