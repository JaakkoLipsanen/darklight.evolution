﻿using System;

namespace DarkLight.Editor.Model.GameObjects
{
    public static class GameObjectHelper
    {
        public static T CreateDefaultGameObject<T>()
            where T : GameObject, new()
        {
            return new T();
        }

        public static GameObject CreateDefaultGameObject(Type type)
        {
            if (type != null && typeof(GameObject).IsAssignableFrom(type))
            {
                return (GameObject)Activator.CreateInstance(type);
            }

            return null;
        }

        public static GameObject CreateDeepClone(GameObject gameObject)
        {
            return gameObject.DeepClone();
        }

        public static T CreateDeepClone<T>(T gameObject)
            where T : GameObject
        {
            return (T)gameObject.DeepClone();
        }
    }
}
