﻿using System;
using System.ComponentModel;
using System.IO;
using DarkLight.Editor.View;
using Flai;
using Flai.Editor.Misc;
using Flai.Graphics;
using Flai.IO;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.Model.GameObjects
{
    public interface IGameObject
    {
        void RegisterToRoom(EditorRoom room, string initialId);
        void Draw(GraphicsContext graphicsContext, GameDimension gameDimension, float alpha);
    }

    public abstract class GameObject : IBinarySerializable, INotifyPropertyChanged, IGameObject
    {
        #region Fields

        public event PropertyChangedEventHandler PropertyChanged;
        protected Vector2i _gridIndex;

        private EditorRoom _ownerRoom;
        private string _id = null;
        protected GameDimension _gameDimension = GameDimension.Both;

        #endregion

        #region Properties

        [DisplayName("Id")]
        [Description("Id of the game object")]
        [Category("General")]
        public string Id
        {
            get { return _id; }
            set
            {
                if (_id != value && !string.IsNullOrWhiteSpace(value) && !_ownerRoom.GameObjects.IsIdUsed(value))
                {
                    string oldValue = _id;
                    _id = value;
                    this.OnPropertyChanged("Id", oldValue, _id);
                }
            }
        }

        [DisplayName("Game Dimension")]
        [Description("Game Dimension of the game object")]
        [Category("General")]
        public GameDimension GameDimension
        {
            get { return _gameDimension; }
            set
            {
                if (_gameDimension != value)
                {
                    _gameDimension = value;
                    this.OnPropertyChanged("GameDimension");
                }
            }
        }

        [Browsable(false)]
        public Vector2i GridIndex
        {
            get { return _gridIndex; }
            set
            {
                if (_gridIndex != value)
                {
                    _gridIndex = value;
                    this.OnPropertyChanged("GridIndex");
                    this.OnPositionChanged();
                }
            }
        }

        [Browsable(false)]
        public Vector2i Position
        {
            get { return _gridIndex * Tile.Size; }
        }

        [Browsable(false)]
        public abstract RectangleF VisualArea { get; }

        [Browsable(false)]
        public virtual RectangleF InputArea
        {
            get { return this.VisualArea; }
        }

        [Browsable(false)]
        public virtual Type Type
        {
            get { return base.GetType(); }
        }

        [Browsable(false)]
        public EditorRoom OwnerRoom
        {
            get { return _ownerRoom; }
        }

        #endregion

        public abstract GameObject DeepClone();

        #region Draw

        // Okay, I'm pretty sure that this is the easiest way to do this
        void IGameObject.Draw(GraphicsContext graphicsContext, GameDimension gameDimension, float alpha)
        {
            if (!_gameDimension.IsAnyFlagSet(gameDimension))
            {
                throw new ArgumentException("Invalid gameDimension");
            }

            // draw the game object
            this.DrawInner(graphicsContext, EditorRenderer.GetGameDimensionColor(_gameDimension & gameDimension), alpha);

            // draw "not-selected" area (even for the selected game object)
            ICamera2D camera = graphicsContext.Services.GetService<ICameraManager2D>().ActiveCamera;
            graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext, this.VisualArea, Color.Blue, 1f / camera.Zoom);
        }

        protected virtual void DrawInner(GraphicsContext graphicsContext, Color color, float alpha)
        {
            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, this.VisualArea, EditorRenderer.UnknownColor * alpha);
        }

        #endregion

        #region OnPropertyChanged

        protected void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged.InvokeIfNotNull(this, propertyName);
        }

        protected void OnPropertyChanged<T>(string propertyName, T oldValue, T newValue)
        {
            this.PropertyChanged.InvokeIfNotNull(this, new PropertyChangedEventArgs<T>(propertyName, oldValue, newValue));
        }

        #endregion

        #region Misc

        void IGameObject.RegisterToRoom(EditorRoom ownerRoom, string initialId)
        {
            if (_ownerRoom != null)
            {
                // when undoing for example "delete" action, the game object will be initialized again.. so let's not throw an exception
                // though then the id will be generated for "nothing".. maybe IsInitialized property? think about it

                if (_ownerRoom != ownerRoom)
                {
                    throw new ArgumentException("Invalid room!");
                }

                // Okay I think this is correct
                if (string.IsNullOrWhiteSpace(this.Id) || ownerRoom.GameObjects.IsIdUsed(this.Id))
                {
                    this.Id = initialId;
                }

                return;
            }

            _ownerRoom = ownerRoom;

            // if loaded from file then don't overwrite it
            if (string.IsNullOrWhiteSpace(_id))
            {
                this.Id = initialId;
            }
        }

        protected virtual void OnPositionChanged()
        {
        }

        #endregion

        #region Write/Read

        void IBinarySerializable.Write(BinaryWriter writer)
        {
            writer.WriteString(_id);
            writer.Write((int)_gameDimension);
            writer.Write(_gridIndex);

            this.WriteToStream(writer);
        }

        void IBinarySerializable.Read(BinaryReader reader)
        {
            _id = reader.ReadString();
            this.GameDimension = (GameDimension)reader.ReadInt32();
            this.GridIndex = reader.ReadGeneric<Vector2i>();

            this.ReadFromStream(reader);
        }

        protected virtual void WriteToStream(BinaryWriter writer) { }
        protected virtual void ReadFromStream(BinaryReader reader) { }

        #endregion
    }
}
