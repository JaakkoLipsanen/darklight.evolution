﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using Flai;
using Flai.Editor.Misc;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace DarkLight.Editor.Model.GameObjects
{
    public class MovingPlatform : GameObject
    {
        #region Fields

        private const float DefaultSpeed = Tile.Size * 4;
        private const int DefaultWidth = 8;
        private const int DefaultHeight = 2;

        private readonly ObservableVector2i _sizeWrapper;
        private int _width = MovingPlatform.DefaultWidth;
        private int _height = MovingPlatform.DefaultHeight;

        private bool _isEnabled = true;
        private bool _isCircular = false;
        private bool _isRepeating = true;

        private float _speed = MovingPlatform.DefaultSpeed;
        private readonly List<Vector2i> _loopPoints = new List<Vector2i>() { Vector2i.Zero };
        private readonly ReadOnlyCollection<Vector2i> _readOnlyLoopPoints;

        #endregion

        #region Properties

        [Browsable(false)]
        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        [Browsable(false)]
        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        [ExpandableObject]
        [DisplayName("Size")]
        [Description("Size of the moving platform")]
        public object Size
        {
            get { return _sizeWrapper; }
        }

        [DisplayName("Speed")]
        [Description("Speed of the moving platform")]
        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        [DisplayName("Is Enabled")]
        [Description("Is the moving platform enabled when the level starts")]
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }

        [DisplayName("Is Circular")]
        [Description("Is the moving platform circular")]
        public bool IsCircular
        {
            get { return _isCircular; }
            set { _isCircular = value; }
        }

        [DisplayName("Is Repeating")]
        [Description("Is the moving platform repeating")]
        public bool IsRepeating
        {
            get { return _isRepeating; }
            set { _isRepeating = value; }
        }

        // Draw MovingPlatforms everywhere because the path can go everywhere in the map
        public override RectangleF VisualArea
        {
            get
            {
                Vector2 size = new Vector2(_width, _height) * Tile.Size;
                Vector2 topLeftPosition = base.Position - size / 2f;
                return new RectangleF(topLeftPosition.X, topLeftPosition.Y, size.X, size.Y);
            }
        }

        [Browsable(false)]
        public ReadOnlyCollection<Vector2i> LoopPoints
        {
            get { return _readOnlyLoopPoints; }
        }

        #endregion

        public MovingPlatform()
        {
            _readOnlyLoopPoints = new ReadOnlyCollection<Vector2i>(_loopPoints);
            _sizeWrapper = new ObservableVector2i(new Vector2i(_width, _height));
            _sizeWrapper.ValueChanged += () =>
            {
                _sizeWrapper.X = Math.Max(1, _sizeWrapper.X);
                _width = _sizeWrapper.X;

                _sizeWrapper.Y = Math.Max(1, _sizeWrapper.Y);
                _height = _sizeWrapper.Y;
            };

        }

        public void AddLoopPointTo(int index, Vector2i position)
        {
            _loopPoints.Insert(index, position);
        }

        public void RemoveLoopPointAt(int index)
        {
            _loopPoints.RemoveAt(index);
        }

        public void MoveLoopPointAt(int index, Vector2i newPosition)
        {
            _loopPoints[index] = newPosition;
            if (index == 0)
            {
                this.GridIndex = newPosition;
            }
        }

        public override GameObject DeepClone()
        {
            return new MovingPlatform
            {
                GridIndex = _gridIndex,
                GameDimension = _gameDimension,
                IsEnabled = _isEnabled,
                Width = _width,
                Height = _height,
                IsCircular = _isCircular,
                IsRepeating = _isRepeating,
                Speed = _speed,
            };
        }

        protected override void DrawInner(GraphicsContext graphicsContext, Color color, float alpha)
        {
            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, this.VisualArea, color * alpha);
        }

        protected override void OnPositionChanged()
        {
            base.OnPositionChanged();
            _loopPoints[0] = _gridIndex;
        }

        protected override void WriteToStream(BinaryWriter writer)
        {
            writer.Write(_width);
            writer.Write(_height);
            writer.Write(_isEnabled);
            writer.Write(_isCircular);
            writer.Write(_isRepeating);
            writer.Write(_speed);
            writer.WriteArray(_loopPoints.ToArray());
        }

        protected override void ReadFromStream(BinaryReader reader)
        {
            this.Width = reader.ReadInt32();
            this.Height = reader.ReadInt32();
            this.IsEnabled = reader.ReadBoolean();
            this.IsCircular = reader.ReadBoolean();
            this.IsRepeating = reader.ReadBoolean();
            this.Speed = reader.ReadSingle();

            _loopPoints.Clear();
            _loopPoints.AddAll(reader.ReadArray<Vector2i>());
        }
    }
}
