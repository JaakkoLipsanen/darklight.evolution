﻿using System;

namespace DarkLight.Editor.Model
{
    [Flags]
    public enum GameDimension
    {
        Dark = 2,
        Light = 4,
        Both = Dark | Light,
    }

    public static class GameDimensionHelper
    {
        public static bool IsFlagSet(this GameDimension gameDimension, GameDimension other)
        {
            return (gameDimension & other) == other;
        }

        public static bool IsAnyFlagSet(this GameDimension gameDimension, GameDimension other)
        {
            return (gameDimension & other) != 0;
        }

        public static GameDimension Opposite(this GameDimension gameDimension)
        {
            switch (gameDimension)
            {
                case GameDimension.Dark:
                    return GameDimension.Light;

                case GameDimension.Light:
                    return GameDimension.Dark;

                case GameDimension.Both:
                    return GameDimension.Both;

                default:
                    throw new ArgumentException("Invalid argument");
            }
        }
    }
}
