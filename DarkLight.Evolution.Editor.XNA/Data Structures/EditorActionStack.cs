﻿using System.ComponentModel;
using DarkLight.Editor.Model;
using Flai;
using Flai.Editor.DataStructures;
using Flai.Editor.Misc;

namespace DarkLight.Editor.DataStructures
{
    public class EditorActionStack : IObservableUndoRedoStack<EditorAction>
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly ObservableUndoRedoStack<EditorAction> _actionStack = new ObservableUndoRedoStack<EditorAction>();
        private readonly EditorWorld _world;
        private readonly EditorUser _user;

        public DelegateCommand UndoCommand
        {
            get { return _actionStack.UndoCommand; }
        }

        public DelegateCommand RedoCommand
        {
            get { return _actionStack.RedoCommand; }
        }

        public bool CanUndo
        {
            get { return _actionStack.CanUndo; }
        }

        public bool CanRedo
        {
            get { return _actionStack.CanRedo; }
        }

        public EditorActionStack(EditorWorld world, EditorUser user)
        {
            _world = world;
            _user = user;

            _actionStack.PropertyChanged += (sender, args) => this.PropertyChanged.InvokeIfNotNull(sender, args);
        }

        public void Push(EditorAction value)
        {
            value.Initialize(_world, _user);
            _actionStack.Push(value);
        }

        public void Undo()
        {
            _actionStack.Undo();
        }

        public void Redo()
        {
            _actionStack.Redo();
        }
    }

}
