﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using DarkLight.Editor.IO;
using DarkLight.Editor.Model;
using Flai;

namespace DarkLight.Editor.DataStructures
{
    public interface IRoomManager
    {
        bool IsIdUsed(string id);
    }

    // TODO: Create "MoveUp"/"MoveDown" buttons to UI and implement methods here. So that the rooms can be ordered properly. also maybe same for game objects? or not
    public class EditorRoomContainer : IRoomManager, INotifyPropertyChanged, IEnumerable<EditorRoom>
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event GenericEvent<EditorRoom, EditorRoom> SelectedRoomChanged;
        public event GenericEvent<EditorRoom> RoomAdded;
        public event GenericEvent<EditorRoom> RoomRemoved;

        private readonly ObservableCollection<EditorRoom> _rooms;
        private readonly ReadOnlyObservableCollection<EditorRoom> _readOnlyRooms;

        private EditorRoom _selectedRoom;

        public ReadOnlyObservableCollection<EditorRoom> InnerCollection
        {
            get { return _readOnlyRooms; }
        }

        public EditorRoom SelectedRoom
        {
            get { return _selectedRoom; }
            set { this.ChangeSelectedRoom(value); }
        }

        public int Count
        {
            get { return _rooms.Count; }
        }

        public EditorRoomContainer(ICollection<EditorRoom> rooms)
        {
            if (rooms.Count == 0)
            {
                throw new ArgumentException("There must be at least one room");
            }

            _rooms = new ObservableCollection<EditorRoom>();
            _readOnlyRooms = new ReadOnlyObservableCollection<EditorRoom>(_rooms);

            this.AddAll(rooms);
            this.SelectedRoom = _rooms[0];
        }

        public EditorRoom GetById(string id)
        {
            return _rooms.FirstOrDefault(room => room.Id == id);
        }

        public void Add(EditorRoom room, bool setSelected = false)
        {
            if (room == null || _rooms.Contains(room))
            {
                throw new ArgumentException("Room is already added to the world!");
            }

            room.Initialize(this, this.GenerateId());
            _rooms.Add(room);
            this.RoomAdded.InvokeIfNotNull(room);

            if (setSelected)
            {
                this.ChangeSelectedRoom(room);
            }
        }

        public void AddAll(IEnumerable<EditorRoom> rooms)
        {
            foreach (EditorRoom room in rooms)
            {
                this.Add(room);
            }
        }

        public void Remove(EditorRoom room)
        {
            if (_rooms.Count == 1)
            {
                throw new ArgumentException("Can't remove the last room in the world!");
            }

            if (_selectedRoom == room)
            {
                this.SelectedRoom = _rooms[0] != room ?
                    _rooms[_rooms.IndexOf(room) - 1] :
                    _rooms[1];
            }

            if (_rooms.Remove(room))
            {
                this.RoomRemoved.InvokeIfNotNull(room);
            }
        }

        private void ChangeSelectedRoom(EditorRoom room)
        {
            if (_selectedRoom == room || room == null)
            {
                return;
            }

            if (room == null || !_rooms.Contains(room))
            {
                throw new ArgumentException("The room isn't added to EditorWorld.Rooms");
            }

            EditorRoom old = _selectedRoom;
            _selectedRoom = room;

            this.SelectedRoomChanged.InvokeIfNotNull(old, _selectedRoom);
            this.PropertyChanged.InvokeIfNotNull(this, "SelectedRoom");
        }

        public void AddEmptyRoom()
        {
            this.Add(EditorRoomSerializer.CreateEmpty());
        }

        private string GenerateId()
        {
            for (int i = 1; ; i++)
            {
                string candidateId = "Room" + i;
                if (this.GetById(candidateId) == null)
                {
                    return candidateId;
                }
            }
        }

        #region Implementation of IEnumerable<T>

        public IEnumerator<EditorRoom> GetEnumerator()
        {
            return _rooms.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _rooms.GetEnumerator();
        }

        #endregion

        #region IRoomManager Members

        bool IRoomManager.IsIdUsed(string id)
        {
            return this.GetById(id) != null;
        }

        #endregion
    }
}
