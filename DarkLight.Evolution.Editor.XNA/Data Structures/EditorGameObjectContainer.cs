﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using DarkLight.Editor.Tools;
using Flai;
using Microsoft.Xna.Framework;

namespace DarkLight.Editor.DataStructures
{
    // And if possible, remove the "IsSelected" stuff from GameObject.cs
    public class EditorGameObjectContainer : IEnumerable<GameObject>
    {
        #region Fields and Properties

        public event GenericEvent<GameObject> GameObjectAdded;
        public event GenericEvent<GameObject> GameObjectAdding; // meh!! but needed for EditorRoom to call RegisterToRoom...
        public event GenericEvent<GameObject> GameObjectRemoved;

        private readonly ObservableCollection<GameObject> _gameObjects;
        // private readonly Dictionary<string, GameObject> _gameObjectsById = new Dictionary<string, GameObject>(); // don't use if not necessary! 

        private readonly ReadOnlyObservableCollection<GameObject> _readOnlyGameObjects;

        public ReadOnlyObservableCollection<GameObject> InnerCollection
        {
            get { return _readOnlyGameObjects; }
        }

        public int Count
        {
            get { return _gameObjects.Count; }
        }

        #endregion

        public EditorGameObjectContainer()
        {
            _gameObjects = new ObservableCollection<GameObject>();
            _readOnlyGameObjects = new ReadOnlyObservableCollection<GameObject>(_gameObjects);
        }

        public GameObject this[int index]
        {
            get { return _gameObjects[index]; }
        }

        #region Move

        public void MoveAll(Vector2i offsetInTiles)
        {
            // Move game objects
            foreach (GameObject gameObject in _gameObjects)
            {
                gameObject.GridIndex += offsetInTiles;
            }

            foreach (MovingPlatform movingPlatform in this.GetAllOfType<MovingPlatform>())
            {
                // Start from 1 because 0 would be movingPlaform.GridIndex which is already being shifted
                for (int i = 1; i < movingPlatform.LoopPoints.Count; i++)
                {
                    movingPlatform.MoveLoopPointAt(i, movingPlatform.LoopPoints[i] + offsetInTiles);
                }
            }
        }

        #endregion

        #region Get

        public IEnumerable<T> GetAllOfType<T>()
            where T : GameObject
        {
            return _gameObjects.Where(gameObject => gameObject is T).Cast<T>();
        }

        public GameObject GetById(string id)
        {
            return this.GetById<GameObject>(id);
        }

        public T GetById<T>(string id)
            where T : GameObject
        {
            return _gameObjects.FirstOrDefault(go => go.Id == id) as T;
        }

        #region Get At (Single)

        public GameObject GetAt(Vector2 position)
        {
            return this.GetAt(position, GameDimension.Both);
        }

        public T GetAt<T>(Vector2 position)
            where T : GameObject
        {
            return this.GetAt<T>(new RectangleF(position.X - 1, position.Y - 1, 2, 2), GameDimension.Both);
        }

        public GameObject GetAt(Vector2 position, GameDimension gameDimension)
        {
            return this.GetAt(new RectangleF(position.X - 1, position.Y - 1, 2, 2), gameDimension);
        }

        public T GetAt<T>(Vector2 position, GameDimension gameDimension)
            where T : GameObject
        {
            return this.GetAt<T>(new RectangleF(position.X - 1, position.Y - 1, 2, 2), gameDimension);
        }

        public GameObject GetAt(RectangleF area, GameDimension gameDimension)
        {
            return this.GetAt<GameObject>(area, gameDimension);
        }

        public T GetAt<T>(RectangleF area, GameDimension gameDimension)
            where T : GameObject
        {
            // Find all game objects within the area and return the one that is closest to the area center
            return this.GetAllAt<T>(area, gameDimension).
                OrderBy(go => Vector2.Distance(area.Center, go.InputArea.Center)).
                FirstOrDefault() as T;
        }

        #endregion

        #region Get All At (Multiple)

        public IEnumerable<GameObject> GetAllAt(Vector2 position)
        {
            return this.GetAllAt(position, GameDimension.Both);
        }

        public IEnumerable<GameObject> GetAllAt(Vector2 position, GameDimension gameDimension)
        {
            return this.GetAllAt(new RectangleF(position.X - 1, position.Y - 1, 2, 2), gameDimension);
        }

        public IEnumerable<GameObject> GetAllAt(RectangleF area)
        {
            return this.GetAllAt<GameObject>(area, GameDimension.Both);
        }

        public IEnumerable<GameObject> GetAllAt(RectangleF area, GameDimension gameDimension)
        {
            return this.GetAllAt<GameObject>(area, gameDimension);
        }

        public IEnumerable<T> GetAllAt<T>(RectangleF area, GameDimension gameDimension)
            where T : GameObject
        {
            return _gameObjects.
                Where(go => go is T && go.InputArea.Intersects(area) && gameDimension.IsAnyFlagSet(go.GameDimension)). // IsAnyFlagSet correct?
                Cast<T>();
        }

        #endregion

        #endregion

        #region Add

        public void Add(GameObject gameObject)
        {
            // Contains is a bit slow since it's not a hashset but whatever
            if (gameObject == null || _gameObjects.Contains(gameObject))
            {
                throw new ArgumentException("Invalid game object");
            }

            this.GameObjectAdding.InvokeIfNotNull(gameObject);
            _gameObjects.Add(gameObject);
            this.GameObjectAdded.InvokeIfNotNull(gameObject);
        }

        public void AddAll(IEnumerable<GameObject> gameObjects)
        {
            foreach (GameObject gameObject in gameObjects)
            {
                this.Add(gameObject);
            }
        }

        #endregion

        #region Remove

        public bool Remove(GameObject gameObject)
        {
            int index = _gameObjects.IndexOf(gameObject);
            if (index >= 0)
            {
                this.RemoveAt(index);
                return true;
            }

            return false;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= _gameObjects.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            GameObject gameObject = _gameObjects[index];
            _gameObjects.RemoveAt(index);

            this.GameObjectRemoved.InvokeIfNotNull(gameObject);
        }

        // not tested, should work though
        public void RemoveAll(Predicate<GameObject> predicate)
        {
            for (int i = _gameObjects.Count - 1; i >= 0; i--)
            {
                if (predicate(_gameObjects[i]))
                {
                    this.RemoveAt(i);
                }
            }
        }

        #endregion

        #region Misc

        public string GenerateIdForType(Type type)
        {
            string typeName = type.Name;
            for (int i = 1; ; i++)
            {
                string candidateId = typeName + i;
                if (!this.IsIdUsed(candidateId))
                {
                    return candidateId;
                }
            }
        }

        #endregion

        #region Portal Specific

        public Portal GetPortalAt(Vector2i index)
        {
            return this.GetAllOfType<Portal>().FirstOrDefault(
                portal => Segment2D.MinimumDistance(portal.TileSegment, index) < 1);
        }

        public Portal GetPortalIndexAt(Vector2i index, out PortalIndex portalIndex)
        {
            foreach (Portal portal in this.GetAllOfType<Portal>())
            {
                if (portal.StartIndex == index)
                {
                    portalIndex = PortalIndex.Start;
                    return portal;
                }
                else if (portal.EndIndex == index)
                {
                    portalIndex = PortalIndex.End;
                    return portal;
                }
            }

            portalIndex = default(PortalIndex);
            return null;
        }

        #endregion

        #region Implementation of IGameObjectManager

        public bool IsIdUsed(string id)
        {
            return this.GetById(id) != null;
        }

        #endregion

        #region Implementation of IEnumerable<T>

        public IEnumerator<GameObject> GetEnumerator()
        {
            return _gameObjects.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _gameObjects.GetEnumerator();
        }

        #endregion
    }
}
