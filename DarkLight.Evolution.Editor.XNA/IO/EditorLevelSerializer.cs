﻿using System.IO;
using DarkLight.Editor.Model;

namespace DarkLight.Editor.IO
{
    internal class EditorLevelSerializer
    {
        internal static void Save(BinaryWriter writer, EditorLevel level)
        {
            EditorWorldSerializer.Save(writer, level.World);
            EditorUserSerializer.Save(writer, level.User);
        }

        internal static EditorLevel FromStream(BinaryReader reader)
        {
            EditorWorld world = EditorWorldSerializer.FromStream(reader);
            EditorUser user = EditorUserSerializer.FromStream(reader, world);
            return new EditorLevel(world, user);
        }

        // Does this belong here...?
        internal static EditorLevel CreateEmpty()
        {
            EditorWorld world = EditorWorldSerializer.CreateEmpty();
            EditorUser user = new EditorUser(world);
            return new EditorLevel(world, user);
        }
    }
}
