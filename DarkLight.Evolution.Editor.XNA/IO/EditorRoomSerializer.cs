﻿using System.IO;
using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using Flai;
using Flai.General;

namespace DarkLight.Editor.IO
{
    internal class EditorRoomSerializer
    {
        internal static void Save(BinaryWriter writer, EditorRoom editorRoom)
        {
            writer.Write(editorRoom.Id);
            EditorRoomMapSerializer.Save(writer, editorRoom.Map);

            writer.Write(editorRoom.GameObjects.Count);
            foreach (GameObject gameObject in editorRoom.GameObjects)
            {
                GameObjectSerializer.Save(writer, gameObject);
            }

            EditorRoomSerializer.SaveProperties(writer, editorRoom);
        }

        private static void SaveProperties(BinaryWriter writer, EditorRoom editorRoom)
        {
            //
        }

        internal static EditorRoom FromStream(BinaryReader reader)
        {
            string id = reader.ReadString();
            EditorRoomMap roomMap = EditorRoomMapSerializer.FromStream(reader);

            int gameObjectCount = reader.ReadInt32();
            GameObject[] gameObjects = new GameObject[gameObjectCount];
            for (int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i] = GameObjectSerializer.FromStream(reader);
            }

            return new EditorRoom(roomMap, gameObjects, EditorRoomSerializer.LoadProperties(reader)) { Id = id };
        }

        private static PropertyCollection LoadProperties(BinaryReader reader)
        {
            return new PropertyCollection();
        }

        internal static EditorRoom CreateEmpty()
        {
            const int Width = 56;
            const int Height = 32;
            TileType[] tiles = new TileType[Width * Height];
            tiles.Populate(TileType.Air);

            return new EditorRoom(new EditorRoomMap(
                new ResizableTileMap<TileType>(tiles, Width, Height),
                new ResizableTileMap<TileType>(tiles.ToArray(), Width, Height)), // Create a copy of the tile-array
                new GameObject[0], EditorRoomSerializer.CreateEmptyProperties());
        }

        private static PropertyCollection CreateEmptyProperties()
        {
            return new PropertyCollection();
        }
    }
}
