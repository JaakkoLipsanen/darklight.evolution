﻿using System;
using System.IO;
using System.Linq;
using DarkLight.Editor.Model;
using Flai;
using Flai.General;

namespace DarkLight.Editor.IO
{
    internal class EditorRoomMapSerializer
    {
        internal static void Save(BinaryWriter writer, EditorRoomMap roomMap)
        {
            writer.Write(roomMap.Width);
            writer.Write(roomMap.Height);

            // http://stackoverflow.com/a/15168879/925777
            // interesting.. cast byte[] array to object[] array and then that to TileType[]?
            // something like "writer.WriteArray((byte[])(object[])roomMap.DarkTiles)" :P

            // holy fuck does this look bad. but whatever, at least it's not long
            writer.WriteArray(roomMap.DarkTiles.Cast<int>().ToArray());
            writer.WriteArray(roomMap.LightTiles.Cast<int>().ToArray());
        }

        internal static EditorRoomMap FromStream(BinaryReader reader)
        {
            int width = reader.ReadInt32();
            int height = reader.ReadInt32();

            // yeaahh slow but whatever
            TileType[] darkTiles = reader.ReadInt32Array().Select(i => (TileType)i).ToArray(); // Cast<TileType> doesn't work, some exception in LINQ when using enums or something
            TileType[] lightTiles = reader.ReadInt32Array().Select(i => (TileType)i).ToArray();

            if (width * height == 0 || darkTiles.Length != width * height || lightTiles.Length != width * height)
            {
                throw new ArgumentException("Invalid tile map sizes");
            }

            return new EditorRoomMap(new ResizableTileMap<TileType>(darkTiles, width, height), new ResizableTileMap<TileType>(lightTiles, width, height));
        }
    }
}
