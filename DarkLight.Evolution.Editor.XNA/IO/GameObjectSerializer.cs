﻿using System;
using System.IO;
using System.Reflection;
using DarkLight.Editor.Model.GameObjects;
using Flai;
using Flai.IO;

namespace DarkLight.Editor.IO
{
    // TODO: Make this generic? So that GameObject.Save can take "Cannon"/"MovingPlatform" etc as a parameter instead of "GameObject"
    internal class GameObjectSerializer
    {
        internal static void Save(BinaryWriter writer, GameObject gameObject)
        {
            writer.Write(gameObject.Type.Name);
            writer.Write(gameObject);
        }

        #region FromStream

        // TODO: !! this is slow. maybe cache those MethodInfo's (or even "compile" MethodInfo to delegate via ReflectionHelper.Compile)
        internal static GameObject FromStream(BinaryReader reader)
        {
            string typeName = reader.ReadString();

            Type type = AssemblyHelper.FindType(Assembly.GetCallingAssembly(), typeName);
            if (type == null || !typeof(GameObject).IsAssignableFrom(type))
            {
                throw new ArgumentException("Invalid type");
            }

            MethodInfo methodInfo = typeof(BinarySerializerExtensions).GetMethod("ReadGeneric");
            MethodInfo genericMethodInfo = methodInfo.MakeGenericMethod(type);
            return genericMethodInfo.Invoke(null, new object[] { reader }) as GameObject;
        }

        #endregion
    }
}
