﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DarkLight.Editor.Model;
using DarkLight.Editor.Model.GameObjects;
using Flai;
using Flai.General;

namespace DarkLight.Editor.IO
{
    internal class EditorWorldSerializer
    {
        #region Save

        internal static void Save(BinaryWriter writer, EditorWorld world)
        {
            writer.Write(world.Rooms.Count);
            foreach (EditorRoom room in world.Rooms)
            {
                EditorRoomSerializer.Save(writer, room);
            }

            EditorWorldSerializer.SaveProperties(writer, world);
        }

        private static void SaveProperties(BinaryWriter writer, EditorWorld world)
        {
            //// TODO
            //writer.Write(Vector2i.One * 3); // spawn
            //writer.Write(Vector2i.One * 12); // goal
        }

        #endregion

        #region FromStream

        internal static EditorWorld FromStream(BinaryReader reader)
        {
            int roomCount = reader.ReadInt32();
            EditorRoom[] rooms = new EditorRoom[roomCount];
            for (int i = 0; i < rooms.Length; i++)
            {
                rooms[i] = EditorRoomSerializer.FromStream(reader);
            }

            EditorWorld world = new EditorWorld(rooms, EditorWorldSerializer.LoadProperties(reader));
            EditorWorldSerializer.ResolvePortalDestinations(world);

            return world;
        }

        private static void ResolvePortalDestinations(EditorWorld world)
        {
            foreach (Portal portal in world.Rooms.SelectMany(room => room.GameObjects.GetAllOfType<Portal>()))
            {
                portal.ResolveDestination(world.Rooms);
            }
        }

        private static PropertyCollection LoadProperties(BinaryReader reader)
        {
            return new PropertyCollection(new Dictionary<string, object>
            {
                //{ "SpawnIndex", reader.ReadGeneric<Vector2i>() },
                //{ "GoalIndex", reader.ReadGeneric<Vector2i>() },
            });
        }

        #endregion

        #region CreateEmpty

        // Does this belong here...?
        internal static EditorWorld CreateEmpty()
        {
            PropertyCollection propertyCollection = new PropertyCollection(new Dictionary<string, object>
            {
                // THESE CAN'T BE INDEXES ANYMORE! Needs <RoomID, Index>
                { "SpawnIndex", Vector2i.One * 4 },
                { "GoalIndex",  Vector2i.One * 12 },
            });

            return new EditorWorld(new EditorRoom[1] { EditorRoomSerializer.CreateEmpty() }, propertyCollection);
        }

        #endregion
    }
}
