﻿using System.IO;
using DarkLight.Editor.Model;

namespace DarkLight.Editor.IO
{
    internal class EditorUserSerializer
    {
        // WAAIIT A MINUTE. this stuff will be serialized to the actual game's map file too, 
        // so maybe here shouldn't be any editor stuff..? Or at least they should be LAST thing to write (which, this is at the time of writing)
        internal static void Save(BinaryWriter writer, EditorUser editorUser)
        {
            //
        }

        internal static EditorUser FromStream(BinaryReader reader, EditorWorld world)
        {
            return new EditorUser(world);
        }
    }
}
