using System;
using DarkLight.Editor.Controller;
using DarkLight.Editor.Model;
using DarkLight.Editor.View;
using Flai;
using Flai.Editor;
using Flai.Graphics;
using Flai.Input;

namespace DarkLight.Editor
{
    // Should the game be disposed when exiting the application?
    public class EditorGame : EditorGameBase
    {
        #region Fields and Properties

        private EditorLevel _level;
        private EditorLevelRenderer _levelRenderer;
        private EditorLevelController _levelController;

        public EditorLevel Level
        {
            get { return _level; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Level can't be null!");
                }
                else if (_level != value)
                {
                    this.LoadLevel(value);
                }
            }
        }

        #endregion

        public EditorGame()
        {
            _contentProvider.RootDirectory = "DarkLight.Evolution.Editor.XNA.Content";
            this.LoadLevel(EditorLevel.CreateEmpty());
        }

        protected override void LoadContentInner()
        {
            _levelRenderer.LoadContent();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            if (WpfHelper.IsMainWindowActive && base.IsActive && base.ViewportArea.Contains(updateContext.InputState.MousePosition))
            {
                this.UpdateShouldBeFocused(updateContext);

                _levelController.Control(updateContext);
                _level.Update(updateContext);
                _levelRenderer.Update(updateContext);
            }

            // ?
            base.OnPropertyChanged("FPS");
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            _levelRenderer.Draw(graphicsContext);
        }

        private void UpdateShouldBeFocused(UpdateContext updateContext)
        {
            // If any mouse button is pressed or the scroll wheel delta is not zero, then the game should be focused
            this.ShouldBeFocused =
                updateContext.InputState.IsMouseButtonPressed(MouseButton.Left) ||
                updateContext.InputState.IsMouseButtonPressed(MouseButton.Right) ||
                updateContext.InputState.IsMouseButtonPressed(MouseButton.Middle) ||
                updateContext.InputState.ScrollWheelDelta != 0;
        }

        private void LoadLevel(EditorLevel level)
        {
            if (_levelRenderer != null)
            {
                _levelRenderer.Unload();
            }

            _level = level;
            _levelRenderer = new EditorLevelRenderer(_level);
            _levelController = new EditorLevelController(_level);

            if (this.IsLoaded)
            {
                _levelRenderer.LoadContent();
            }

            base.OnPropertyChanged("Level");
        }
    }

    #region Dummy Entry Point

    internal static class Program
    {
        static void Main(string[] args)
        {
            System.Windows.Forms.MessageBox.Show("DarkLight.Evolution.Editor.XNA must be used from WPF editor!");
        }
    }

    #endregion
}
